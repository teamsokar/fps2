# PCG FPS v2

Demo Project: First person shooter build from scratch with C++ and OpenGL with procedurally generated levels.

## Usage

Binary build is available from the [Project Page on my Portfolio](https://m-legner.eu/projects/1-pcg-fps).

The program should start by simply double clicking on the **fps2.exe** file, give the requirements are met. Do not change the folder structure, given this is a demo, file loading is not implemented in the most robust way and could result in unforeseen behavior of the program.

## System Requirements

* Microsoft Windows 10
* OpenGL 3.3 enabled Graphics Card and driver

## Build Instructions
* Building requires the following Visual Studio Workloads
  * Visual Studio 2019 Community Edition Version 16.5.4
  * Desktop Development with C++
  * Windows SDK 10.0.17763.0
* clone repository with option **--recursive** to also get the needed submodules
* run **tools/createAssetLink.bat** with admin rights
	* It uses ``mklink`` to create a symlink from the VS output directory to the assets directory. The paths to assets are hard coded, to preserve them without copying them multiple times.
	* alternatively, copy the **assets** folder to **Debug** or **Release** folder as it only includes the needed assets.
* Load **fps2.sln** in Visual Studio 2019
* Build Debug or Release configuration for x86/Win32 Platform
* run from within Visual Studio or run **fps2.exe** from Debug or Release directory

## Tech
* Build with Visual Studio 2019 Community Edition (16.5.4) [[Download Installer](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)]
* Windows SDK Version 10.0.17763.0
* [C++17](https://isocpp.org)
* [OpenGL 3.3](https://www.khronos.org/registry/OpenGL/index_gl.php)
* currently only x86 build supported

### Used Third Party Libraries
* [GLFW](https://www.glfw.org)
* [GLEW](http://glew.sourceforge.net)
* [stb](https://github.com/nothings/stb)
* [GLM](https://github.com/g-truc/glm)
* [Open Asset Importer Library assimp](http://assimp.org)
* [ReactPhysics3D](https://www.reactphysics3d.com/)

## Gameplay

The goal of the game is to go through the rooms, hit all the targets with the least amount of shots and reach the end. The end is a small room with green-rusty-textures. Targets are a wood/metal box and a rebuild of the Crysis Nanosuit model. Once hit, they turn green.

### Controls

Keys are hardcoded and not changeable unless the code is changed an recompiled. This is a demo/prototype, not a game ready for sale.

#### Camera

| Key | Function |
| ----| -------- |
| W | Move Forward |
| S | Move Backward |
| A | Move Left |
| S | Move Right
| Mouse | Change camera angle |

#### Gameplay
| Key | Function | Comment | 
| --- | -------- | ------- |
| Left Mouse Button | Shoot | hits will be visualized as red sphere | 

#### Game

| Key | Function | Comment |
| --- | -------- | ------- |
| F2 | Reset game | Reset stats, set player back to start | 
| F3 | print current game stats to console | | 
| F5 | Restart Game | Removes all hits, resets stats and sets player back into start box | 

#### Demonstration and Debug

| Key | Function | Comment |
| --- | -------- | ------- |
| F6 | write current position of camera to log file | X,Y,Z coordiantes |
| F7 | activates ghost mode | ignores all collision | 
| F8 | show positions of lights | switch between on and off. always drawn in color of light |
| F9 | make lights move | switch between static at center of room and moving around center in a circle |
| F10| Draw BoundingBox of Level, Targets and Collision Geometry |  Center drawn as Point, extends as box. Colors: Red: Collision; Blue: Target; Green: Startline | 
| F11 | toggle normal mapping | toggles usage of normal mapping on walls and targets |
| F12 | switches shadow rendering modes | Modes: off, Shadow Mapping, Shadow Mapping with PCF, Shadow Mapping with PCF with fixed Sample Positions

#### other

| Key | Function | Comment |
| --- | -------- | ------- |
| Escape | close programm | how dare you? |

### Settings file

| Key | Values | Comment | Default | Comment |
| --- | ------ | ------- | -------- | ------ |
| scr_width | Integer | Width of window | 1280 | |
| scr_height | Integer | Height tof window | 720 | |
| rndr_type | Integer: 0 = OpenGL | select type of render | 0 | |
| rnd_near | floating point number | position of near plane from camera | 0.1 | |
| rnd_far | Floating point number | position of far plane from camera | 300.0 | |
| rnd_vsync | boolean value via 0/1 | use [vertical synchronization](https://en.wikipedia.org/wiki/Analog_television#Vertical_synchronization) | 0  | | 
|rng_mode | Integer | 0 = random single seed, 0 = fixed seed | 0 | 0 = use specified seed **rng_seed** to generate level; 1 = use random seed to generate level, **rng_seed** setting will be ignored  |
| rng_seed | unsigned Integer | seed to re-generate a know level | 0 | specify integer between 0 and 4294967295 to be used in generator |
| rng_printseed | boolean | 1 = prints used seed to command line. | 0| output seed for generator to console |
| rnd_colorcoderooms | boolean value via 0/1| 0 (off) | 1 = add color code according to type of room (hallway, start, finish), 0 = off | 

## Directory structure

### assets
contains assets for the projects: models, textures, shaders

### fps2
main project  
Subdirectories *Debug* and *Release* will be created upon build

### Libraries
contains first party libraries as git submodules

* [PCG](https://bitbucket.org/teamsokar/pcg): library handling procedural content generation
* [Tools](https://bitbucket.org/teamsokar/tools): various tools

### ThirdParty
contains third party libraries  

* [GLFW](https://www.glfw.org) Version 3.3 Win32  
* [GLEW](http://glew.sourceforge.net) Version 2.1 Win32  
* [stb](https://github.com/nothings/stb)  as git submodule  
* [GLM](https://github.com/g-truc/glm) as git submodule
* [assimp](http://assimp.org) Version 4.1.0 Win32
* [ReactPhysics3D](https://www.reactphysics3d.com/) Version 0.7.1

### Tools
contains small helper scripts

**Build Tools**

* **createAssetLink.bat**: uses Windows utility *mklink* to create a symlink from assets directory to both build-directories

* **createBuild.bat**: creates a shippable build, only including necessary executables and all assets

**Counting lines of Code**

* **cloc.bat**: calculates lines of code of the projects. Just for fun.

* **cloc.exe**: build of [cloc](https://github.com/AlDanial/cloc)
  
* **cloc_exclude_pcg.txt**: list of excluded files from cloc
