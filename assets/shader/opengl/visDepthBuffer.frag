#version 330 core

out vec4 FragColor;

in vec2 TexCoords;

uniform float near = 0.1;
uniform float far = 300.0;

uniform bool linearized = true;

float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // back to NDC
    return (2.0 * near * far) / (far + near - z * (far - near));
}

void main()
{     
    if(!linearized)
    {
        FragColor = vec4(vec3(gl_FragCoord.z), 1.0);
    }
    else
    {
        float depth = LinearizeDepth(gl_FragCoord.z) / far;
        FragColor = vec4(vec3(depth), 1.0);
    }
    
}