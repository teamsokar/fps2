#version 330 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoord;
layout(location = 3) in vec3 aTangent;
layout(location = 4) in vec3 aBitangent;

#define NR_POINT_LIGHTS_RESERVED 2
//uniform PointLight pointLights[NR_POINT_LIGHTS_RESERVED];
uniform int NR_POINTLIGHTS_SET;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoord;

uniform vec3 lightPos[NR_POINT_LIGHTS_RESERVED];
uniform vec3 viewPos;

uniform bool useNormalMapping;
out vec3 TangentLightPos[NR_POINT_LIGHTS_RESERVED];
out vec3 TangentViewPos;
out vec3 TangentFragPos;

void main()
{
    Normal = mat3(transpose(inverse(model))) * aNormal;
    FragPos = vec3(model * vec4(aPos, 1.0));
    TexCoord = aTexCoord;

    if (useNormalMapping)
    {
        vec3 T = normalize(vec3(model * vec4(aTangent, 0.0)));
        //vec3 B = normalize(vec3(model * vec4(aBitangent, 0.0)));
        vec3 N = normalize(vec3(model * vec4(aNormal, 0.0)));

        // Gram-Schmidt process
        T = normalize(T - dot(T, N) * N);
        vec3 B = cross(N, T);

        mat3 TBN = mat3(T, B, N);

        for (int i = 0; i < NR_POINTLIGHTS_SET; ++i)
        {
            TangentLightPos[i] = TBN * lightPos[i];
        }

        TangentViewPos = TBN * viewPos;
        TangentFragPos = TBN * FragPos;
    }
    else
    {
        for (int i = 0; i < NR_POINTLIGHTS_SET; ++i)
        {
            TangentLightPos[i] = lightPos[i];
        }

        TangentViewPos = viewPos;
        TangentFragPos = FragPos;
    }


    gl_Position = projection * view * model * vec4(aPos, 1.0);
}
