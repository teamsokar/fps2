# Readme
PCG-FPS Project Version XXXXX compiled on 2020-MM-DD  
git commit: [##hash###](https://bitbucket.org/teamsokar/fps2/commits/###hash###)  
[Download](https://www.sok4r.de/pcgfps/builds/2020-mm-dd_v2_mX.XX.XX/2020-mm-dd_v2_mX.XX.XX.7z)

## Index

- [Readme](#readme)
  - [Index](#index)
  - [Features](#features)
  - [Reference Screenshot](#reference-screenshot)
  - [Know Issues](#know-issues)
  - [Usage](#usage)
  - [System Requirements](#system-requirements)
  - [Controls](#controls)
    - [Camera](#camera)
    - [Gameplay](#gameplay)
    - [Game](#game)
    - [Demonstration and Debug](#demonstration-and-debug)
    - [other](#other)
  - [Settings file](#settings-file)
  - [Technical Stuff](#technical-stuff)
    - [Libraries](#libraries)
    - [ThirdParty](#thirdparty)

## Features
*

## Reference Screenshot
![Reference Screenshot](ref.jpg)

## Know Issues
* Some times "ERROR SHADER LINKING" appears in cmd-window, no idea why and should not be a problem as long as everything else is visible
* overly zealous heuristic mechanisms of virus protection software might detect some of the files as malicious. I can assure that there is nothing malicious in this software. Whitelisting the files and/or directories might be required to run.
* Normal mapping does not work property on certain surfaces and in combination with shadows. It is deactivated by default

## Usage

The program should start by simply double clicking on the *fps2.exe* file, give the requirements are met. Do not change the folder structure, given this is a demo, file loading is not implemented in the most robust form and could result in unforeseen behavior of the program.

## System Requirements

* Microsoft Windows 10
* OpenGL 3.3 enabled Graphics Card and driver

## Controls

### Camera

| Key | Function |
| ----| -------- |
| W | Move Forward |
| S | Move Backward |
| A | Move Left |
| S | Move Right
| Mouse | Change camera angle |

### Gameplay
| Key | Function | Comment | 
| --- | -------- | ------- |
| Left Mouse Button | Shoot | hits will be visualized as red sphere | 

### Game

| Key | Function | Comment |
| --- | -------- | ------- |
| F2 | Reset game | Reset stats, set player back to start | 
| F3 | print current game stats to console | | 
| F5 | Restart Game | Removes all hits, resets stats and sets player back into start box | 

### Demonstration and Debug

| Key | Function | Comment |
| --- | -------- | ------- |
| F6 | write current position of camera to log file | X,Y,Z coordiantes |
| F7 | activates ghost mode | ignores all collision | 
| F8 | show positions of lights | switch between on and off. always drawn in color of light |
| F9 | make lights move | switch between static at center of room and moving around center in a circle |
| F10| Draw BoundingBox of Level, Targets and Collision Geometry |  Center drawn as Point, extends as box. Colors: Red: Collision; Blue: Target; Green: Startline | 
| F11 | toggle normal mapping | toggles usage of normal mapping on walls and targets |
| F12 | switches shadow rendering modes | Modes: off, Shadow Mapping, Shadow Mapping with PCF, Shadow Mapping with PCF with fixed Sample Positions

### other

| Key | Function | Comment |
| --- | -------- | ------- |
| Escape | close programm | how dare you? |

## Settings file

| Key | Values | Comment | Default | Comment |
| --- | ------ | ------- | -------- | ------ |
| scr_width | Integer | Width of window | 1280 | |
| scr_height | Integer | Height tof window | 720 | |
| rndr_type | Integer: 0 = OpenGL | select type of render | 0 | |
| rnd_near | floating point number | position of near plane from camera | 0.1 | |
| rnd_far | Floating point number | position of far plane from camera | 300.0 | |
| rnd_vsync | boolean value via 0/1 | use [vertical synchronization](https://en.wikipedia.org/wiki/Analog_television#Vertical_synchronization) | 0  | | 
|rng_mode | Integer | 0 = random single seed, 0 = fixed seed | 0 | 0 = use specified seed **rng_seed** to generate level; 1 = use random seed to generate level, **rng_seed** setting will be ignored  |
| rng_seed | unsigned Integer | seed to re-generate a know level | 0 | specify integer between 0 and 4294967295 to be used in generator |
| rng_printseed | boolean | 1 = prints used seed to command line. | 0| output seed for generator to console |
| rnd_colorcoderooms | boolean value via 0/1| 0 (off) | 1 = add color code according to type of room (hallway, start, finish), 0 = off | 

## Technical Stuff
* Build with Visual Studio 2019 Community Edition (16.4.5) [[Download Installer](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16)]
* Windows SDK Version 10.0.17763.0
* [C++17](https://isocpp.org)
* [OpenGL 4.6](https://www.khronos.org/registry/OpenGL/index_gl.php)
* currently only x86 build supported

### Libraries
* [PCG](https://bitbucket.org/teamsokar/pcg): library handling procedural content generation
* [Tools](https://bitbucket.org/teamsokar/tools): various tools

### ThirdParty

* [GLFW](https://www.glfw.org) Version 3.3 Win32  
* [GLEW](http://glew.sourceforge.net) Version 2.1 Win32  
* [stb](https://github.com/nothings/stb)  as git submodule  
* [GLM](https://github.com/g-truc/glm) as git submodule
* [assimp](http://assimp.org) Version 4.1.0 Win32
* [ReactPhysics3D](https://www.reactphysics3d.com/) Version 0.7.1
