@echo off
REM simple script to create a new build in tmp-folder

REM TODO come up with a better name for this script...

mkdir ..\..\Builds\tmp

cd.. 

REM executables
xcopy /Y Release\fps2.exe ..\builds\tmp
xcopy /Y Release\skr-tools.dll ..\builds\tmp
xcopy /Y Release\skr-pcg.dll ..\builds\tmp
xcopy /Y Release\glew32.dll ..\builds\tmp
xcopy /Y Release\assimp-vc140-mt.dll ..\builds\tmp
echo D|xcopy /Y emptyreadme.md ..\builds\tmp\readme.md

REM resources
xcopy /Y Release\assets ..\builds\tmp\assets /s /i

echo copying ended

REM Just4fun
echo calculating lines of code via cloc
cd tools
cloc.bat > ..\..\Builds\tmp\loc.txt
