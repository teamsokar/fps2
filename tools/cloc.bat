@echo off
REM count lines of MY code - just for fun

echo FPS2 - main project
cloc.exe --exclude-dir=doc --exclude-ext=vcxproj,filters,mdbat --progress-rate=0 ..\fps2

echo Libraries
echo.
echo pcg lib
cloc.exe  --exclude-dir=doc --exclude-ext=vcxproj,filters,md,bat --progress-rate=0 --exclude-list-file=cloc_exclude_pcg.txt ..\Libraries\pcg
echo.
echo tools lib
cloc.exe --exclude-dir=doc --exclude-ext=vcxproj,filters,md,bat --progress-rate=0 ..\Libraries\tools
