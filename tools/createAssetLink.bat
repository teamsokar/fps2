@echo off
REM create symbolik links to assets folder
REM mklink requires admin rights to work

cd..

IF NOT EXIST Debug mkdir Debug

cd Debug
mklink /D assets "..\assets"
cd..

IF NOT EXIST Release mkdir Release

cd Release
mklink /D assets "..\assets"
