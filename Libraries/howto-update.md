# Update Libraries
* Changes only in the original repository!
* in submodule folder:  
*     git pull origin master
* in Visual Studio: commit [submodule update]
* on other machine: use submodule update in Visual Studio
