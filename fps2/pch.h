#ifndef PCH_H
#define PCH_H

// TODO: add headers that you want to pre-compile here

#include <glm/glm.hpp>

#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "reactphysics3d.h"

#include <Windows.h>

#endif //PCH_H
