#ifndef SKR_FPS2_VERSION_H
#define SKR_FPS2_VERSION_H

namespace skr
{
namespace fps2
{

    // defines version number of project

#define PCGFPS_MAJOR_VERSION 0
#define PCGFPS_MINOR_VERSION 22
#define PCGFPS_PATCH_VERSION 4

}
}

#endif // ! SKR_FPS2_VERSION_H
