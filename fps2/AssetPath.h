//! \file

#ifndef SKR_FPS2_ASSET_PATH_H
#define SKR_FPS2_ASSET_PATH_H

#include <string>
#include <Windows.h>

namespace skr
{
namespace fps2
{

    //! \brief helper function got get the complete path of a given file in assets folder
    //! since the place of the exe-file is unknow at compile time, call it at runtime and add relative path to assets
    //! \param file relative path to file requested
    //! \return complete path to file requested
    static std::string GetAssetPath(std::string file)
    {
        auto getExeDir = []()
        {
            char result[MAX_PATH];
            auto path = std::string(result, GetModuleFileNameA(nullptr, result,     MAX_PATH));
            return path.substr(0, path.rfind("\\"));
        };
    
        return getExeDir() + "/assets/" + file;
    }

}
}
#endif // !SKR_FPS2_ASSET_PATH_H

