//! \file

#include "pch.h"

#define STB_IMAGE_IMPLEMENTATION

//#define SKR_FPS_DEMO

#include <iostream>
#include <string>

#include "version.h"

#include "Logger.h"
#include "CPUInfo.h"
#include "Renderer/OpenGL/Settings.h"

#include "Renderer/OpenGL/RendererOpenGL.h"
#include "Renderer/OpenGL/Camera.h"
#include "Game/Game.h"

#pragma region GLFW Callback functions

//TODO I __REALLY__ don't like global variables, find something better - literally anything could be
skr::fps2::Renderer::OpenGL::Renderer _oglRenderer; //!< render object

std::shared_ptr<skr::fps2::Renderer::OpenGL::Camera> _camera; //!< pointer to camera
std::shared_ptr<skr::fps2::Collision::CollisionManager> _collisionManager; //!< pointer to collision manager

//! \brief GLFW callback function triggered if size of framebuffer changes (e.g. window resize)
// \param window unsued
//! \param width new width of framebuffer
//! \param height new height of framebuffer
void Callback_FramebufferSize(GLFWwindow* /*window*/, int width, int height)
{
    glViewport(0, 0, width, height);
}

//! \brief GLFW callback function triggered if mouse is moved
//  \param window unsued
//! \param xpos new position of mouse cursor on x-axis
//! \param ypos new position of mouse cursor on y-axis
void Callback_Mouse(GLFWwindow* /*window*/, double xpos, double ypos)
{
#ifdef _DEBUG
    assert(_camera != nullptr);
#endif // DEBUG

    if (_camera->_firstMouse)
    {
        _camera->_lastX = (float)xpos;
        _camera->_lastY = (float)ypos;
        _camera->_firstMouse = false;
    }

    float xoffset = (float)(xpos - _camera->_lastX);
    float yoffset = (float)(_camera->_lastY - ypos);

    _camera->_lastX = (float)xpos;
    _camera->_lastY = (float)ypos;

    _camera->ProcessMouseMovement(xoffset, yoffset);
}

//! \brief GLFW callback function triggered if mouse scrollwheel is used
//! currently unused 
// \param window unsued
// \param xoffset unused
//! \param yoffset offset on y-axis
void Callback_ScrollWheel(GLFWwindow* /*window*/, double /*xoffset*/, double yoffset)
{

}

//! \brief GLFW callback function triggered if keystroke on keyboard is registered
//! handels most input handling and/or calls functions to handle input actions
//! \param window window the keystroke was registered by
//! \param key key that is pressed or released
// \param scancode unused
//! \param action action (eg. press or release)
// \param mods unused
void Callback_Key(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/)
{
    // --------------------------------------------------------------------------------------------------------------
    // Game revelant Keys
    // --------------------------------------------------------------------------------------------------------------
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (key == GLFW_KEY_F2 && action == GLFW_PRESS)
    {


        skr::tools::Logger::GetInstance().Log("Clear Hits / Reset Game Stats");
        _collisionManager->ClearHits();
        skr::fps2::Game::Game::GetInstance().ResetStats();
    }

    if (key == GLFW_KEY_F3 && action == GLFW_PRESS)
    {
        std::string msg = "Game Stats " + skr::fps2::Game::Game::GetInstance().GetStats();
        skr::tools::Logger::GetInstance().Log(msg);
        std::cout << msg << std::endl;
    }

    if (key == GLFW_KEY_F5 && action == GLFW_PRESS)
    {
        skr::tools::Logger::GetInstance().Log("Recreating Geometry");
        _oglRenderer.RecreateGeometry();
        _collisionManager->ClearHits();
    }

    // --------------------------------------------------------------------------------------------------------------
    // Debug keys
    // --------------------------------------------------------------------------------------------------------------

    if (key == GLFW_KEY_F6 && action == GLFW_PRESS)
        skr::tools::Logger::GetInstance().Log(_camera->GetCurrentParametersReadable());

    if (key == GLFW_KEY_F7 && action == GLFW_PRESS)
    {
        skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::GhostMode] = !skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::GhostMode];

#ifdef SKR_FPS_DEMO
        std::cout << (_oglRenderer._ghostMode ? "Ghost mode activated" : "Ghost mode deactivated") << std::endl;
#endif
    }

    if (key == GLFW_KEY_F8 && action == GLFW_PRESS)
    {
        skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::DrawDebugLight] = !skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::DrawDebugLight];

#ifdef SKR_FPS_DEMO
        std::cout << (_oglRenderer._drawDebugLight ? "Draw Debuglight activated" : "Draw Debuglight deactivated") << std::endl;
#endif
    }

    if (key == GLFW_KEY_F9 && action == GLFW_PRESS)
    {
        skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::MoveLights] = !skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::MoveLights];

#ifdef SKR_FPS_DEMO
        std::cout << (_oglRenderer._moveLights ? "Moving lights activated" : "Moving lights deactivated") << std::endl;
#endif
    }

    if (key == GLFW_KEY_F10 && action == GLFW_PRESS)
    {
        skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::DrawBoundingBox] = !skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::DrawBoundingBox];

#ifdef SKR_FPS_DEMO
        std::cout << (_oglRenderer._drawBoundingBox ? "Draw BoundingBoxes activated" : "Draw BoundingBoxes deactivated") << std::endl;
#endif
    }

    if (key == GLFW_KEY_F11 && action == GLFW_PRESS)
    {
        skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::UseNormalMapping] = !skr::fps2::Settings::GetInstance()._runtime_bool[skr::fps2::Settings::RuntimeBoolSetting::UseNormalMapping];

#ifdef SKR_FPS_DEMO
        std::cout << (_oglRenderer._useNormalMapping ? "Normal Mapping activated" : "Normal Mapping deactivated") << std::endl;
#endif
    }

    if (key == GLFW_KEY_F12 && action == GLFW_PRESS)
    {
        auto shadow = static_cast<uint8_t>(skr::fps2::Settings::GetInstance()._shadowRenderingMode);

        if (shadow < 3)
            shadow++;
        else
            shadow = 0;

        skr::fps2::Settings::GetInstance()._shadowRenderingMode = static_cast<skr::fps2::Settings::ShadowType>(shadow);

#ifdef SKR_FPS_DEMO
        switch (_oglRenderer._shadows)
        {
        case skr::fps2::Renderer::OpenGL::ShadowType::Off:
            std::cout << "Shadows off" << std::endl;
            break;
        case skr::fps2::Renderer::OpenGL::ShadowType::ShadowMapping:
            std::cout << "Shadow Mapping" << std::endl;
            break;
        case skr::fps2::Renderer::OpenGL::ShadowType::ShadowMappingPCF:
            std::cout << "Shadow Mapping with PCF" << std::endl;
            break;
        case skr::fps2::Renderer::OpenGL::ShadowType::ShadowMappingPCFDisk:
            std::cout << "Shadow Mapping with PCF and fixed Sample Disk" << std::endl;
            break;
        default:
            std::cout << "Unknow Shadow Mode" << std::endl;

        }
#endif
    }

}

//! \brief GLFW callback function triggered if a mouse button press or release is registered
// \param window unused
//! \param button mouse button pressed or released
//! \param action action (eg. press or release)
// \param mods unused
void Callback_MouseButton(GLFWwindow* /*window*/, int button, int action, int /*mods*/)
{
#ifdef _DEBUG
    assert(_camera != nullptr);
    assert(_collisionManager !=  nullptr);
#endif // DEBUG

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
        auto viewMatrix = _camera->GetViewMatrix();
        glm::vec3 dir(viewMatrix[0][2], viewMatrix[1][2], viewMatrix[2][2]);
        _collisionManager->RayCast(_camera->_position, dir);

        skr::fps2::Game::Game::GetInstance().Shot();
    }
}

#pragma endregion

//! \brief main function of the progrmam
//! sets up basic functionality: starts Logger, gatheres system info and starts initialization of OpenGL Renderer
int main()
{
    const std::string windowTitle = "PCGFPS v2 m"
        + std::to_string(PCGFPS_MAJOR_VERSION) + '.' + std::to_string(PCGFPS_MINOR_VERSION) + '.' + std::to_string(PCGFPS_PATCH_VERSION) + ' '
        + __DATE__ + ' ' + __TIME__;

    // start Logger
    skr::tools::Logger::GetInstance().LogWithDatenAndTime(windowTitle);
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("main()");
    

	//skr::tools::Settings::GetInstance().ReadSettingsFromFile();

    // init renderer
    skr::fps2::Settings::GetInstance().ReadSettingsFromFile();
    _oglRenderer.SetWindowTitle(windowTitle);
    _oglRenderer.Initialize();

    _camera = _oglRenderer._camera;
    _collisionManager = _oglRenderer._collisionManager;

    // set GLFW input callbacks
    // NOTE: setting current window context is done in 
    glfwSetFramebufferSizeCallback(_oglRenderer._window, Callback_FramebufferSize);
    glfwSetCursorPosCallback(_oglRenderer._window, Callback_Mouse);
    glfwSetScrollCallback(_oglRenderer._window, Callback_ScrollWheel);
    glfwSetKeyCallback(_oglRenderer._window, Callback_Key);
    glfwSetMouseButtonCallback(_oglRenderer._window, Callback_MouseButton);

    glfwSetInputMode(_oglRenderer._window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    // Gather and Log system information
    skr::tools::Logger::GetInstance().LogWithDatenAndTime(skr::tools::CPUInfo::GetSystemInfo(true));
    skr::tools::Logger::GetInstance().LogSeparationLine();
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Details");
    skr::tools::Logger::GetInstance().Log(_oglRenderer.GetGLDetails(false));
    skr::tools::Logger::GetInstance().LogSeparationLine();

    // and finally start rendering
    _oglRenderer.Render();

    // save settings on close
    skr::fps2::Settings::GetInstance().WriteSettingsToFile();

    // goodbye
    exit(EXIT_SUCCESS);
}
