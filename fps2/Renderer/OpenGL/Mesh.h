#ifndef SKR_FPS2_RENDERER_OPENGL_MESH_H
#define SKR_FPS2_RENDERER_OPENGL_MESH_H

#include "..\..\pch.h"

#include <vector>

#include "Texture.h"
#include "Shader.h"
#include "pcg\Definitions.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class Vertex
    //! \brief vertex information
    struct Vertex
    {
        glm::vec3 Position;     //!< position
        glm::vec3 Normal;       //!< normal
        glm::vec2 TexCoords;    //!< texture coordinates
        glm::vec3 Tangent;      //!< tangent
        glm::vec3 Bitangent;    //!< bitangent
    };

    //! \class Mesh
    //! \brief holds geometry of a single mesh
    class Mesh
    {
        private:
            std::vector<Vertex> _vertices;      //!< list of vertices
            std::vector<uint32_t> _indices;     //!< list of indices. If raw vertices are used, this list is empty
            
            uint32_t _vao;  //!< vertex array object
            uint32_t _vbo;  //!< vertex buffer object
            uint32_t _ebo;  //! element buffer object. If raw vertices are used, this field is not used

        public:
            std::vector<Texture> _textures;     //!< list of texture for this mesh

            bool _useDrawColor;     //!< flag inidicating if color is used to draw mesh instead of textures
            glm::vec3 _drawColor;   //! draw color. If texture are used, this is ignored

        public:
            //! \brief constructor
            //! build from lists of raw data
            //! \param vertices list of vertices
            //! \param indices list of indices. If raw vertices are used, an empty list is assumed
            //! \param textures list of textures
            //! \param drawColor color to draw mesh in instead of textures. Default: white (1,1,1)
            Mesh(std::vector<Vertex> vertices, std::vector<uint32_t> indices, std::vector<Texture> textures, glm::vec3 drawColor = glm::vec3(1.0f));

            //! \brief constructor
            //! build from face defined in procedural generation
            //! \param face face containing geometry information
            Mesh(skr::pcg::Face face);

            //! \brief destructor
            ~Mesh();

            //! \brief Draw mesh with diven shader
            //! "regular" draw function, that should usually be used
            //! \param shader shader to draw mesh in
            //! \param depthMap texture id to depthmap. Default: 0 (non used)
            //! \param useMaterialTextures flag indicating if material textures (diffuse maps, normal maps, etc) are used instead of flat colors
            void Draw(Shader shader, uint32_t depthMap = 0, bool useMaterialTextures = false) const;

            //! \brief draws only geometry without texture or color
            //! used to render depthmap for shadow mapping
            //! \param shader Shader to draw geometry with. Assuming one of type DepthMap
            void DrawGeometry() const;

            //! \brief sets _drawColor
            //! \param color color _drawColor is set to
            void SetDrawColor(glm::vec3 color);

            //! \brief calcualtes the extrem positions of the mesh in every direction
            //! output parameters should result in oppisite edge corners of a bounding box
            //! \param max [out] maximum values
            //! \param min [out] minimum values
            void GetExtremePositions(glm::vec3& max, glm::vec3& min);

        private:
            //! \brief prepares Textures before drawing them
            //! \param shader shader to draw mesh with
            //! \param depthMap id of depthmap used. Default: 0 (non used)
            //! \param useMaterialTextures flag indicating if material textures (diffuse maps, normal maps, etc) are used instead of flat colors
            void DrawPrepareTextures(Shader shader, uint32_t depthMap = 0, bool useMaterialTextures = false) const;

            //! \brief setup meshes to draw using index buffer
            void SetupMeshIndexed();

            //! \brief setup raw meshes to draw
            void SetupMeshRawTriangles();

            //! \brief calculates tangent and bitangent of given positions and texture coordinates
            //! used to prepare mesh for normal mapping, used to transform normal map into suitable coordiante system of the mesh
            //! positions and texture coordiantes have to match and form a triangle with the same normal vector
            //! \param pos1 first position
            //! \param pos2 second position
            //! \param pos3 third position
            //! \param tex1 texture coordiante at first position
            //! \param tex2 texture coordiante at second position
            //! \param tex3 texture coordiante at third position
            //! \param normal normal vector of positions
            //! \return list of two vectors: tangent, bitangent
            std::array<glm::vec3, 2> CalculateTangentAndBitangentForTriangle(glm::vec3 pos1, glm::vec3 pos2, glm::vec3 pos3, glm::vec2 tex1, glm::vec2 tex2, glm::vec2 tex3, glm::vec3 normal);

            //! \brief calculates tangent and bitangent of given positions and texture coordinates
            //! used to prepare mesh for normal mapping, used to transform normal map into suitable coordiante system of the mesh
            //! positions and texture coordiantes have to match and form a triangle with the same normal vector
            //! \param positions list of three positions
            //! \param texcoords list of three texture coordinates matching the three positions
            //! \param normal normal vector of positions
            //! \return list of two vectors: tangent, bitangent
            std::array<glm::vec3, 2> CalculateTangentAndBitangentForTriangle(std::array<glm::vec3, 3> positions, std::array<glm::vec2, 3> texcoords, glm::vec3 normal);
    };

}
}
}
}

#endif //!SKR_FPS2_RENDERER_OPENGL_MESH_H
