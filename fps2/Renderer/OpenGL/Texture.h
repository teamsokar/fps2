#ifndef SKR_FPS2_RENDERER_OPENGL_TEXTURE_H
#define SKR_FPS2_RENDERER_OPENGL_TEXTURE_H

#include "..\..\pch.h"

#include <iostream>
#include <string>
#include <unordered_map>

#include "stb_image.h"

namespace skr
{ 
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! enum types of texture
    enum struct TextureType
    {
        EMPTY,      //!< no texture
        DIFFUSE,    //!< diffuse texture
        NORMAL,     //!< normal map
        SPECULAR,   //!< specular map
        HEIGHT      //!< height map. Not used here, but assimp requires it
    };

    //! mapping table from a texture type to string
    typedef std::unordered_map<TextureType, std::string> TextureTypToStringMap;

    //! mapping table from a string to a texture type
    typedef std::unordered_map<std::string, TextureType> StringToTextureTypeMap;

    //! \class Texture
    //! \brief holds all informations for a texture
    class Texture
    {
        public:
            uint32_t _id;       //!< id of texture
            TextureType _type;  //!< TextureType
            std::string _path;  //!< path where texture is loaded from
            
            //TODO i would like to keep the maps instead of single strings, because it would make them easier to organize. However, I need to access them without making a deep copy at every draw call, which tanks the framerate
            //static const std::unordered_map<TextureType, std::string> MapTextureTypToString;
            //static const std::unordered_map<std::string, TextureType> MapStringToTextureType;

            static const std::string TextureTypeDiffuse;    //!< string with name of diffuse texture used in shader
            static const std::string TextureTypeNormal;     //!< string with name of normal map used in shader
            static const std::string TextureTypeSpecular;   //!< string with name of specular map used in shader
            static const std::string TextureTypeHeight;     //!< string with name of height map used in shader

        public:
            //! \brief default constructor
            //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
            Texture()
                : _id(0), _type(TextureType::EMPTY), _path("")
            {};

            //! \brief constructor
            //! \param type TextureType
            //! \param path path where texture is loaded from
            Texture(TextureType type, std::string path);

            //! \brief loads textures from a model
            //!  uses loadedTexture-parameter to skip already loaded textures
            //! \param loadedTexture [out] reference to list of already loaded textures
            //! \param dir directory with textures from model
            //! \param aiMat assimp material type
            //! \param aiType assimp texture type
            //! \param ttype TextureType
            //! \return list of new textures loaded
            static std::vector<Texture> LoadMaterialTextures(std::vector<Texture>& loadedTexture, std::string dir, aiMaterial* aiMat, aiTextureType aiType, TextureType ttype);

        private:
            //! \brief load texture from given path
            //! texture is bound to _id
            //! \param path path to texture
            //! \return true if loading successful, otherwise false
            bool LoadTexture(std::string path);
    };
}
}
}
}

#endif //!SKR_FPS2_RENDERER_OPENGL_TEXTURE_H
