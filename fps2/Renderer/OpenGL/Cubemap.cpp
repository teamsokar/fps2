#include "..\..\pch.h"
#include "Cubemap.h"
#include "stb_image.h"
#include <iostream>

#include "Logger.h"

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{

Cubemap::Cubemap(std::vector<std::string> faces)
    : _faces(faces)
{
    if (!LoadCubemap())
    {
        std::cout << "ERROR failed to load cubemap" << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR failed to load cubemap");
        exit(EXIT_FAILURE);
    }
}

bool Cubemap::LoadCubemap()
{
    glGenTextures(1, &_id);
    glBindTexture(GL_TEXTURE_CUBE_MAP, _id);

    int width, height, nrChannels;
    for (uint32_t i = 0; i < _faces.size(); i++)
    {
        unsigned char* data = stbi_load(_faces[i].c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else
        {
            std::cout << "ERROR failed to load cubemap face: " << _faces[i] << std::endl;
            skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR failed to load cubemap face: " + _faces[i]);
            stbi_image_free(data);
            return false;
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    return true;
}

}
}
}
}
