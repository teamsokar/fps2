#ifndef SKR_FPS2_RENDERER_OPENGL_DEFINITIONS_H
#define SKR_FPS2_RENDERER_OPENGL_DEFINITIONS_H

//! \file

#include "..\..\pch.h"

namespace skr
{ 
namespace fps2
{

#define NUM_ROOMS 10 //!< Number of Rooms to be generated

#define LOGSIZE 1024 //!< size of log from OpenGL Error Messages

constexpr float RP3D_DEFAULT_COLLISION_WALL_THICKNESS = 0.05f; //!< default thickness of collision boxes on walls
constexpr float RP3D_DEFAULT_RAYCAST_LENGHT = 25.0f; //!< length of a raycst for RP3D

constexpr glm::vec3 RENDER_TARGET_COLOR = glm::vec3(0.0f, 1.0f, 0.0f); //!< Color used to render a target (of non other is specified or textures used)
constexpr glm::vec3 RENDER_TARGET_HIT_COLOR = glm::vec3(0.0f, 1.0f, 0.0f); //!< Color added to a target once it is hit
constexpr glm::vec3 RENDER_HIT_COLOR = glm::vec3(1.0f, 0.0f, 0.0f); //!< Color of hit visualization

//!  \brief CollisionCategory
//! collision bodys have categories, which can be used to filter collision events, eg. only trigger action if collision with a body with a specified category is registered
enum struct CollisionCategory
{
    WALL = 1,       //!< walls
    TARGET = 2,     //!< targets
    PLAYER = 4,     //!< player body
    START = 8,      //!< start box
    STARTLINE = 16, //!< start line
    FINISH = 32     //!< finish box
};

// Size of Shadow Map
const uint32_t SHADOW_WIDTH = 1024;     //!< width of shadow map
const uint32_t SHADOW_HEIGHT = 1024;    //!< heigt of shadow map

}
}
#endif // !SKR_FPS2_RENDERER_OPENGL_DEFINITIONS_H

