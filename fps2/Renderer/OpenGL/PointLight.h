#ifndef SKR_FPS2_RENDERER_OPENGL_POINTLIGHT_H
#define SKR_FPS2_RENDERER_OPENGL_POINTLIGHT_H

#include "..\..\pch.h"
#include "Shader.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    constexpr glm::vec3 Default_Ambient_Color = glm::vec3(0.2f);    //!< default ambient color
    constexpr glm::vec3 Default_Diffuse_Color = glm::vec3(0.8f);    //!< default diffuse color
    constexpr glm::vec3 Default_Specular_Color = glm::vec3(0.5f);   //!< default specular color

    constexpr float Default_Attenuation_Constant = 1.0f;    //!< default attenuation constant factor
    constexpr float Default_Attenuation_Linear = 0.25f;     //!< default attenuation linear factor
    constexpr float Default_Attenuation_Quadratic = 0.060f; //!< default attenuation quadratic factor

    //! \class PointLight
    //! \brief defines a point light, emitting light in all directions
    class PointLight
    {
    public:
        //! \brief contructor
        //! \param pos Position
        //! \param ambientColor ambient color. Default: Default_Ambient_Color
        //! \param diffuseColor diffuse color. Default: Default_Diffuse_Color
        //! \param specularColor specular color. Default: Default_Specular_Color
        //! \param constant attenuation constant factor. Default: Default_Attenuation_Constant
        //! \param linear attenuation linear factor. Default: Default_Attenuation_Linear
        //! \param quadratic attenuation quadratic factor. Default: Default_Attenuation_Quadratic
        PointLight
        (
            std::array<float, 3> pos, 
            glm::vec3 ambientColor = Default_Ambient_Color, 
            glm::vec3 diffuseColor = Default_Diffuse_Color, 
            glm::vec3 specularColor = Default_Specular_Color,
            float constant = Default_Attenuation_Constant,
            float linear =   Default_Attenuation_Linear, 
            float quadratic = Default_Attenuation_Quadratic
        );
    
        //! \brief contructor
        //! \param pos Position
        //! \param ambientColor ambient color. Default: Default_Ambient_Color
        //! \param diffuseColor diffuse color. Default: Default_Diffuse_Color
        //! \param specularColor specular color. Default: Default_Specular_Color
        //! \param constant attenuation constant factor. Default: Default_Attenuation_Constant
        //! \param linear attenuation linear factor. Default: Default_Attenuation_Linear
        //! \param quadratic attenuation quadratic factor. Default: Default_Attenuation_Quadratic
        PointLight
        (
            glm::vec3 pos, 
            glm::vec3 ambientColor = Default_Ambient_Color, 
            glm::vec3 diffuseColor = Default_Diffuse_Color, 
            glm::vec3 specularColor = Default_Specular_Color,
            float constant = Default_Attenuation_Constant, 
            float linear =   Default_Attenuation_Linear, 
            float quadratic =     Default_Attenuation_Quadratic
        );
    
    #pragma region Inline functions to generate names for shader variables

        //! \brief function to generate name of position for shader variable
        inline static std::string GetPositionName();
        //! \brief function to generate name of position with given index for shader variable
        //! \param index index of point light
        inline static std::string GetPositionName(int index);

        //! \brief function to generate name of ambient color for shader variable
        inline static std::string GetAmbientName();
        //! \brief function to generate name of ambient color with given index for shader variable
        //! \param index index of point light
        inline static std::string GetAmbientName(int index);
        //! \brief function to generate name of diffuse color for shader variable
        inline static std::string GetDiffuseName();
        //! \brief function to generate name of diffuse color with given index for shader variable
        //! \param index index of point light
        inline static std::string GetDiffuseName(int index);
        //! \brief function to generate name of specular color for shader variable
        inline static std::string GetSpecularName();
        //! \brief function to generate name of specular color with given index for shader variable
        //! \param index index of point light
        inline static std::string GetSpecularName(int index);

        //! \brief function to generate name of attenuation constant factor for shader variable
        inline static std::string GetConstantName();
        //! \brief function to generate name of attenuation constant factor with given index for shader variable
        //! \param index index of point light
        inline static std::string GetConstantName(int index);
        //! \brief function to generate name of attenuation linear factor for shader variable
        inline static std::string GetLinearName();
        //! \brief function to generate name of attenuation linear factor with given index for shader variable
        //! \param index index of point light
        inline static std::string GetLinearName(int index);
        //! \brief function to generate name of attenuation quadratic factor for shader variable
        inline static std::string GetQuadraticName();
        //! \brief function to generate name of attenuation quadratic factor with given index for shader variable
        //! \param index index of point light
        inline static std::string GetQuadraticName(int index);
#pragma endregion
    
        //! \brief set basic light properties in given shader
        //! \param shader shader to set parameters in
        void SetBasicLighting(Shader shader);

        //! \brief set basic light properties in given shader
        //! \param shader shader to set parameters in
        //! \param pos position of point light
        void SetBasicLighting(Shader shader, glm::vec3 pos);

        //! \brief set basic light properties in given shader
        //! \param shader shader to set parameters in
        //! \param pl PointLight object
        //! \param diffColor diffuse color value
        void SetBasicLight(Shader shader, PointLight pl, glm::vec3 diffColor);
    
        //! \brief set light properties in given shader
        //! \param shader shader to set parameters in
        void SetPointLight(Shader shader);

        //! \brief set light properties in given shader
        //! \param shader shader to set parameters in
        //! \param pos position of point light
        void SetPointLight(Shader shader, glm::vec3 pos);

        //! \brief set light properties in given shader
        //! \param shader shader to set parameters in
        //! \param pos position of point light
        //! \param index index of point light
        void SetPointLight(Shader shader, glm::vec3 pos, size_t index);

        //! \brief set light properties in given shader
        //! \param shader shader to set parameters in
        //! \param ambColor ambient color
        //! \param diffColor diffuse color
        void SetPointLight(Shader shader, glm::vec3 ambColor, glm::vec3 diffColor);
        
    
        glm::vec3 _position; //!< position
    
        glm::vec3 _ambientColor;    //!< ambient color
        glm::vec3 _diffuseColor;    //!< diffuse color
        glm::vec3 _specularColor;   //!< specular color
    
        float _constant;            //!< attenuation constant factor
        float _linear;              //!< attenuation linear factor
        float _quadratic;           //!< attenuation quadrtic factor
    
    private:
        static const std::string _PointLightMultipleName;   //!< name template of pointlight in shader if multiple are used
        static const std::string _PointLightSingleName;     //!< name template of pointlight in shader
};

}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_POINTLIGHT_H

