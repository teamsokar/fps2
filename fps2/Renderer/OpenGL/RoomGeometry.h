#ifndef SKR_FPS2_RENDERER_OPENGL_ROOM_H
#define SKR_FPS2_RENDERER_OPENGL_ROOM_H

#include "..\..\pch.h"
#include "pcg\Room.h"

#include "PointLight.h"
#include "Mesh.h"
#include "..\..\Collision\CollisionBox.h"
#include "TargetGeometry.h"
//#include "ShadowMap.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class RoomGeometry
    //! \brief holds geometry of a room
    class RoomGeometry
    {

    public:
        skr::pcg::Room _room;   //!< room from procedural generation
        uint32_t _VAO;          //!< vertex array object
        uint32_t _VBO;          //!< vertex buffer object
        std::vector<PointLight> _PointLights;   //!< list of point lights in this room
        
        // Shadow Map
        uint32_t _depthMapFBO;  //!< frame buffer to render depth map to, used in shadow mapping
        uint32_t _depthCubeMap; //!< cube map texture for depth map, used in shadow mapping

        uint32_t _numVertices;  //!< number of vertices of geometry
        std::vector<Mesh> _meshes;  //!< list of meshes
        ShaderType _shadertype;     //!< type of shader used to draw geometry
        MaterialParameters _material;   //!< material used to draw geometry

        std::vector<skr::fps2::Collision::CollisionBox*> _collisionObjects; //!< list of collisions objects
        std::vector<TargetGeometry> _targets;   //!< list of targets in this room

        //! \brief default constructor
        //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
        RoomGeometry()
        : _VAO(0), _VBO(0), _numVertices(0), _shadertype(ShaderType::Main), _depthMapFBO(0), _depthCubeMap(0)
        {};

        //! \brief constructor
        //! \param room room from procedural generation
        //! \param shadertype type of shader used to draw geometry. Default: Main
        RoomGeometry(skr::pcg::Room room, ShaderType shadertype = ShaderType::Main);

        //! \brief destructor
        ~RoomGeometry();

        //! \brief initializes framebuffer and cubemap to render depthmap
        void PrepareDepthMaps();

        //! \brief renders depthmap
        //! \param shader shader to draw depthmap. Assuming ShaderType::DepthShadow
        //! \param light_movement movement of point light in current frame from position
        //! \param currentTime current frame time
        void RenderDepthMap(Shader shader, glm::vec3 light_movement, float currentTime = 0.0f);
    };
}
}
}
}

#endif //! SKR_FPS2_RENDERER_OPENGL_ROOM_H
