#include "..\..\pch.h"
#include "Settings.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>

namespace skr
{
namespace fps2 
{

#pragma region Mapping table definitions
const Settings::StringToSettingIntMap Settings::MapStringToSettingInt =
{
    {"scr_width", SettingUInt32t::SCR_WIDTH},
    {"scr_height", SettingUInt32t::SCR_HEIGHT},
    {"rng_mode", SettingUInt32t::RNG_MODE},
    {"rng_seed", SettingUInt32t::RNG_SEED},
};

const Settings::SettingIntToStringMap Settings::MapSettingIntToString =
{
    {SettingUInt32t::SCR_WIDTH, "scr_width"},
    {SettingUInt32t::SCR_HEIGHT, "scr_height"},
    {SettingUInt32t::RNG_MODE, "rng_mode"},
    {SettingUInt32t::RNG_SEED, "rng_seed"},
};

const Settings::SettingFloatToStringMap Settings::MapSettingFloatToString =
{
     {SettingFloat::RND_NEAR, "rnd_near"},
     {SettingFloat::RND_FAR, "rnd_far"},
};

const Settings::StringToSettingFloatMap Settings::MapStringToSettingFloat =
{
      {"rnd_near", SettingFloat::RND_NEAR},
      {"rnd_far", SettingFloat::RND_FAR},
};

const Settings::SettingBoolToStringMap Settings::MapSettingBoolToString =
{
    {SettingBool::RND_VSYNC, "rnd_vsync"},
    {SettingBool::RNG_PRINTSEED, "rng_printseed"},
    {SettingBool::RND_GAMMACORRECTION, "rnd_gammacorrection"},
    {SettingBool::RND_COLORCODEDROOMS, "rnd_colorcoderooms"},
};

const Settings::StringToSettinBoolMap Settings::MapStringToSettingBool =
{
    {"rnd_vsync", SettingBool::RND_VSYNC},
    {"rng_printseed", SettingBool::RNG_PRINTSEED},
    {"rnd_gammacorrection", SettingBool::RND_GAMMACORRECTION},
    {"rnd_colorcoderooms", SettingBool::RND_COLORCODEDROOMS}
};
#pragma endregion

Settings& Settings::GetInstance()
{
    static Settings instance;
    return instance;
}

bool Settings::WriteSettingsToFile()
{
    return WriteSettingsToFile(_settingsFilePath);
}

bool Settings::WriteSettingsToFile(const std::string path)
{
    std::fstream fs;
    fs.open(path, std::ios::out);
    if (!fs.is_open())
    {
        std::cout << "ERROR; unable to write settings to file: " << path << std::endl;
        return false;;
    }


    fs.clear();

    for (auto seti : _settings_uint32t)
    {
        auto[key, value] = seti;
        auto idInt = MapSettingIntToString.find(key);
        if (idInt != MapSettingIntToString.end())
        {
            fs << idInt->second << ' ' << value << std::endl;
        }
        else
        {
            std::cout << "ERROR: unable to find integer key" << std::endl;
        }
    }

    for (auto setf : _settings_float)
    {
        auto[key, value] = setf;
        auto idFloat = MapSettingFloatToString.find(key);
        if (idFloat != MapSettingFloatToString.end())
        {
            fs << idFloat->second << ' ' << value << std::endl;
        }
        else
        {
            std::cout << "ERROR: unable to find float key" << std::endl;
        }
    }

    for (auto setb : _settings_bool)
    {
        auto[key, value] = setb;
        auto idBool = MapSettingBoolToString.find(key);
        if (idBool != MapSettingBoolToString.end())
        {
            fs << idBool->second << ' ' << value << std::endl;
        }
        else
        {
            std::cout << "ERROR: unable to find boolean key" << std::endl;
        }
    }

    fs.close();
    return true;
}

bool Settings::ReadSettingsFromFile()
{
    if (!std::filesystem::exists(_settingsFilePath))
    {
        std::cout << "WARNING: could not read default settings file " << _settingsFilePath << ". Using default values" << std::endl;
        return false;
    }

    return ReadSettingsFromFile(_settingsFilePath);
}

bool Settings::ReadSettingsFromFile(const std::string path)
{
    if (!std::filesystem::exists(path))
    {
        std::cout << "WARNING: could not read settings file " << path << ". Using default values" << std::endl;
        return false;
    }

    _settingsFilePath = path;
    std::vector<std::string> settings;
    if (!GetSettingsFromFile(path, settings))
    {
        std::cout << "ERROR: unable to read settings due to previous error" << std::endl;
        return false;
    }

    if (!SetSettingsFromString(settings))
    {
        std::cout << "ERROR: unable to set settings due to previous error" << std::endl;
        return false;
    }

    return true;
}

std::string Settings::PrintSettingsToString()
{
    std::string settings;
    for (auto s : _settings_uint32t)
    {
        auto id = MapSettingIntToString.find(s.first);
        if (id != MapSettingIntToString.end())
        {
            settings.append(id->second + ' ' + std::to_string(s.second) + '\n');
        }
    }

    for (auto s : _settings_float)
    {
        auto id = MapSettingFloatToString.find(s.first);
        if (id != MapSettingFloatToString.end())
        {
            settings.append(id->second + ' ' + std::to_string(s.second) + '\n');
        }
    }

    for (auto s : _settings_bool)
    {
        auto id = MapSettingBoolToString.find(s.first);
        if (id != MapSettingBoolToString.end())
        {
            settings.append(id->second + ' ' + GetHumanReadableBool(s.second) + '\n');
        }
    }

    return settings;
}

bool Settings::GetSettingsFromFile(const std::string path, std::vector<std::string>& settings)
{
    std::fstream fs;
    fs.open(path, std::ios::in);

    if (!fs.is_open())
    {
        std::cout << "ERROR: could not open settings file: " << path << std::endl;
        return false;
    }

    std::string inputStr;
    while (!fs.eof())
    {
        std::getline(fs, inputStr);
        if (inputStr.empty())
            continue;

        settings.push_back(inputStr);
    }

    return true;
}

bool Settings::SetSettingsFromString(std::vector<std::string> rawsettings)
{
    std::vector<std::tuple<std::string, std::string>> settingsTuples;

    for (auto s : rawsettings)
    {
        std::vector<std::string> singleSetting;

        auto pos = s.find(' ');
        std::string settingType = s.substr(0, pos);
        std::string settingValue = s.substr(pos + 1, s.length() - pos);
        settingsTuples.push_back({ settingType, settingValue });
    }

    for (auto s : settingsTuples)
    {
        auto[keyStr, value] = s;

        auto idInt = MapStringToSettingInt.find(keyStr);
        if (idInt != MapStringToSettingInt.end())
        {
            _settings_uint32t[idInt->second] = std::stoul(value);
            continue;
        }
        else
        {
            auto idFloat = MapStringToSettingFloat.find(keyStr);
            if (idFloat != MapStringToSettingFloat.end())
            {
                _settings_float[idFloat->second] = std::stof(value);
                continue;
            }
            else
            {
                auto idBool = MapStringToSettingBool.find(keyStr);
                if (idBool != MapStringToSettingBool.end())
                {
                    _settings_bool[idBool->second] = std::stoul(value);
                    continue;
                }
            }
        }

        std::cout << "ERROR: unable to find setting key: " << keyStr << std::endl;
    }

    return true;
}

std::string Settings::GetHumanReadableBool(bool v)
{
    return v ? "true" : "false";
}

}
}
