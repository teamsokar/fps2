#include "../../pch.h"
#include "TargetGeometry.h"

#include "../../AssetPath.h"
#include "ModelCache.h"
#include "pcg/Definitions.h"

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{

TargetGeometry::TargetGeometry(skr::pcg::Target target, uint32_t id)
    : _target(target), _id(id), _completed(false),
      _position(glm::vec3(0.0f)),
      _smpMaterial(RENDER_TARGET_COLOR * 0.5f, RENDER_TARGET_COLOR, glm::vec3(0.5f), 32.0f),
      _movement(glm::vec3(0.0f))
{
    auto mid = id % _modelList.size();

    _modelId = ModelCache::GetInstance().AddModel(GetAssetPath(_modelList[mid].path), glm::vec3(_modelList[mid].scale), _modelList[mid].posOffset.y);
    _position = glm::vec3(target._position[0], target._position[1], target._position[2]) - _modelList[mid].posOffset;
}

void TargetGeometry::SetCompleted(bool value)
{
    _completed = value;

    if (_completed)
    {
        for (auto& m : ModelCache::GetInstance().GetModel(_modelId)._meshes)
        {
            _smpMaterial = MaterialParameters(RENDER_TARGET_HIT_COLOR * 0.2f, RENDER_TARGET_HIT_COLOR * 0.8f, glm::vec3(0.25f), 2048.0f);
        }
    }
    else
    {
        for (auto& m : ModelCache::GetInstance().GetModel(_modelId)._meshes)
        {
            _smpMaterial = MaterialParameters(glm::vec3(1.0f) * 0.2f, glm::vec3(1.0f) * 0.8f, glm::vec3(0.25f), 32.0f);
        }
    }
}

glm::vec3 TargetGeometry::CalculateMovement()
{
    return CalculateMovement(glfwGetTime());
}

glm::vec3 TargetGeometry::CalculateMovement(double currentTime)
{
    if (_completed)
    {
        return _movement;
    }

    float movX = sin((float)currentTime * skr::pcg::PCG_DEFAULT_TARGET_MAX_MOVEMENT_SPEED) * _target._movement.extend;
    float movY = sin((float)currentTime * skr::pcg::PCG_DEFAULT_TARGET_MAX_MOVEMENT_SPEED) * _target._movement.extend;
    float movZ = cos((float)currentTime * skr::pcg::PCG_DEFAULT_TARGET_MAX_MOVEMENT_SPEED) * _target._movement.extend;

    glm::vec3 movement(_target._movement.axis[0] * movX, _target._movement.axis[1] * movY, _target._movement.axis[2] * movZ);

    for (auto& c : _collisionObjects)
    {
        c->UpdatePosition(c->_position + _movement);
    }

    return movement;
}

}
}
}
}
