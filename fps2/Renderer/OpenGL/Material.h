#ifndef SKR_FPS2_RENDERER_OPENGL_MATERIAL_H
#define SKR_FPS2_RENDERER_OPENGL_MATERIAL_H

#include "..\..\pch.h"
#include "Shader.h"
namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class Material
    //! \brief interface for material types
    class Material
    {
    public:
        virtual void ApplyMaterial(Shader shader) = 0;
    };

}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_MATERIAL_H
