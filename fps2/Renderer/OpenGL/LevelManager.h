#ifndef SKR_FPS2_RENDERER_OPENGL_LEVELMANAGER_H
#define SKR_FPS2_RENDERER_OPENGL_LEVELMANAGER_H

#include "..\..\pch.h"

#include <vector>

#include "RoomGeometry.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{

    //! \class LevelManager
    //! \brief holds all rooms in a central place
    /*! Implements Scott Meyers' singleton pattern */
    class LevelManager
    {
    private:
        //! \brief default constructor
        LevelManager() 
        : _initialPosition(glm::vec3(0.0f, 0.0f, 0.0f))
        {};
    
        //! \brief copy constructor is deleted
        LevelManager(const LevelManager&) = delete;

        //! \brief copy assignment operator is deleted
        LevelManager operator=(const LevelManager&) = delete;
    
        glm::vec3 _initialPosition; //!< initial player position
    
    public:
        std::vector<RoomGeometry> _rooms; //!< list of rooms
    
        //! \brief Get only instance of LevelManger
        //! Implements Scott Meyers' singleton pattern
        static LevelManager& GetInstance()
        {
            static LevelManager instance;
            return instance;
        }
    
        //! \brief add a room
        //! \param r room to add
        void AddRoom(RoomGeometry r)
        {
            _rooms.push_back(r);
        }
    
        //! \brief sets initial position
        //! \param pos new initial position
        void SetInitialPosition(glm::vec3 pos)
        {
            _initialPosition = pos;
        }
    
        //! \brief gets initial player position
        //! \return initial player position
        glm::vec3 GetInitialPosition()
        {
            return _initialPosition;
        }
    
        //! \brief get rooms adjecent to given room
        //! \param room given room
        //! \return list of adjecent room
        std::vector<skr::pcg::Room> GetAdjecentRooms(skr::pcg::Room room)
        {
            std::vector<skr::pcg::Room> rooms;
    
            for (auto& ar : room._adjecentRooms)
            {
                for (auto& r : _rooms)
                {
                    if (ar._center == r._room._center)
                        rooms.push_back(r._room);
                }
            } 
            return rooms;
        }
    
        //! \brief get rooms adjecent to given room
        //! \param room given room
        //! \return list of adjecent room
        std::vector<skr::pcg::Room> GetAdjecentRooms(RoomGeometry room)
        {
            return GetAdjecentRooms(room._room);
        }
    
        //! \brief get rooms adjecent to given room
        //! \param room given room
        //! \return list of adjecent room
        std::vector<RoomGeometry> GetAdjecentRoomGeometries(skr::pcg::Room room)
        {
            std::vector<RoomGeometry> rooms;
    
            for (auto& ar : room._adjecentRooms)
            {
                for (auto& r : _rooms)
                {
                    if (ar._center == r._room._center)
                        rooms.push_back(r);
                }
            }
    
            return rooms;
        }
    
        //! \brief get rooms adjecent to given room
        //! \param room given room
        //! \return list of adjecent room
        std::vector<RoomGeometry> GetAdjecentRoomGeometries(RoomGeometry room)
        {
            std::vector<RoomGeometry> rooms;
    
            for (auto& ar : room._room._adjecentRooms)
            {
                for (auto& r : _rooms)
                {
                    if (ar._center == r._room._center) {
                        rooms.push_back(r);
                    }
                }
            }
    
            return rooms;
        }
    };

}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_LEVELMANAGER_H

