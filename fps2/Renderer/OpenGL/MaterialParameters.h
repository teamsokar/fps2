#ifndef SKR_FPS2_RENDERER_OPENGL_SIMPLEMATERIAL_H
#define SKR_FPS2_RENDERER_OPENGL_SIMPLEMATERIAL_H

#include "..\..\pch.h"
#include "Material.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class MaterialParameters
    //! \brief holding material parameters for objects without textures
    class MaterialParameters : public Material
    {
    public:
        glm::vec3 ambient;  //!< ambient color
        glm::vec3 diffuse;  //!< diffuse color
        glm::vec3 specular; //!< specular color
        float shininess;    //!< shininess factor

    public:
        //! \brief default constructor
        MaterialParameters();

        //! \brief constructor
        //! \param amb ambient color
        //! \param diff diffuse color
        //! \param spec specular color
        //! \param shine shininess. Default: 32.0f
        MaterialParameters(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, float shine = 32.0f);

        //! \brief set material properties
        //! \param amb ambient color
        //! \param diff diffuse color
        //! \param spec specular color
        //! \param shine shininess. Default: 32.0f
        void SetMaterialProperties(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, float shine = 32.0f);

        //! \brief apply material to object through given shader
        //! \param shader Shader to draw object with
        void ApplyMaterial(Shader shader) override;

        //! \brief apply shininess to object through given shader
        //! \param shader Shader to draw object with
        void ApplyShininess(Shader shader);
    };
}
}
}
}
#endif // !SKR_FPS2_RENDERER_OPENGL_SIMPLEMATERIAL_H

