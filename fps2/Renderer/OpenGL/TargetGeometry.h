#ifndef SKR_FPS2_RENDERER_OPENGL_TARGETGEOMETRY
#define SKR_FPS2_RENDERER_OPENGL_TARGETGEOMETRY

#include "../../pch.h"

#include "pcg/Target.h"
#include "Mesh.h"
#include "Model.h"
#include "..\..\Collision\CollisionBox.h"
#include "MaterialParameters.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class TargetModel
    //! \brief model for a target
    struct TargetModel
    {
        std::string path;       //!< path to where model is loaded from
        float scale;            //!< scale of model
        glm::vec3 posOffset;    //!< offset of meshes from center of model
    };

    //! \class TargetGeometry
    //! \brief manages geometry of a target
    class TargetGeometry
    {
    public:
        skr::pcg::Target _target;   //!< target from procedural generation
        uint32_t _id;               //!< id of target
        
        glm::vec3 _position;        //!< position
        bool _completed;            //!< completion status
        glm::vec3 _movement;        //!< movement vector

        MaterialParameters _smpMaterial;    //!< material used to draw model

        size_t _modelId;            //!< id of model used for target

        std::vector<skr::fps2::Collision::CollisionBox*> _collisionObjects; //!< collision objects of target

    public:
        //! \brief constructor
        //! \param target target from procedural generation
        //! \param id id of target
        TargetGeometry(skr::pcg::Target target, uint32_t id);

        //! \brief set completed status
        //! \param value status value
        void SetCompleted(bool value);

        //! \brief calculate movement in current frame
        //! also updates position of collision object
        //! if target is already complete, position is kept at last befor completen -> stops movement on completion
        //! \return vector with movemnt relativ to position in current frame
        glm::vec3 CalculateMovement();

        //! \brief calcualte movement in given (assumed) frame
        //! also updates position of collision object
        //! if target is already complete, position is kept at last befor completen -> stops movement on completion
        //! \return vector with movemnt relativ to position in given frame
        glm::vec3 CalculateMovement(double currentTime);

    private:
        //! \brief list to chose models from. 
        //! models are not set during procedural generation, but randomly choosen from this list
        std::vector<TargetModel> _modelList
        { 
            {"models/container/container.obj", 1.0f, glm::vec3(0.0f)},
            {"models/nanosuit/nanosuit.obj", 0.25f, glm::vec3(0.0f, 2.49f, 0.0f)}
        };
    };
}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_TARGETGEOMETRY

