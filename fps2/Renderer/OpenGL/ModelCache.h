#ifndef SKR_FPS2_RENDERER_OPENGL_MODELCACHE_H
#define SKR_FPS2_RENDERER_OPENGL_MODELCACHE_H

#include "..\..\pch.h"
#include <assert.h>

#include "Model.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class ModelCache
    //! \brief manages loaded models in a central place
    //! Implements Scott Meyers' singleton pattern
    //! currently, ModelCache is not designed to load and unload models freely at runtime
    class ModelCache
    {
    public:
        //! \brief gets only instance of ModelCache
        //! Implements Scott Meyers' singleton pattern
        static ModelCache& GetInstance()
        {
            static ModelCache instance;
            return instance;
        }
    
        //! \brief add model to cache
        //! if model is already added, return its id. If not, model is added and its new id is returned
        //! \param model Model
        //! \return id
        size_t AddModel(Model model)
        {
            size_t id;
            if (!CheckIfModelAlreadyAdded(model._path, id))
            {
                id = _nextModelId;
                _modelCache.insert({ id, std::move(model) });
                _nextModelId++;
                return id;
            }
            else
            {
                return id;
            }
        }
    
        //! \brief add model to cache
        //! If model is already in cache, skip loading and return id of model. Otherwise, load model and return the new id.
        //! \param modelDir direction containig model data
        //! \param scale scale of the model. Default: 1.0
        //! \param yoffset if coordinates of meshes are not set from the center of the model, use this value to offset its rendering. Default: 0.0f
        //! \param shadertype Type of Shader used to draw the model. Default: Main
        //! \return id of model
        size_t AddModel(std::string modelDir, glm::vec3 scale = glm::vec3(1.0f), float yoffset = 0.0f, ShaderType shadertype = ShaderType::Main)
        {
            size_t id;
            if (!CheckIfModelAlreadyAdded(modelDir, id))
            {
                Model model(modelDir, scale, yoffset, shadertype);
                return AddModel(std::move(model));
            }
            else
            {
                return id;
            }
        }
    
        //! \brief get reference to model of specified id
        //! \param id id of model
        //! \return reference requested model
        Model& GetModel(size_t id)
        {
            // TODO bounds check?
            return _modelCache[id];
        }
    
    private:
        //! \brief default constructor
        //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
        ModelCache()
            : _nextModelId(0)
        {};
    
        //! \brief copy constructor is deleted due to singleton pattern
        ModelCache(const ModelCache&) = delete;

        //! \brief copy assignment operator is deleted due to singleton pattern
        ModelCache operator=(const ModelCache&) = delete;
    
        //! map containing models and there ids as key
        std::unordered_map<size_t, Model> _modelCache;

        //! id of the next model
        size_t _nextModelId = 1;
    
        //! \brief checks if a given model is already present in the cache, based on its path
        //! \param path Path to new model
        //! \param [out] id id of new model. Not set if check fails
        //! \return true if model is already present, id has its id. Otherwise false and id is not set
        bool CheckIfModelAlreadyAdded(std::string path, size_t& id)
        {
            for (auto& m : _modelCache)
            {
    #ifdef _DEBUG
                assert(!m.second._path.empty()); // model path string is not allowed    to be empty empty
    #endif // _DEBUG
    
                if (m.second._path == path)
                {
                    id = m.first;
                    return true;
                }
            }
    
            return false;
        }
    };

}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_MODELCACHE_H
