#include "..\..\pch.h"
#include "PointLight.h"

#include <glm/gtc/matrix_transform.hpp>
#include "RendererOpenGL.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{

const std::string PointLight::_PointLightMultipleName = "pointLights[";
const std::string PointLight::_PointLightSingleName = "pointLight";

#pragma region Constructors

PointLight::PointLight(std::array<float, 3> pos, glm::vec3 ambientColor, glm::vec3 diffuseColor, glm::vec3 specularColor, float constant, float linear, float quadratic)
    : _position(pos[0], pos[1], pos[2]),
      _ambientColor(ambientColor), _diffuseColor(diffuseColor), _specularColor(specularColor),
      _constant(constant), _linear(linear), _quadratic(quadratic)
{
}

PointLight::PointLight(glm::vec3 pos, glm::vec3 ambientColor, glm::vec3 diffuseColor, glm::vec3 specularColor, float constant, float linear, float quadratic)
    : _position(pos), 
    _ambientColor(ambientColor), _diffuseColor(diffuseColor), _specularColor(specularColor),
    _constant(constant), _linear(linear), _quadratic(quadratic)
{
}

#pragma endregion

#pragma region Inline functions to generate names for shader variables
inline std::string PointLight::GetPositionName()
{
    return std::string (_PointLightSingleName + ".position");
}

std::string PointLight::GetPositionName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].position");
}

inline std::string PointLight::GetAmbientName()
{
    return std::string(_PointLightSingleName + ".ambient");
}

std::string PointLight::GetAmbientName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].ambient");
}

inline std::string PointLight::GetDiffuseName()
{
    return std::string(_PointLightSingleName + ".diffuse");
}

std::string PointLight::GetDiffuseName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].diffuse");
}

inline std::string PointLight::GetSpecularName()
{
    return std::string(_PointLightSingleName + ".specular");
}

std::string PointLight::GetSpecularName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].specular");
}

inline std::string PointLight::GetConstantName()
{
    return std::string(_PointLightSingleName + ".constant");
}

std::string PointLight::GetConstantName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].constant");
}

inline std::string PointLight::GetLinearName()
{
    return std::string(_PointLightSingleName + ".linear");
}

std::string PointLight::GetLinearName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].linear");
}

inline std::string PointLight::GetQuadraticName()
{
    return std::string(_PointLightSingleName + "+quadratic");
}

std::string PointLight::GetQuadraticName(int index)
{
    return std::string(_PointLightMultipleName + std::to_string(index) + "].quadratic");
}
#pragma endregion

#pragma region Basic Light
void PointLight::SetBasicLighting(Shader shader)
{
    SetBasicLighting(shader, _position);
}

void PointLight::SetBasicLighting(Shader shader, glm::vec3 pos)
{
    glm::mat4 modelMat = glm::mat4(1.0f);
    modelMat = glm::translate(modelMat, pos);

    shader.SetVec3("lightPos", pos);

    shader.SetVec3("lightColor", _diffuseColor);
    shader.SetVec3("ambientColor", _ambientColor);
}

void PointLight::SetBasicLight(Shader shader, PointLight pl, glm::vec3 diffColor)
{
    shader.SetVec3("lightColor", diffColor);
    shader.SetVec3("lightPos", pl._position);
    shader.SetVec3("ambientColor", diffColor * glm::vec3(0.1f));
}
#pragma endregion

#pragma region Material-based PointLight

void PointLight::SetPointLight(Shader shader)
{
    SetPointLight(shader, _position);
}

void PointLight::SetPointLight(Shader shader, glm::vec3 pos)
{
    glm::mat4 modelMat = glm::mat4(1.0f);
    modelMat = glm::translate(modelMat, pos);

    shader.SetVec3(GetPositionName(), pos);
    shader.SetVec3(GetAmbientName(), _ambientColor);
    shader.SetVec3(GetDiffuseName(), _diffuseColor);

    shader.SetFloat(GetConstantName(), _constant);
    shader.SetFloat(GetLinearName(), _linear);
    shader.SetFloat(GetQuadraticName(), _quadratic);
}

void PointLight::SetPointLight(Shader shader, glm::vec3 pos, size_t index)
{
    glm::mat4 modelMat = glm::mat4(1.0f);
    modelMat = glm::translate(modelMat, pos);

    shader.SetVec3(GetPositionName(index), pos);
    shader.SetVec3(GetAmbientName(index), _ambientColor);
    shader.SetVec3(GetDiffuseName(index), _diffuseColor);

    shader.SetFloat(GetConstantName(index), _constant);
    shader.SetFloat(GetLinearName(index), _linear);
    shader.SetFloat(GetQuadraticName(index), _quadratic);
}

void PointLight::SetPointLight(Shader shader, glm::vec3 ambColor, glm::vec3 diffColor)
{
    glm::mat4 modelMat = glm::mat4(1.0);
    modelMat = glm::translate(modelMat, _position);

    shader.SetVec3(GetPositionName(), _position);
    shader.SetVec3(GetAmbientName(), ambColor);
    shader.SetVec3(GetDiffuseName(), diffColor);

    shader.SetFloat(GetConstantName(), _constant);
    shader.SetFloat(GetLinearName(), _linear);
    shader.SetFloat(GetQuadraticName(), _quadratic);
}

#pragma endregion
}
}
}
}
