#include "..\..\pch.h"

#include "reactphysics3d.h"
#include <glm/gtc/matrix_transform.hpp>

#include "RoomGeometry.h"
#include "Settings.h"
#include "ModelCache.h"

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{

RoomGeometry::RoomGeometry(skr::pcg::Room room, ShaderType shadertype)
    : _room(room), _shadertype(shadertype),
      _VAO(0), _VBO(0),_numVertices(0),
      _depthMapFBO(0), _depthCubeMap(0)
{
    auto geom = room.GenerateGeometry3D();
    //auto geom2 = room.GenerateGeometry3DIndexed();

    for (auto& g : geom)
    {
        _meshes.push_back(Mesh(g));
    }

#ifdef _DEBUG
    assert(_meshes.size() > 0); // RoomGeometry needs to have some meshes
#endif // _DEBUG

    if(Settings::GetInstance()._settings_bool[Settings::SettingBool::RND_COLORCODEDROOMS])
        _material = MaterialParameters(_meshes[0]._drawColor * glm::vec3(0.2f), _meshes[0]._drawColor, glm::vec3(0.0f), 16.0f);
    else
        _material = MaterialParameters(glm::vec3(1.0f), glm::vec3(1.0f), glm::vec3(0.0f), 16.0f);

    PrepareDepthMaps();
}

RoomGeometry::~RoomGeometry()
{
    glDeleteVertexArrays(1, &_VAO);
    glDeleteBuffers(1, &_VBO);
}

void RoomGeometry::PrepareDepthMaps()
{
    glGenFramebuffers(1, &_depthMapFBO);
    glGenTextures(1, &_depthCubeMap);

    glBindTexture(GL_TEXTURE_CUBE_MAP, _depthCubeMap);

    for (size_t i(0); i < 6; ++i)
    {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, _depthMapFBO);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthCubeMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RoomGeometry::RenderDepthMap(Shader shader, glm::vec3 light_movement, float currentTime)
{
    // Assuming glViewport has been set correctly - (0, 0, SHADOW_WIDHT, SHADOW_HEIGHT)
    glBindFramebuffer(GL_FRAMEBUFFER, _depthMapFBO);

    glClear(GL_DEPTH_BUFFER_BIT);

    // Configure matrices
    float aspect = (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT;
    float nearPlane = Settings::GetInstance()._settings_float[Settings::SettingFloat::RND_NEAR];
    float farPlane = Settings::GetInstance()._settings_float[Settings::SettingFloat::RND_FAR];
    glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, nearPlane, farPlane);

    glm::vec3 lightPos = _PointLights[0]._position + light_movement;

    std::vector<glm::mat4> shadowTransforms;
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
    shadowTransforms.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));

    // Configure shader
    // Assuming ShaderType::DepthShadow
    shader.Use();

    for (size_t i(0); i < 6; ++i)
    {
        shader.SetMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
    }

    shader.SetFloat("far_plane", farPlane);
    shader.SetVec3("lightPos", lightPos);

    glm::mat4 model = glm::mat4(1.0f);
    shader.SetMat4("model", model);

    // Draw depthmap
    for (const auto& m : _meshes)
    {
        m.DrawGeometry();
    }

    for (const auto& t : _targets)
    {
        auto& t_model = ModelCache::GetInstance().GetModel(t._modelId);

        glm::mat4 modelMatTarget = glm::mat4(1.0f);
        modelMatTarget = glm::translate(modelMatTarget, t._position + t._movement);
        modelMatTarget = glm::scale(modelMatTarget, t_model._scale);

        shader.SetMat4("model", modelMatTarget);

        t_model.DrawGeometry();
    }
}

}
}
}
}
