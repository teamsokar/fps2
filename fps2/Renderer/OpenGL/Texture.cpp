#include "..\..\pch.h"
#include "Texture.h"

#include "Logger.h"

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{

//TODO i would like to keep the maps instead of single strings, because it would make them easier to organize. However, I need to access them without making a deep copy at every draw call, which tanks the framerate
//const TextureTypToStringMap Renderer::OpenGL::Texture::Texture::MapTextureTypToString =
//{
//    {TextureType::DIFFUSE,  "texture_diffuse"},
//    {TextureType::NORMAL,   "texture_normal"},
//    {TextureType::SPECULAR, "texture_specular"},
//    {TextureType::HEIGHT,   "texture_height"}
//};
//
//const StringToTextureTypeMap Renderer::OpenGL::Texture::MapStringToTextureType = 
//{
//    {"texture_diffuse",  TextureType::DIFFUSE},
//    {"texture_normal",   TextureType::NORMAL},
//    {"texture_specular", TextureType::SPECULAR},
//    {"texture_height",   TextureType::HEIGHT}
//};

const std::string Texture::TextureTypeDiffuse  = "texture_diffuse";
const std::string Texture::TextureTypeNormal   = "texture_normal";
const std::string Texture::TextureTypeSpecular = "texture_specular";
const std::string Texture::TextureTypeHeight   = "texture_height";

Texture::Texture(TextureType type, std::string path)
    : _type(type), _path(path)
{
    LoadTexture(_path);
}

std::vector<Texture> Texture::LoadMaterialTextures(std::vector<Texture>& loadedTextures, std::string dir, aiMaterial * aiMat, aiTextureType aiType, TextureType ttype)
{
    std::vector<Texture> newTextures;

    for (uint32_t i(0); i < aiMat->GetTextureCount(aiType); ++i)
    {
        aiString aiStr;
        aiMat->GetTexture(aiType, i, &aiStr);

        //check if texture is already loaded and if so skip it
        bool skip = false;
        for (uint32_t j(0); j < loadedTextures.size(); ++j)
        {
            if (std::strcmp(loadedTextures[j]._path.c_str(), aiStr.C_Str()) == 0)
            {
                newTextures.push_back(loadedTextures[j]);
                skip = true;
                break;
            }
        }

        if (!skip)
        {
            std::string filepath = dir + "/" + aiStr.C_Str();
            Texture tex(ttype, filepath);
            newTextures.push_back(tex);
            loadedTextures.push_back(tex);
        }
    }


    return newTextures;
}

bool Texture::LoadTexture(std::string path)
{
    glGenTextures(1, &_id);

    int height, width, nrComponents;

    unsigned char* data = stbi_load(path.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format = GL_RGB;
        switch (nrComponents)
        {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            std::cout << "ERROR: unknown number of components in texture " << path << std::endl;
            skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR: unknown number of components in texture " + path);

            stbi_image_free(data);
            return false;
        }

        glBindTexture(GL_TEXTURE_2D, _id);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTextureParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTextureParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTextureParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTextureParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "ERROR: unable to load texture: " << path << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR: unable to load texture: " + path);

        stbi_image_free(data);
        return false;
    }

    return true;
}

}
}
}
}
