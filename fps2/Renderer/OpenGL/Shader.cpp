#include "..\..\pch.h"

#include "Shader.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include "Definitions.h"
#include "Logger.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{

Shader::Shader(std::string vertpath, std::string fragpath, std::string geompath)
{
    auto vertexShader = LoadShader(vertpath);
    auto fragmentShader = LoadShader(fragpath);
    std::string geometryShader;
    
    if(!geompath.empty())
        geometryShader = LoadShader(geompath);

    uint32_t vertexShaderId = CompileShader(vertexShader, GL_VERTEX_SHADER);
    uint32_t fragmentShaderId = CompileShader(fragmentShader, GL_FRAGMENT_SHADER);
    uint32_t geometryShaderId;

    if(!geompath.empty())
        geometryShaderId = CompileShader(geometryShader, GL_GEOMETRY_SHADER);

    _id = glCreateProgram();
    glAttachShader(_id, vertexShaderId);
    glAttachShader(_id, fragmentShaderId);

    if(!geompath.empty())
        glAttachShader(_id, geometryShaderId);

    glLinkProgram(_id);
    CheckLinkErrors(_id);

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    if(!geompath.empty())
        glDeleteShader(geometryShaderId);
}

std::string Shader::LoadShader(std::string path)
{
    std::string code;
    std::ifstream shaderFile;

    shaderFile.exceptions(std::fstream::failbit | std::ifstream::badbit);

    try
    {
        shaderFile.open(path);
        std::stringstream shaderStream;
        shaderStream << shaderFile.rdbuf();
        shaderFile.close();

        code = shaderStream.str();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << "ERROR opneing shader file: " << path << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR opneing shader file: " + path);
    }

    return code;
}

uint32_t Shader::CompileShader(std::string code, GLenum type)
{
    uint32_t shader_id = glCreateShader(type);
    auto shaderCode = code.c_str();
    glShaderSource(shader_id, 1, &shaderCode, nullptr);
    glCompileShader(shader_id);
    CheckCompileErrors(shader_id);

    return shader_id;
}

void Shader::CheckCompileErrors(GLuint shader)
{
    GLint success;
    GLchar infoLog[LOGSIZE];

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, LOGSIZE, nullptr, infoLog);
        std::cout << "ERROR in Shader compilation" << std::endl << infoLog << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR in Shader compilation" + '\n\t' + std::string(infoLog));
    }
}

void Shader::CheckLinkErrors(GLuint shader)
{
    GLint success;
    GLchar infoLog[LOGSIZE];

    glGetShaderiv(shader, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, LOGSIZE, nullptr, infoLog);
        std::cout << "ERROR linking shader" << std::endl << infoLog << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR linking shader" + '\n\t' + std::string(infoLog));
    }
}

const void Shader::Use()
{
    glUseProgram(_id);
}

const void Shader::SetBool(std::string loc, bool value)
{
    glUniform1i(glGetUniformLocation(_id, loc.c_str()), (int)value);
}

const void Shader::SetFloat(std::string loc, float value)
{
    glUniform1f(glGetUniformLocation(_id, loc.c_str()), value);
}

const void Shader::SetInt(std::string loc, int value)
{
    glUniform1i(glGetUniformLocation(_id, loc.c_str()), value);
}

const void Shader::SetVec3(std::string loc, glm::vec3 value)
{
    glUniform3fv(glGetUniformLocation(_id, loc.c_str()), 1, &value[0]);
}

const void Shader::SetMat4(std::string loc, glm::mat4 value)
{
    glUniformMatrix4fv(glGetUniformLocation(_id, loc.c_str()), 1, GL_FALSE, &value[0][0]);
}

}
}
}
}
