#ifndef SKR_FPS2_RENDERER_OPENGL_GEOMETRY_BOUNDINGBOX
#define SKR_FPS2_RENDERER_OPENGL_GEOMETRY_BOUNDINGBOX

#include "..\..\pch.h"

#include <vector>

#include "RoomGeometry.h"
#include "pcg\Room.h"
#include "pcg\LevelBoundingBox.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class GeometryBoundingBox
    //! \brief bounding box of entire level geometry
    class GeometryBoundingBox
    {
    public:
        skr::pcg::LevelBoundingBox _boundingBox; //!< bounding box from level

        uint32_t _pointVAO; //!< vertex array object for center point
        uint32_t _pointVBO; //!< vertex buffer object for center point
        uint32_t _linesVAO; //!< vertex array object for lines to visualize size
        uint32_t _linesVBO; //!< vertex buffer object for lines to visualize size

        //! \brief default constructor
        //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
        GeometryBoundingBox();

        //! \brief constructor
        //! \param rooms list of rooms from procedural generation
        GeometryBoundingBox(std::vector<skr::pcg::Room> rooms);

    private:
        //! \brief create buffer to draw bounding box
        void CreateOpenGLGeometryBuffer();
    
    };
}
}
}
}

#endif //!SKR_FPS2_RENDERER_OPENGL_GEOMETRY_BOUNDINGBOX
