#include "../../pch.h"
#include "MaterialParameters.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    MaterialParameters::MaterialParameters()
        : ambient(glm::vec3(0.2f)),  diffuse(glm::vec3(0.8f)),  specular(glm::vec3(0.5f)), shininess(32.0f)
    {
    }

    MaterialParameters::MaterialParameters(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, float shine)
        : ambient(amb), diffuse(diff), specular(spec), shininess(shine)
    {
    }

    void MaterialParameters::SetMaterialProperties(glm::vec3 amb, glm::vec3 diff, glm::vec3 spec, float shine)
    {
        ambient = amb;
        diffuse = diff;
        specular = spec;
        shininess = shine;
    }

    void MaterialParameters::ApplyMaterial(Shader shader)
    {
        shader.SetVec3("smpMaterial.ambient", ambient);
        shader.SetVec3("smpMaterial.diffuse", diffuse);
        shader.SetVec3("smpMaterial.specular", specular);
        shader.SetFloat("smpMaterial.shininess", shininess);
    }
    void MaterialParameters::ApplyShininess(Shader shader)
    {
        shader.SetFloat("smpMaterial.shininess", shininess);
    }
}
}
}
}
