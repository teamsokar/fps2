#include "..\..\pch.h"
#include "Camera.h"
#include <iostream>

#include <glm/gtc/matrix_transform.hpp>

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{

glm::mat4 Camera::GetViewMatrix()
{
    return glm::lookAt(_position, _position + _front, _up);
}

void Camera::ProcessKeyboard(CameraMovement direction, float deltaTime, glm::vec3 collisionNormal, bool fly)
{
    float velocity = _movementSpeed * deltaTime;

    glm::vec3 inputVector(0.0f);

    switch (direction)
    {
    case CameraMovement::FORWARD:
        inputVector = _front;
        break;
    case CameraMovement::BACKWARD:
        inputVector = glm::vec3(-1.0f) * _front;
        break;
    case CameraMovement::RIGHT:
        inputVector = _right;
        break;
    case CameraMovement::LEFT:
        inputVector = glm::vec3(-1.0f) * _right;
    }

    // prevent movement in direction of collision
    glm::vec3 projection = glm::dot(inputVector, collisionNormal) * collisionNormal;
    auto projWallDir = glm::dot(collisionNormal, inputVector);

    if (projWallDir > 0.0f)
    {
        inputVector -= glm::dot(inputVector, collisionNormal) * collisionNormal;
    }

    _position += inputVector * velocity;

    if (!fly)
        _position.y = 0.0f;
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constraintPitch)
{
    xoffset *= _mouseSensitivity;
    yoffset *= _mouseSensitivity;

    _yaw += xoffset;
    _pitch += yoffset;

    if (constraintPitch)
    {
        if (_pitch > 89.0f)
            _pitch = 89.0f;
        if (_pitch < -89.0f)
            _pitch = -89.0f;
    }

    UpdateCameraVectors();
}

std::string Camera::GetCurrentPositionReadable()
{
    return std::string("PosX: " + std::to_string(_position.x) + " PosY: " + std::to_string(_position.y) + " PosZ: " + std::to_string(_position.z));
}

std::string Camera::GetCurrentParametersReadable()
{
    std::string params = "Camera Parameters: ";

    params.append(GetCurrentPositionReadable() +
        " UpX: " + std::to_string(_up.x) + " UpY: " + std::to_string(_up.y) + " UpZ: " + std::to_string(_up.z) +
        " Yaw: " + std::to_string(_yaw) + " Pitch: " + std::to_string(_pitch) + " FOV: " + std::to_string(_fov));

    return params;
}

void Camera::SetPosition(glm::vec3 pos)
{
    _position = pos;
    UpdateCameraVectors();
}

void Camera::SetParameters(glm::vec3 pos, glm::vec3 up, float yaw, float pitch, float fov)
{
    _position = pos;
    _up = up;
    _yaw = yaw;
    _pitch = pitch;
    _fov = fov;

    UpdateCameraVectors();
}

void Camera::UpdateCameraVectors()
{
    glm::vec3 front(cos(glm::radians(_yaw)) * cos(glm::radians(_pitch)), sin(glm::radians(_pitch)), sin(glm::radians(_yaw)) * cos(glm::radians(_pitch)));
    _front = glm::normalize(front);

    _right = glm::normalize(glm::cross(_front, _worldUp));
    _up = glm::normalize(glm::cross(_right, _front));

    //PrintCameraVectors();
}

void Camera::PrintCameraVectors()
{
        std::cout
            << "Position" << "\t\t" << "Front" << "\t\t" << "Up" << "\t\t" << "Right" << "\t\t" << "World Up" << std::endl
            << _position.x << ',' << _position.y << ',' << _position.z << '\t'
            << _front.x << ',' << _front.y << ',' << _front.z << "\t\t"
            << _up.x << ',' << _up.y << ',' << _up.z << "\t\t"
            << _right.x << ',' << _right.y << ',' << _right.z << "\t\t"
            << _worldUp.x << ',' << _worldUp.y << ',' << _worldUp.z << '\t'
            << std::endl;
}

}
}
}
}
