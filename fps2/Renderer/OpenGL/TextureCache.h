#ifndef SKR_FPS2_RENDERER_OPENGL_TEXTURECACHE_H
#define SKR_FPS2_RENDERER_OPENGL_TEXTURECACHE_H

#include "..\..\pch.h"

#include <vector>

#include "Texture.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class TextureCache
    //! \brief manages loaded textures in a central place
    //! Implements Scott Meyers' singleton pattern
    //! currently, TextureCache is not designed to load and unload textures freely at runtime
    class TextureCache
    {
    public:
        //! \brief gets only instance of TextureCache
        //! Implements Scott Meyers' singleton pattern
        static TextureCache& GetInstance()
        {
            static TextureCache instance;
            return instance;
        }

        //! list of loaded textures
        std::vector<Texture> cachedTextures;

    private:
        //! \brief default constructor
        //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
        TextureCache() {};

        //! \brief copy constructor is deleted due to singleton pattern
        TextureCache(const TextureCache&) = delete;

        //! \brief copy assignment operator is deleted due to singleton pattern
        TextureCache operator=(const TextureCache&) = delete;
    };
}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_TEXTURECACHE_H
