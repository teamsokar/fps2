#ifndef SKR_FPS2_RENDERER_OPENGL_RENDERER_H
#define SKR_FPS2_RENDERER_OPENGL_RENDERER_H

#include "..\..\pch.h"

#include <vector>

#include "Definitions.h"

#include "Settings.h"
#include "Logger.h"
#include "Texture.h"
#include "Cubemap.h"
#include "Model.h"
#include "MaterialParameters.h"

#include "Camera.h"
#include "PointLight.h"

#include "RoomGeometry.h"
#include "GeometryBoundingBox.h"
#include "..\..\Collision\CollisionManager.h"

#include "pcg/rng.h"

// callback function 
static void APIENTRY GLDebugMessageCallback(GLenum src, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* msg, const void* userParam);

//! \brief get errors from OpenGL state machine
//! encapusaltes glGetError()-call and generates readable output
static void GetGLError();

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! type identifies for vertex array objects
    enum struct VaoType
    { 
        Geometry,   //!< general geometry
        Screen,     //!< screen box
        Skybox      //!< skybox
    };
    //! type identifies for vertex buffer objects
    enum struct VboType
    {
        Geometry,   //!< general geometry
        Screen,     //!< screen box
        Skybox      //!< skybox
    };

    //! \class Renderer
    //! \brief OpenGL renderer
    class Renderer
    {
        private:
            uint32_t _framebuffer;          //!< id of main framebuffer
            uint32_t _framebufferTexture;   //!< id of main framebuffer texture
    
            size_t _debugLightModelId;      //!< id of model to visualize lights      
    
            Cubemap _skyboxCubemap;         //!< cubemap of skybox
            std::string _windowTitle;       //!< title of windows
    
            GeometryBoundingBox _geoBB;     //!< bounding box of entire level geometry
    
        public:
            GLFWwindow* _window;            //!< GLFW window
            std::unordered_map<VboType, uint32_t> _vertexBufferObjects; //!< map of vertex buffer objects
            std::unordered_map<VaoType, uint32_t> _vertexArrayObjects; //!< map of vertex array objects
            std::unordered_map<ShaderType, Shader> _shaders;    //!< map of shaders

            std::shared_ptr<Collision::CollisionManager> _collisionManager;  //!< collision manager
    
            std::shared_ptr<Camera> _camera;                 //!< camera
        
            float _deltatime = 0.0f;        //!< time between current and last frame
            float _lastFrame = 0.0f;        //!< time of last frame
       

        public:
            //! \brief default constructor
            //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
            Renderer();

            //! \brief constructor
            //! \param windowTitle title of window
            //! \param settingsfile settings file. Default: "settings.cfg"
            Renderer(std::string windowTitle, std::string settingsfile = "settings.cfg");

            //! \brief copy constructor is deleted
            Renderer(const Renderer&) = delete;

            //! \brief rvalue copy constructor is deleted
            Renderer(const Renderer&&) = delete;

            //! \brief destructor
            ~Renderer();
    
            //! \brief initialization
            //! since the renderer-object in main is global, its constructed at runtime, but glfw, opengl, etc. are not yet ready. Therefore, do the actual initialization later
            void Initialize();

            //! \brief sets window title to given string
            //! \param title string containing new window title
            void SetWindowTitle(std::string title);
    
            //! \brief main rendering function
            //! contains the render loop
            void Render();

            //! \brief recreates geometry if game is restarted
            void RecreateGeometry();
    
            //! \brief gets system details on OpenGL
            //! \param getExtensions flag indicating if information on available OpenGL extensions is requested or not. Since its a lot of text, default is false.
            //! \return string containig OpenGL details
            std::string GetGLDetails(bool getExtensions = false);
    
        private:
            //! \brief Initialize GLFW
            void InitGLFW();

            //! \brief Initialze Glew
            void InitGlew();

            //! \brief Initialize Shaders
            void InitShaders();

            //! \brief configure global OpenGL state variables
            void ConfigureGlobalOpenGLStates();

            //! \brief create geometry
            void CreateGeometry();

            //! \brief create geometry used in debug modes
            void CreateDebugGeometry();

            //! \brief create screen quad to render everything to
            void CreateScreenQuad();

            //! \brief create framebuffer for screen to render everything to
            void CreateFramebufferForScreen();

            //! \brief create skybox
            void CreateSkybox();

            //! \brief Initialize camera
            void InitCamera();

            //! \brief Initialize debug light
            void InitDebugLight();
    
            //! \brief process input on current frame
            //! \param collisionNormal mean normal of all collision geometry collieded with. Default (0,0,0), which equals none
            void ProcessInputPerFrame(glm::vec3 collisionNormal = glm::vec3(0.0f));
    };
    
    }
    }
    }
    }
    
    #endif // !SKR_FPS2_RENDERER_OPENGL_RENDERER_H
    