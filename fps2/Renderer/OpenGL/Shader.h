#ifndef SKR_FPS2_RENDERER_OPENGL_SHADER_H
#define SKR_FPS2_RENDERER_OPENGL_SHADER_H

#include "..\..\pch.h"

#include <string>

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! shader type identifies. Used as keys in map keeping shaders
    enum struct ShaderType
    { 
        Main,           //!< main shader
        Screen,         //!< screen shader
        Skybox,         //!< shader to draw skybox
        DebugLight,     //!< draw position of lights for debug purposes
        Simple,         //!< very simple shader: draw everything in a given solor
        MaterialMaps,   //!< use material maps (normal maps, specular maps) to draw
        DepthShadow     //!< draw depthmap for shadow mapping
    };

    //! \class Shader
    //! \brief handles shader loading, compiling and linking. Also provides access to set uniform ids and usage.
    class Shader
    {
        public:
            uint32_t _id;   //!< shader id

        public:
            //! \brief default constructor
            //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
            Shader() : _id(0) {};

            //! \brief constructor
            //! \param vertpath path to vertex shader
            //! \param fragpath path to fragment shader
            //! \param geompath path to geometry shader. Default empty, then it is not used
            Shader(std::string vertpath, std::string fragpath, std::string geompath = std::string(""));

        private:
            //! \brief load shader from given path
            //! \param path where shader file is located
            //! \return string containing the shader code
            std::string LoadShader(std::string path);

            //! \brief compile shader
            //! \param code shader code
            //! \param type of OpenGL shader
            //! \return id of compiled shader
            uint32_t CompileShader(std::string code, GLenum type);

            //! \brief check shader for compile errors
            //! \param shader shader id
            void CheckCompileErrors(GLuint shader);

            //! \brief check shader for linker errors
            //! \param shader shader id
            void CheckLinkErrors(GLuint shader);

        public:
            //! \brief set shader active
            const void Use();

            //! \brief set boolean value
            //! \param loc name of variable in shader
            //! \param value value
            const void SetBool(std::string loc, bool value);

            //! \brief set float value
            //! \param loc name of variable in shader
            //! \param value value
            const void SetFloat(std::string loc, float value);

            //! \brief set integer value
            //! \param loc name of variable in shader
            //! \param value value
            const void SetInt(std::string loc, int value);

            //! \brief set vector 3 value
            //! \param loc name of variable in shader
            //! \param value value
            const void SetVec3(std::string loc, glm::vec3 value);

            //! \brief set matrix 4x4 value
            //! \param loc name of variable in shader
            //! \param value value
            const void SetMat4(std::string loc, glm::mat4 value);
    };
}
}
}
}

#endif // !SKR_FPS2_RENDERER_OPENGL_SHADER_H

