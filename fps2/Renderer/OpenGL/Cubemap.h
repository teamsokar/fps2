#ifndef SKR_FPS2_RENDERER_OPENGL_CUBEMAP_H
#define SKR_FPS2_RENDERER_OPENGL_CUBEMAP_H

#include "..\..\pch.h"

#include <vector>
#include <string>

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class Cubemap
    //! \brief manages cubemap texture
    class Cubemap
    {
        private:
            std::vector<std::string> _faces; //!< list of faces

        public:
            uint32_t _id; //!< texture id

        public:
            //! \brief default constructor
            // default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
            Cubemap() : _id(0) { }; 

            //! \brief constructor
            //! \param faces list of faces
            Cubemap(std::vector<std::string> faces);

        private:
            //! \brief load cubemap
            //! loads texture specified in _faces list
            bool LoadCubemap();
    };
}
}
}
}

#endif // SKR_FPS2_RENDERER_OPENGL_CUBEMAP_H

