#ifndef SKR_FPS2_RENDERER_OPENGL_CAMERA_H
#define SKR_FPS2_RENDERER_OPENGL_CAMERA_H

#include "..\..\pch.h"
#include "Settings.h"

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{
    //! camera movement identifiers
    enum struct CameraMovement
    { 
        FORWARD,    //!< move camera forward
        BACKWARD,   //!< move camera backwards
        LEFT,       //!< move camera left
        RIGHT       //!< move camera right
    };

    constexpr float YAW(-90.0);             //!< default yaw value
    constexpr float PITCH(0.0f);            //!< default pitch value
    constexpr float SPEED(17.5f);           //!< default movement speed
    constexpr float SENSITIVITY(0.075f);    //!< default camera/mouse sensitivity value
    constexpr float FOV(60.0f);             //!< default field of view

    //! \class Camera
    //! \brief manages camera
    class Camera
    {
    public:
        glm::vec3 _position;    //!< position
        glm::vec3 _front;       //!< front vector
        glm::vec3 _up;          //!< up vector
        glm::vec3 _right;       //!< right vector
        glm::vec3 _worldUp;     //!< world up vector

        float _yaw;             //!< current yaw
        float _pitch;           //!< current pitch

        float _movementSpeed;       //!< camera movement peed
        float _mouseSensitivity;    //!< camera/mouse sensitivity
        float _fov;                 //!< field of view

        float _lastX = (float)Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH] / 2; //!< last position of mouse on x-axis
        float _lastY = (float)Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT] / 2; //!< last position of mouse on y-axis
        bool _firstMouse = true;        //!< flag indicating if mouse is used for the first tome

        //! \brief default constructor
        Camera() : 
            _position(glm::vec3(0.0f, 0.0f, 0.0f)),
            _front(glm::vec3(0.0f, 0.0f, -1.0f)),
            //_up is calculated and set through UpdateCameraVectors
            _up(glm::vec3(0.0f, 1.0f, 0.0f)),
            //_right is calculated and set through UpdateCameraVectors
            _worldUp(glm::vec3(0.0f, 1.0f, 0.0f)),
            _yaw(YAW),
            _movementSpeed(SPEED),
            _mouseSensitivity(SENSITIVITY),
            _pitch(PITCH),
            _fov(FOV)
        {
            UpdateCameraVectors();
        }

        //! \brief constructor
        //! \param position Position
        //! \param up Up vector
        //! \param yaw Yaw
        //! \param pitch Pitch
        //! \param fov Field of View
        Camera(
            glm::vec3 position, 
            glm::vec3 up,
            float yaw,
            float pitch,
            float fov
        ) : 
            _position(position),
            _front(glm::vec3(0.0f, 0.0f, -1.0f)),
            //_up is calculated and set through UpdateCameraVectors
            _up(up),
            //_right is calculated and set through UpdateCameraVectors
            _worldUp(up),
            _yaw(yaw),
            _pitch(pitch),
            _movementSpeed(SPEED),
            _mouseSensitivity(SENSITIVITY),
            _fov(fov)
        {
            UpdateCameraVectors();
        }

        //! \brief gets current view matrix
        //! \return current view matrix
        glm::mat4 GetViewMatrix();

        //! \brief process keyboard input
        //! \param direction movement direction
        //! \param deltaTime time since last frame
        //! \param collisionNormal normal of hit collision boxes. Default value (0,0,0) if no collision
        //! \param fly flag indicating if camera is in flying or FPS mode (no movement in y-axis)
        void ProcessKeyboard(CameraMovement direction, float deltaTime, glm::vec3 collisionNormal = glm::vec3(0.0f), bool fly = false);

        //! \brief process mouse movement
        //! \param xoffset movement since last frame on x-axis
        //! \param yoffset movement since last frame on y-axis
        //! \param constrainPitch true: limit pitch to prevent vertical camera rollover. Default: true
        void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);

        //void ProcessMouseScroll(float yoffset);

        //! \brief get current camera position in human readable form
        //! \return string with current camera position in human readable form
        std::string GetCurrentPositionReadable();

        //! \brief get current camera parameters (up, yaw, fov) in human readable form
        //! \return  string with current camera parameters in human readable form
        std::string GetCurrentParametersReadable();

        //! \brief set camera position
        //! \param pos new Positin
        void SetPosition(glm::vec3 pos);

        //! \brief set camera parameters
        //! \param pos new position
        //! \param up new up vector
        //! \param yaw new yaw
        //! \param pitch new pitch
        //! \param fov new field of view
        void SetParameters(glm::vec3 pos, glm::vec3 up, float yaw, float pitch, float fov);

    private:
        //! \brief recalculates camera vectors
        void UpdateCameraVectors();

        //! \brief print camera vectors to cmd
        void PrintCameraVectors();      
    };
}
}
}
}

#endif //!SKR_FPS2_RENDERER_OPENGL_CAMERA_H
