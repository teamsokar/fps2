#ifndef SKR_FPS2_SETTINGS_H
#define SKR_FPS2_SETTINGS_H

#include "..\..\pch.h"

#include <string>
#include <unordered_map>
#include <vector>

namespace skr
{
namespace fps2
{
    //! \class Settings
    //! \brief managing and handling of settings for program
    /*! Implements Scott Meyers' singleton pattern */
    class Settings
    {
        private:
            //! \brief default constructor
            //! empty as OpenGL-Renderer object is a global object an requiers one at launch, even though it does nothing
            Settings(){};

            //! \brief copy constructor is deleted
            Settings(const Settings&) = delete;

            //! \brief copy assignment constructor is deleted
            Settings operator=(const Settings&) = delete;

            //! path to settings file
            //! default: settings.cfg in local folder
            std::string _settingsFilePath = "settings.cfg";
        public:
            //! \brief Get only instance of this class
            //! Implements Scott Meyers' singleton pattern
            static Settings& GetInstance();

#pragma region Integer type Settings
        public:
            //! identifiers for settings of type unsigned integer
            enum struct SettingUInt32t
            {
                SCR_WIDTH,  //!< viewport width
                SCR_HEIGHT, //!< viewport height
                RNG_MODE,   //!< mode of procedural generation
                RNG_SEED    //!< seed for random number generator used in procedural generation
            };

#pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251

            //! map keeping settings of type unsigned integer and their values
            std::unordered_map<SettingUInt32t, uint32_t> _settings_uint32t
            {
                {SettingUInt32t::SCR_WIDTH,  1280},
                {SettingUInt32t::SCR_HEIGHT, 720},
                {SettingUInt32t::RNG_MODE, 0},
                {SettingUInt32t::RNG_SEED, 0}
            };

        private:
            // using this manually maintained mapping tables is a dirty hack
            // using compile-time reflection would be better, but also requires a lot of work up front, currently not worth it with the limited amount of settings

            //! typedef to map a string to a setting of type unsigned integer
            typedef std::unordered_map<std::string, SettingUInt32t> StringToSettingIntMap;

            //! mapping table for string to setting of type unsinged integer
            static const std::unordered_map<std::string, SettingUInt32t> MapStringToSettingInt;
           
            //! typedef to map settings of type unsigned integer to a string
            typedef std::unordered_map<SettingUInt32t, std::string> SettingIntToStringMap;

            //! mapping table for settings of type unsigned integer to a string
            static const std::unordered_map<SettingUInt32t, std::string> MapSettingIntToString;

#pragma warning(default:4251)
#pragma endregion

#pragma region Float type Settings
        public:
            //! identifies for settings of type float
            enum struct SettingFloat
            {
                RND_NEAR,   //!< distance of near plane
                RND_FAR     //!< distance of far plane
            };

#pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251

            //! map keeping settings of type float and their values
            std::unordered_map<SettingFloat, float> _settings_float
            {
                {SettingFloat::RND_NEAR, 0.1f},
                {SettingFloat::RND_FAR,  300.0f}
            };

        private:
            // using this manually maintained mapping tables is a dirty hack
            // using compile-time reflection would be better, but also requires a lot of work up front, currently not worth it with the limited amount of settings

            //! typedef to map a setting of type flaot to a string
            typedef std::unordered_map<SettingFloat, std::string> SettingFloatToStringMap;

            //! mapping table from a setting of type float to a string
            static const std::unordered_map<SettingFloat, std::string> MapSettingFloatToString;

            //! typedef to map a string to a setting of type float
            typedef std::unordered_map<std::string, SettingFloat> StringToSettingFloatMap;

            //! mapping table from a string to setting of type float
            static const std::unordered_map<std::string, SettingFloat> MapStringToSettingFloat;

#pragma warning(default:4251)
#pragma endregion

#pragma region Boolean type Settings

        public:
            //! identifiers for settings of type boolean
            enum struct SettingBool
            {
                RND_VSYNC,              //!< vertical synchronization on/off
                RNG_PRINTSEED,          //!< print seed to cmd and log on/off
                RND_GAMMACORRECTION,    //!< use gamma correction of/off
                RND_COLORCODEDROOMS     //!< color code rooms on/off
            };

#pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251

            //! map keeping settings of type boolean and their values
            std::unordered_map<SettingBool, bool> _settings_bool
            {
                {SettingBool::RND_VSYNC, false},
                {SettingBool::RNG_PRINTSEED, true},
                {SettingBool::RND_GAMMACORRECTION, true},
                {SettingBool::RND_COLORCODEDROOMS, false},
            };

        private:
            // using this manually maintained mapping tables is a dirty hack
            // using compile-time reflection would be better, but also requires a lot of work up front, currently not worth it with the limited amount of settings

            //! typedef mapping a setting of type boolean to a string
            typedef std::unordered_map<SettingBool, std::string> SettingBoolToStringMap;

            //! mapping table to map a setting of type boolean to a string
            static const std::unordered_map<SettingBool, std::string> MapSettingBoolToString;

            //! typedef mapping a string to a setting of type boolean
            typedef std::unordered_map<std::string, SettingBool> StringToSettinBoolMap;

            //! mapping table from a string to a setting of type boolean
            static const std::unordered_map<std::string, SettingBool> MapStringToSettingBool;
#pragma warning(default:4251)

#pragma endregion

#pragma region RunTime Settings
        public:
            //! identifiers for settings of type bool only relevant during runtime, they are not written to the settings file
            enum struct RuntimeBoolSetting
            {
                GhostMode,          //!< flag indicating if ghost mode is active (no collision + fly)
                DrawDebugLight,     //!< flag indicating to draw a cube at position of every light
                MoveLights,         //!< flag indicating if lights are moving or static
                DrawBoundingBox,    //!< flag indicating if bounding boxes of geometry are draw
                UseNormalMapping    //!< flag indicating if normal mapping is active
            };

            //! map keeping settings of type bool only relevant during runtime
            std::unordered_map<RuntimeBoolSetting, bool> _runtime_bool
            {
                {RuntimeBoolSetting::GhostMode, false},
                {RuntimeBoolSetting::DrawDebugLight, false},
                {RuntimeBoolSetting::MoveLights, false},
                {RuntimeBoolSetting::DrawBoundingBox, false},
                {RuntimeBoolSetting::UseNormalMapping, false}
            };

            //! type identifies for shadow mapping types
            enum struct ShadowType
            {
                Off,                    //!< shadow mapping off
                ShadowMapping,          //!< simple shadow mapping
                ShadowMappingPCF,       //!< shadow mapping with Percentage-closer filtering
                ShadowMappingPCFDisk    //!< shadow mapping with Percentage-closer filtering with fixed samples
            };

            ShadowType _shadowRenderingMode = ShadowType::ShadowMappingPCF; //!< type of shadow mapping currently used

#pragma endregion

        public:
            //! \brief write settings to path in _settingsFilePath
            //! \return true if write operation was successfull, otherwise false
            bool WriteSettingsToFile();

            //! \brief write settings to given path
            //! \param path path settings will be written to
            //! \return true if write operation was successful, otherwise false
            bool WriteSettingsToFile(const std::string path);

            //! \brief read settings from path set in _settingsFilePath
            //! \return true if read was successful, otherwise false
            bool ReadSettingsFromFile();

            //! \brief read settings from given path
            //! \param path path to file settings are read from
            //! \return truen if read was successful, otherwise false
            bool ReadSettingsFromFile(const std::string path);

            //! \brief write all settings into a single string
            //! used to write everything in one big chunk
            //! \return string containing all settings
            std::string PrintSettingsToString();

        private:
            //! \brief read settings from a file into a list of strings
            //! \param path Path settings are read from
            //! \param[out] settings reference to list of strings settings will be read to
            //! \return true if read operation was successful, otherwise false.
            bool GetSettingsFromFile(const std::string path, std::vector<std::string>& settings);

            //! \brief set settings from a list of strings
            //! goes through a list of settings, one per element, and sets values in maps accordingly
            //! \param settings list of settings
            //! \return bool if operation was successful, otherwise false
            bool SetSettingsFromString(std::vector<std::string> settings);

            //! \brief Get boolean value in human readable form: true/false instead of 1/0
            //! \param v boolean value
            //! return human readable string of boolean value
            std::string GetHumanReadableBool(bool v);
    };

}
}

#endif // !SKR_FPS2_SETTINGS_H
