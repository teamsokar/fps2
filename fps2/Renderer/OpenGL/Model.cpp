#include "..\..\pch.h"
#include "Model.h"
#include <iostream>
#include "Logger.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    Model::Model()
      :  _scale(glm::vec3(1.0f)), _path(std::string("")), _shadertype(ShaderType::Main), _gammaCorrection(false), _useMaterialTextures(false), _yOffset(0.0f)
    {
    }

    Model::Model(std::string const& path, glm::vec3 scale, float yoffset, ShaderType shadertype, bool gamma)
    : _scale(scale), _path(path), _shadertype(shadertype), _gammaCorrection(gamma), _useMaterialTextures(false), _yOffset(yoffset)
{
    LoadModel(path);
    CalculateBoundingBox();
}

Model::Model(Model && other) noexcept
    : 
    _texturesLoaded(std::move(other._texturesLoaded)), _meshes(std::move(other._meshes)),
    _scale(other._scale), _path(other._path), _shadertype(other._shadertype),
    _gammaCorrection(other._gammaCorrection), _directory(other._directory),
    _useMaterialTextures(other._useMaterialTextures),
    _yOffset(other._yOffset)
{
    other._meshes.clear();
    other._texturesLoaded.clear();
    CalculateBoundingBox();
}

Model::Model(std::vector<skr::pcg::Face> faces, glm::vec3 scale, float yoffset, ShaderType shadertype, bool gamma)
    : _scale(scale), _shadertype(shadertype), _gammaCorrection(gamma), _useMaterialTextures(false), _yOffset(yoffset)
{
    Renderer::OpenGL::Vertex vertex;

    for (auto f : faces)
    {
        std::vector<Renderer::OpenGL::Vertex> vertices;
        std::vector<uint32_t> indicies;
        std::vector<Texture> textures;
        glm::vec3 drawColor;

        for (auto v : f.vertices)
        {

            vertex.Position.x = v.Position[0];
            vertex.Position.y = v.Position[1];
            vertex.Position.z = v.Position[2];

            vertex.Normal.x = v.Normal[0];
            vertex.Normal.y = v.Normal[1];
            vertex.Normal.z = v.Normal[2];

            vertex.TexCoords.x = v.TexCoords[0];
            vertex.TexCoords.y = v.TexCoords[1];

            vertex.Tangent = glm::vec3{ 0.0f, 0.0f,0.0f };

            vertex.Bitangent = glm::vec3{ 0.0f, 0.0f, 0.0f };

            vertices.push_back(vertex);
        }

        indicies.insert(indicies.end(), f.indices.begin(), f.indices.end());

        drawColor = glm::vec3{ f.color[0], f.color[1], f.color[2] };

        _meshes.push_back(Mesh(vertices, indicies, textures, drawColor));
    }

    CalculateBoundingBox();
}

void Model::Draw(Shader shader, uint32_t depthMap) const
{
    for (const auto &mesh : _meshes)
    {
        mesh.Draw(shader, depthMap, _useMaterialTextures);
    }
}

void Model::DrawGeometry() const
{
    for (const auto& mesh : _meshes)
    {
        mesh.DrawGeometry();
    }
}

void Model::DrawBoundingBox(Shader shader, glm::mat4 view, glm::mat4 proj, glm::mat4 model, glm::vec3 color) const
{
    _boundingBox.Draw(shader, view, proj, model, color);
}

void Model::LoadModel(std::string const& path)
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::string importerErrorString = importer.GetErrorString();
        std::cout << "ERROR unable to load model through assimp: " << importerErrorString << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR unable to load model through assimp: " + importerErrorString);
        //TODO error handling
    }
    else
    {
        _directory = path.substr(0, path.find_last_of('/'));
        ProcessNode(scene->mRootNode, scene);
    }
}

void Model::ProcessNode(aiNode* node, const aiScene* scene)
{
    for (uint32_t i(0); i < node->mNumMeshes; ++i)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        _meshes.push_back(ProcessMesh(mesh, scene));
    }

    for (uint32_t i(0); i < node->mNumChildren; ++i)
    {
        ProcessNode(node->mChildren[i], scene);
    }
}

Mesh Model::ProcessMesh(aiMesh* mesh, const aiScene* scene)
{
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indicies;
    std::vector<Texture> textures;

    for (uint32_t i(0); i < mesh->mNumVertices; ++i)
    {
        Vertex vertex;
        glm::vec3 vector; 
        
        // temporary object as assimps types are not compatibles with glms
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;

        if (mesh->mNormals)
        {
            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.Normal = vector;
        }
        else
        {
            vertex.Normal = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        if (mesh->mTextureCoords[0])
        {
            glm::vec2 vec;

            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        }
        else
        {
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }

        if (mesh->mTangents)
        {
            vector.x = mesh->mTangents[i].x;
            vector.y = mesh->mTangents[i].y;
            vector.z = mesh->mTangents[i].z;
            vertex.Tangent = vector;
        }
        else
        {
            vertex.Tangent = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        if (mesh->mBitangents)
        {
            vector.x = mesh->mBitangents[i].x;
            vector.y = mesh->mBitangents[i].y;
            vector.z = mesh->mBitangents[i].z;
            vertex.Bitangent = vector;
        }
        else
        {
            vertex.Bitangent = glm::vec3(0.0f, 0.0f, 0.0f);
        }

        vertices.push_back(vertex);
    }

    for (uint32_t i(0); i < mesh->mNumFaces; ++i)
    {
        aiFace face = mesh->mFaces[i];

        for (uint32_t j(0); j < face.mNumIndices; ++j)
        {
            indicies.push_back(face.mIndices[j]);
        }
    }

    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

    std::vector<Texture> diffuseMaps = Texture::LoadMaterialTextures(_texturesLoaded, _directory, material, aiTextureType_DIFFUSE, TextureType::DIFFUSE);
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

    std::vector<Texture> specularMaps = Texture::LoadMaterialTextures(_texturesLoaded, _directory, material, aiTextureType_SPECULAR, TextureType::SPECULAR);
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

    std::vector<Texture> normalMaps = Texture::LoadMaterialTextures(_texturesLoaded, _directory, material, aiTextureType_HEIGHT, TextureType::NORMAL);
    textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

    std::vector<Texture> heightMaps = Texture::LoadMaterialTextures(_texturesLoaded, _directory, material, aiTextureType_AMBIENT, TextureType::DIFFUSE);
    textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());

    if (specularMaps.size() > 0 || normalMaps.size() > 0 || heightMaps.size() > 0)
        _useMaterialTextures = true;

    return Mesh(vertices, indicies, textures);
}

void Model::CalculateBoundingBox()
{
    glm::vec3 max(std::numeric_limits<float>::min());
    glm::vec3 min(std::numeric_limits<float>::max());

    for (auto& m : _meshes)
    {
        glm::vec3 tmax, tmin;
        m.GetExtremePositions(tmax, tmin);

        if (tmax.x > max.x)
            max.x = tmax.x;

        if (tmax.y > max.y)
            max.y = tmax.y;

        if (tmax.z > max.z)
            max.z = tmax.z;


        if (tmin.x < min.x)
            min.x = tmin.x;

        if (tmin.y < min.y)
            min.y = tmin.y;

        if (tmin.z < min.z)
            min.z = tmin.z;
    }

    glm::vec3 center = glm::abs(glm::abs(min) - glm::abs(max)) * 0.5f;

    center.y += _yOffset;

    float width  = max.x - min.x;
    float height = max.y - min.x;
    float length = max.z - min.x;
    
    if (width < 0.0f)
        width = -width;

    if (height < 0.0f)
        height = -height;

    if (length < 0.0f)
        length = -length;

    _boundingBox = Collision::BoundingBox(center, glm::vec3(width, height, length));
}

}
}
}
}
