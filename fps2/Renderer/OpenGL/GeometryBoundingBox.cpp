#include "..\..\pch.h"
#include "GeometryBoundingBox.h"

namespace skr
{
namespace fps2 
{
namespace Renderer
{
namespace OpenGL
{

    GeometryBoundingBox::GeometryBoundingBox()
        : _boundingBox(skr::pcg::LevelBoundingBox()), _pointVAO(0), _pointVBO(0), _linesVAO(0), _linesVBO(0)
{
    // do not create Geometry yet, OpenGL ist not initialized yet
}

GeometryBoundingBox::GeometryBoundingBox(std::vector<skr::pcg::Room> rooms)
    : _boundingBox(skr::pcg::LevelBoundingBox(rooms)), _pointVAO(0), _pointVBO(0), _linesVAO(0), _linesVBO(0)
{
    CreateOpenGLGeometryBuffer();
}

void GeometryBoundingBox::CreateOpenGLGeometryBuffer()
{
    // Draw Center of Bounding Box
    glGenVertexArrays(1, &_pointVAO);
    glGenBuffers(1, &_pointVBO);

    float bbPos[3] = { _boundingBox._center[0], _boundingBox._center[1], _boundingBox._center[2] };

    glBindVertexArray(_pointVAO);
    glBindBuffer(GL_ARRAY_BUFFER, _pointVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bbPos), &bbPos, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    // Draw Bounding Box as lines
    glGenVertexArrays(1, &_linesVAO);
    glGenBuffers(1, &_pointVBO);

    //std::vector<float> lineCoordinates;
    float lineCoordinates[24] =
    {
        _boundingBox._LLB[0], _boundingBox._LLB[1], _boundingBox._LLB[2], _boundingBox._ULB[0], _boundingBox._ULB[1], _boundingBox._ULB[2],
        _boundingBox._ULB[0], _boundingBox._ULB[1], _boundingBox._ULB[2], _boundingBox._URB[0], _boundingBox._URB[1], _boundingBox._URB[2],
        _boundingBox._URB[0], _boundingBox._URB[1], _boundingBox._URB[2], _boundingBox._LRB[0], _boundingBox._LRB[1], _boundingBox._LRB[2],
        _boundingBox._LRB[0], _boundingBox._LRB[1], _boundingBox._LRB[2], _boundingBox._LLB[0], _boundingBox._LLB[1], _boundingBox._LLB[2]
    };

    glBindVertexArray(_linesVAO);
    glBindBuffer(GL_ARRAY_BUFFER, _linesVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineCoordinates), &lineCoordinates, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    // done
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

}
}
}
}
