#include "..\..\pch.h"
#include "RendererOpenGL.h"

#include <glm/gtc/matrix_transform.hpp>

#include "..\..\version.h"
#include "..\..\AssetPath.h"
#include "BasicGeometry.h"
#include "pcg/FloorGenerator.h"
#include "pcg/Definitions.h"
#include "..\..\Game\Game.h"
#include "LevelManager.h"
#include "ModelCache.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{

#pragma region constructor + destructor

Renderer::Renderer()
    : _debugLightModelId(0), _framebuffer(0), _framebufferTexture(0), _window(nullptr), _camera(nullptr)
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer default constructor");
    //Settings::GetInstance().ReadSettingsFromFile();
}

Renderer::Renderer(std::string /*windowTitle*/, std::string settingsfile)
    : _debugLightModelId(0), _framebuffer(0), _framebufferTexture(0), _window(nullptr), _camera(nullptr)
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer constructor");
    Settings::GetInstance().ReadSettingsFromFile(settingsfile);
}

Renderer::~Renderer()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer destructor");

    for (const auto& vao : _vertexArrayObjects)
    {
        glDeleteVertexArrays(1, &vao.second);
    }
        
    for (const auto& vbo : _vertexBufferObjects)
    {
        glDeleteBuffers(1, &vbo.second);
    }

    glfwTerminate();
}

#pragma endregion

#pragma region global initialization

void Renderer::Initialize()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Initialize");

    InitGLFW();
    InitGlew();
    ConfigureGlobalOpenGLStates();
    InitShaders();    
    CreateGeometry();
    CreateDebugGeometry();
    _collisionManager = std::make_shared<Collision::CollisionManager>(Collision::CollisionManager());
    CreateScreenQuad();
    CreateFramebufferForScreen();
    CreateSkybox();
    InitCamera();
}

void Renderer::SetWindowTitle(std::string title)
{
    _windowTitle = title;

    if (_window != nullptr)
    {
        glfwSetWindowTitle(_window, title.c_str());
    }
}

void Renderer::InitGLFW()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Init GLFW");

    if (!glfwInit())
    {
        std::cout << "ERROR: GLFW init not succesfull" << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR: GLFW init not succesfull");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    _window = glfwCreateWindow
        (
            Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH],
            Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT],
            _windowTitle.c_str(),
            nullptr, nullptr
        );


    if (_window == nullptr)
    {
        std::cout << "ERROR: failed to create GLFW window" << std::endl;
       skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR: failed to create GLFW window");
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(_window);
    glfwSetWindowUserPointer(_window, this);

    //glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    //glDebugMessageCallback(GLDebugMessageCallback, nullptr);

    // check Vsync setting
    if(!Settings::GetInstance()._settings_bool[Settings::SettingBool::RND_VSYNC])
        glfwSwapInterval(0);
}

void Renderer::InitGlew()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Init Glew");

    if (glewInit() != GLEW_OK)
    {
        std::cout << "ERROR: Glew init failed" << std::endl;
       skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR: Glew init failed");
        exit(EXIT_FAILURE);
    }
}

void Renderer::InitShaders()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Init Shaders");

    // --------------------------------------------------------------------------------------------------------------
    // Create shader
    // --------------------------------------------------------------------------------------------------------------

    _shaders[ShaderType::Main] = Shader(GetAssetPath("Shader/OpenGL/PhongShading/basic.vs.glsl"), GetAssetPath("Shader/OpenGL/PhongShading/basic.fs.glsl"));
    _shaders[ShaderType::MaterialMaps] = Shader(GetAssetPath("Shader/OpenGL/PhongShading/basic.vs.glsl"), GetAssetPath("Shader/OpenGL/PhongShading/MaterialMaps.fs.glsl"));

    _shaders[ShaderType::Screen] = Shader(GetAssetPath("Shader/OpenGL/screen.vs.glsl"), GetAssetPath("Shader/OpenGL/screen.fs.glsl"));
    _shaders[ShaderType::Skybox] = Shader(GetAssetPath("Shader/OpenGL/skybox.vs.glsl"), GetAssetPath("Shader/OpenGL/skybox.fs.glsl"));

    _shaders[ShaderType::DebugLight] = Shader(GetAssetPath("Shader/OpenGL/debuglight.vs.glsl"), GetAssetPath("Shader/OpenGL/debuglight.fs.glsl"));

    // simple shader which sets a fixed color in fragment shader, do input needed
    _shaders[ShaderType::Simple] = Shader(GetAssetPath("Shader/OpenGL/simple.vs.glsl"), GetAssetPath("Shader/OpenGL/simple.fs.glsl"));

    _shaders[ShaderType::DepthShadow] = Shader(GetAssetPath("Shader/OpenGL/ShadowMapping/ShadowsDepth.vs.glsl"), GetAssetPath("Shader/OpenGL/ShadowMapping/ShadowsDepth.fs.glsl"), GetAssetPath("Shader/OpenGL/ShadowMapping/ShadowsDepth.gs.glsl"));

    // --------------------------------------------------------------------------------------------------------------
    // Configurate shader
    // --------------------------------------------------------------------------------------------------------------

    _shaders[ShaderType::Main].Use();

    _shaders[ShaderType::MaterialMaps].Use();

    _shaders[ShaderType::Screen].Use();
    _shaders[ShaderType::Screen].SetInt("screenTexture", 0);

    _shaders[ShaderType::Skybox].Use();
    _shaders[ShaderType::Skybox].SetInt("skybox", 0);

    _shaders[ShaderType::Simple].Use();

    _shaders[ShaderType::DepthShadow].Use();
}

void Renderer::ConfigureGlobalOpenGLStates()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Configure Global OpenGL states");

    glEnable(GL_DEPTH_TEST);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    if(Settings::GetInstance()._settings_bool[Settings::SettingBool::RND_GAMMACORRECTION])
        glEnable(GL_FRAMEBUFFER_SRGB);

    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_CCW);

    //glEnable(GL_DEBUG_OUTPUT);
    //glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    //glDebugMessageCallback(GLDebugMessageCallback, 0);
}

std::string Renderer::GetGLDetails(bool getExtensions)
{
    std::string gl;

    auto glVendor = glGetString(GL_VENDOR);
    if (nullptr != glVendor)
    {
        std::string glVendorStr(reinterpret_cast<const char*>(glVendor));
        gl.append("\tGL_VENDOR: " + glVendorStr + "\n");
    }
    else
    {
        gl.append("\tGL_VENDOR: ERROR unable to get vendor string\n");
    }

    auto glVersion = glGetString(GL_VERSION);
    if (nullptr != glVersion)
    {
        std::string glVersionStr(reinterpret_cast<const char*>(glVersion));
        gl.append("\tGL_VERSION: " + glVersionStr + "\n");
    }
    else
    {
        gl.append("\tGL_VERSION: ERROR unable to get version string\n");
    }

    auto glRenderer = glGetString(GL_RENDERER);
    if (nullptr != glRenderer)
    {
        std::string glRendererStr(reinterpret_cast<const char*>(glRenderer));
        gl.append("\tGL_RENDERER: " + glRendererStr + "\n");
    }
    else
    {
        gl.append("\tGL_RENDERER: ERROR unable to get renderer string\n");
    }

    auto glShadingLangVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    if (nullptr != glShadingLangVersion)
    {
        std::string glShadingLangVersionStr(reinterpret_cast<const char*>(glShadingLangVersion));
        gl.append("\tGL_SHADING_LANGUAGE_VERSION: " + glShadingLangVersionStr + "\n");
    }
    else
    {
        gl.append("\tGL_SHADING_LANGUAGE_VERSION: ERROR unable to get shading language version string\n");
    }

    if (getExtensions)
    {
        auto glExtensions = glGetString(GL_EXTENSIONS);
        if (nullptr != glExtensions)
        {
            std::string glExtensionsStr(reinterpret_cast<const char*>(glExtensions));
            gl.append("\tGL_EXTENSIONS: " + glExtensionsStr + "\n");
        }
        else
        {
            gl.append("\tGL_EXTENSIONS: ERROR unable to get extensions string\n");
        }
    }

    return gl;
}

#pragma endregion

#pragma region Geometry Creation

void Renderer::CreateGeometry()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Create Geometry");
    
    skr::pcg::FloorGeneratorParameter params(NUM_ROOMS, skr::pcg::FloorLayoutStrategy::Stacking);
    params.SetDimensions(20.0f, 50.0f, 20.0f, 50.0f, 5.0f, 5.0f);
    
    if (Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::RNG_MODE] == 1)
    {
        params._rng_randomSeed = false;
        params._rng_seed = Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::RNG_SEED];
    }
    else
    {
        params._rng_randomSeed = true;
    }
    
    params._rng_printSeed = Settings::GetInstance()._settings_bool[Settings::SettingBool::RNG_PRINTSEED];

    if (params._rng_printSeed)
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("RNG Seed Set: " + std::to_string(params._rng_seed));
    }
    
    skr::pcg::FloorGenerator generator(params);
    
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("Create Room Geometry");
    
    std::vector<skr::pcg::Room> geometry = generator.GetAllGeometry();
    
    for (auto& r : geometry)
    //auto r = geometry[1]; // for fast debugging
    {
        RoomGeometry room(r);
        std::string msg;

        room._PointLights.push_back(PointLight(r._center));
    
        msg.append(r.PrintRoomMeasurements());
        skr::tools::Logger::GetInstance().LogWithDatenAndTime(msg);    

        for (size_t i = 0; i < r._targets.size(); i++)
        {
            room._targets.push_back(TargetGeometry(r._targets[i], i));
        }

        LevelManager::GetInstance().AddRoom(room);
    }

    for (auto& r : LevelManager::GetInstance()._rooms)
    {
        if (r._room._roomType == skr::pcg::RoomType::Room)
        {
            //r._PointLights.push_back(PointLight(r._room._center));
            continue;
        }
        else
        {
            r._PointLights.clear();

            auto adjRooms = LevelManager::GetInstance().GetAdjecentRoomGeometries(r);
            for (auto& ar : adjRooms)
            {
                r._PointLights.insert(r._PointLights.end(), ar._PointLights.begin(), ar._PointLights.end());
            }
        }
    }

    if (params._rng_printSeed)
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("RNG Seed used: " + std::to_string(params._rng_seed));
    }
    Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::RNG_SEED] = params._rng_seed;
}

void Renderer::CreateDebugGeometry()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Create Debug Geometry");

    InitDebugLight();
}

void Renderer::RecreateGeometry()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Recreate Geometry");
    LevelManager::GetInstance()._rooms.clear();

    Game::Game::GetInstance().Reset();
    _collisionManager->Cleanup();

    LevelManager::GetInstance()._rooms.clear();
    CreateGeometry();
    _collisionManager->Init();
    CreateDebugGeometry();
    InitCamera();
}

#pragma endregion

#pragma region screen

void Renderer::CreateScreenQuad()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Create Screen Quad");

    uint32_t quadVAO, quadVBO;
    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &quadVBO);

    glBindVertexArray(quadVAO);
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skr::tools::BasicGeometry::quadVertices), &skr::tools::BasicGeometry::quadVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

    _vertexArrayObjects[VaoType::Screen] = quadVAO;
    _vertexBufferObjects[VboType::Screen] = quadVBO;
}

void Renderer::CreateFramebufferForScreen()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Create Framebuffer for Screen");

    glGenFramebuffers(1, &_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);

    glGenTextures(1, &_framebufferTexture);
    glBindTexture(GL_TEXTURE_2D, _framebufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH], Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT], 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _framebufferTexture, 0);

    uint32_t renderbuffer;
    glGenRenderbuffers(1, &renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH], Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT]);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuffer);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "ERROR Framebuffer not complete";
       skr::tools::Logger::GetInstance().LogWithDatenAndTime("ERROR Framebuffer not complete");
        exit(EXIT_FAILURE);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

#pragma endregion

void Renderer::CreateSkybox()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Create Skybox");

    uint32_t skyboxVAO, skyboxVBO;
    glGenVertexArrays(1, &skyboxVAO);
    glGenBuffers(1, &skyboxVBO);

    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skr::tools::BasicGeometry::skyboxVertices), &skr::tools::BasicGeometry::skyboxVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    _vertexArrayObjects[VaoType::Skybox] = skyboxVAO;
    _vertexBufferObjects[VboType::Skybox] = skyboxVBO;

    std::vector<std::string> faces
    {
        GetAssetPath("textures/skybox/right.jpg"),
        GetAssetPath("textures/skybox/left.jpg"),
        GetAssetPath("textures/skybox/top.jpg"),
        GetAssetPath("textures/skybox/bottom.jpg"),
        GetAssetPath("textures/skybox/front.jpg"),
        GetAssetPath("textures/skybox/back.jpg")
    };

    _skyboxCubemap = Cubemap(faces);
}

void Renderer::InitCamera()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Init Camera");

    auto firstRoom = LevelManager::GetInstance()._rooms[0]._room;
    auto initialPos = firstRoom._center;
    float yaw = 0.0f;

    switch (firstRoom._doors[0])
    {
    case skr::pcg::DoorDirection::TOP: // back
    {
        initialPos[2] -= 1.0f;
        yaw = 90.0f;
        break;
    }

    case skr::pcg::DoorDirection::RIGHT: // right
    {
        initialPos[0] -= 1.0f;
        yaw = 0.0f;
        break;
    }

    case skr::pcg::DoorDirection::BOTTOM: // front
    {
        initialPos[2] += 1.0f;
        yaw = 270.0f;
        break;
    }

    case skr::pcg::DoorDirection::LEFT: // left
    {
        initialPos[0] += 1.0f;
        yaw = 180.0f;
        break;
    }

    default:
        break;
    }

    auto initPos = glm::vec3(initialPos[0], initialPos[1], initialPos[2]);

    LevelManager::GetInstance().SetInitialPosition(initPos);

    if (_camera != nullptr)
    {
        _camera->SetParameters(initPos, glm::vec3(0.0f, 1.0f, 0.0f), yaw, 1.0f, 60.0f);
    }
    else
    {
        _camera = std::make_shared<Camera>(Camera::Camera(initPos, glm::vec3(0.0f, 1.0f, 0.0f), yaw, 1.0f, 60.0f)); // default
        //_camera = std::make_shared<Camera>(Camera::Camera(glm::vec3(_geoBB._boundingBox._center[0], 200.0f, _geoBB._boundingBox._center[2]), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f,    -89.0f, 60.0f)); // birds eye
    }

    _camera->_lastX = (float)Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH] / 2;
    _camera->_lastY = (float)Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT] / 2;
}

#pragma region Lights

void Renderer::InitDebugLight()
{
    skr::tools::Logger::GetInstance().LogWithDatenAndTime("Init Debug Light");

    _debugLightModelId = ModelCache::GetInstance().AddModel(GetAssetPath("models/debuglight/debuglight.obj"), glm::vec3(0.25f));
}

#pragma endregion

#pragma region input handling

void Renderer::ProcessInputPerFrame(glm::vec3 collisionNormal)
{
    if (glfwGetKey(_window, GLFW_KEY_W) == GLFW_PRESS)
        _camera->ProcessKeyboard(CameraMovement::FORWARD, _deltatime, collisionNormal, Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::GhostMode]);
    if (glfwGetKey(_window, GLFW_KEY_S) == GLFW_PRESS)
        _camera->ProcessKeyboard(CameraMovement::BACKWARD, _deltatime, collisionNormal, Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::GhostMode]);
    if (glfwGetKey(_window, GLFW_KEY_A) == GLFW_PRESS)
        _camera->ProcessKeyboard(CameraMovement::LEFT, _deltatime, collisionNormal, Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::GhostMode]);
    if (glfwGetKey(_window, GLFW_KEY_D) == GLFW_PRESS)
        _camera->ProcessKeyboard(CameraMovement::RIGHT, _deltatime, collisionNormal, Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::GhostMode]);
}

#pragma endregion

void Renderer::Render()
{
   skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Render()");
   
   glm::vec3 meanNormalContacts(0.0f);

    while (!glfwWindowShouldClose(_window))
    {
        float currentFrame = (float)glfwGetTime();
        _deltatime = currentFrame - _lastFrame;
        _lastFrame = currentFrame;

        if (!Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::GhostMode])
        {
            _collisionManager->CheckCollision(_camera->_position);
            meanNormalContacts = _collisionManager->GetCurrentMeanNormalOfContactPoints();
        }

        if (!Game::Game::GetInstance().IsFinished())
        {
            if (!Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::GhostMode])
            {
                _collisionManager->CheckOverlap(_camera->_position);
            }

            ProcessInputPerFrame(meanNormalContacts);
        }

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);


        // --------------------------------------------------------------------------------------------------------------
        // Moving Objects
        // --------------------------------------------------------------------------------------------------------------
        glm::vec3 lp_movement;

        if (Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::MoveLights])
            lp_movement = glm::vec3(sin(currentFrame * 2.0f) * 5.0f, 0.0f, cos(currentFrame* 2.0f) * 5.0f);
        else
            lp_movement = glm::vec3(0.0f);

        for (auto& r : LevelManager::GetInstance()._rooms)
        {
            for (auto& t : r._targets)
            {
                t._movement = t.CalculateMovement(currentFrame);
            }
        }


        // --------------------------------------------------------------------------------------------------------------
        // Render to DepthCubeMap
        // --------------------------------------------------------------------------------------------------------------
        if (Settings::GetInstance()._shadowRenderingMode != Settings::ShadowType::Off)
        {
            glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);

            for (auto& r : LevelManager::GetInstance()._rooms)
            {
                r.RenderDepthMap(_shaders[ShaderType::DepthShadow], lp_movement, currentFrame);
            }
        }

        // --------------------------------------------------------------------------------------------------------------
        // Render everything that follows to framebuffer
        // --------------------------------------------------------------------------------------------------------------
        glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
        glViewport(0, 0, Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH], Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT]);
        
        //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // --------------------------------------------------------------------------------------------------------------
        // Setup matrices
        // --------------------------------------------------------------------------------------------------------------
        glm::mat4 modelMat = glm::mat4(1.0);
        glm::mat4 view = _camera->GetViewMatrix();
        glm::mat4 projection = glm::perspective(
            glm::radians(_camera->_fov),
            (float)Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_WIDTH] /
            (float)Settings::GetInstance()._settings_uint32t[Settings::SettingUInt32t::SCR_HEIGHT],
            Settings::GetInstance()._settings_float[Settings::SettingFloat::RND_NEAR],
            Settings::GetInstance()._settings_float[Settings::SettingFloat::RND_FAR]
        );

        // --------------------------------------------------------------------------------------------------------------
        // Render Geometry to Framebuffer
        // --------------------------------------------------------------------------------------------------------------
        _shaders[ShaderType::Main].Use();
        _shaders[ShaderType::Main].SetMat4("view", view);
        _shaders[ShaderType::Main].SetMat4("projection", projection);

        for (auto& r : LevelManager::GetInstance()._rooms)
        {
            std::vector<glm::vec3> lightPositions;

            _shaders[ShaderType::Main].Use();
            _shaders[ShaderType::Main].SetMat4("model", modelMat);
            _shaders[ShaderType::Main].SetVec3("viewPos", _camera->_position);

            _shaders[ShaderType::Main].SetBool("useTexture", true);
            _shaders[ShaderType::Main].SetVec3("addedColor", r._material.diffuse);

            _shaders[ShaderType::Main].SetBool("useNormalMapping", Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::UseNormalMapping]);

            r._material.ApplyMaterial(_shaders[ShaderType::Main]);

            _shaders[ShaderType::Main].SetInt("NR_POINTLIGHTS_SET", r._PointLights.size());

            for(size_t i(0); i < r._PointLights.size(); ++i)
            {
                lightPositions.push_back(r._PointLights[i]._position + lp_movement);
#
                // setup lights for fragment shader
                r._PointLights[i].SetPointLight(_shaders[ShaderType::Main], lightPositions[i], i);

                // set lightPos in VertexShader for transformation to tangent space
                _shaders[ShaderType::Main].SetVec3("lightPos[" + std::to_string(i) + "]", lightPositions[i]);
            }

            // Shadow mapping
            _shaders[ShaderType::Main].SetFloat("far_plane", Settings::GetInstance()._settings_float[Settings::SettingFloat::RND_FAR]);
            _shaders[ShaderType::Main].SetInt("shadows", static_cast<uint8_t>(Settings::GetInstance()._shadowRenderingMode));

            for (const auto& m : r._meshes)
            {
                m.Draw(_shaders[ShaderType::Main], r._depthCubeMap);
            }

            for (auto& t : r._targets)
            {
                auto& t_model = ModelCache::GetInstance().GetModel(t._modelId);

                glm::mat4 modelMatTarget = glm::mat4(1.0f);
                modelMatTarget = glm::translate(modelMatTarget, t._position + t._movement);
                modelMatTarget = glm::scale(modelMatTarget, t_model._scale);

                _shaders[ShaderType::MaterialMaps].Use();
                _shaders[ShaderType::MaterialMaps].SetMat4("view", view);
                _shaders[ShaderType::MaterialMaps].SetMat4("projection", projection);
                _shaders[ShaderType::MaterialMaps].SetMat4("model", modelMatTarget);
                
                _shaders[ShaderType::MaterialMaps].SetVec3("addedColor", t._completed ? RENDER_TARGET_HIT_COLOR : glm::vec3(1.0f));
                _shaders[ShaderType::MaterialMaps].SetFloat("material.shininess", 256.0f);

                _shaders[ShaderType::MaterialMaps].SetBool("useNormalMapping", Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::UseNormalMapping]);

                _shaders[ShaderType::MaterialMaps].SetInt("NR_POINTLIGHTS_SET", r._PointLights.size());
                _shaders[ShaderType::MaterialMaps].SetVec3("viewPos", _camera->_position);

                for (size_t i(0); i < r._PointLights.size(); ++i)
                {
                    // setup lights for fragment shader
                    r._PointLights[i].SetPointLight(_shaders[ShaderType::MaterialMaps], lightPositions[i], i);

                    // set lightPos in VertexShader for transformation to tangent space
                    _shaders[ShaderType::MaterialMaps].SetVec3("lightPos[" + std::to_string(i) + "]", lightPositions[i]);
                }

                // Shadow mapping
                _shaders[ShaderType::MaterialMaps].SetFloat("far_plane", Settings::GetInstance()._settings_float[Settings::SettingFloat::RND_FAR]);
                _shaders[ShaderType::MaterialMaps].SetInt("shadows", static_cast<uint8_t>(Settings::GetInstance()._shadowRenderingMode));

                t_model.Draw(_shaders[ShaderType::MaterialMaps], r._depthCubeMap);

                if (Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::DrawBoundingBox])
                {
                    t_model.DrawBoundingBox(_shaders[ShaderType::Simple], view, projection, modelMatTarget);
                }
            }

            // Draw BoundingBox for Room Collision shapes and Targets
            if (Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::DrawBoundingBox])
            {
                for (auto& co : r._collisionObjects)
                {
                    if(co->GetCollisionCategory() == CollisionCategory::STARTLINE)
                        co->DrawCollisionShape(_shaders[ShaderType::Simple], view, projection, modelMat, glm::vec3(0.0f, 1.0f, 0.0f));
                    else
                        co->DrawCollisionShape(_shaders[ShaderType::Simple], view, projection, modelMat);
                    
                }
            }
        }

        // --------------------------------------------------------------------------------------------------------------
        // Debug Points/Visualization
        // ---------------------------------------------------------------------------------------------------------------

        if (_collisionManager->DrawHits())
        {
            for (auto& h : _collisionManager->GetRaycastCallback()._hitInfos)
            {
                auto& pHitModel = ModelCache::GetInstance().GetModel(_collisionManager->HitModelId());
                glm::mat4 modelMatHit = glm::mat4(1.0f);
                modelMatHit = glm::translate(modelMatHit, glm::vec3(h.x, h.y, h.z));
                modelMatHit = glm::scale(modelMatHit, pHitModel._scale);
                _shaders[ShaderType::Simple].Use();
                _shaders[ShaderType::Simple].SetMat4("view", view);
                _shaders[ShaderType::Simple].SetMat4("projection", projection);
                _shaders[ShaderType::Simple].SetMat4("model", modelMatHit);

                pHitModel.Draw(_shaders[pHitModel._shadertype]);
            }
        }

        // Draw Lights
        if (Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::DrawDebugLight])
        {
            for (auto& r : LevelManager::GetInstance()._rooms)
            {
                for (auto& pl : r._PointLights)
                {
                    auto& pDebugLightModel = ModelCache::GetInstance().GetModel(_debugLightModelId);

                    glm::mat4 modelMatDebugLight = glm::mat4(1.0);
                    modelMatDebugLight = glm::translate(modelMatDebugLight, pl._position + lp_movement);
                    modelMatDebugLight = glm::scale(modelMatDebugLight, pDebugLightModel._scale);

                    _shaders[ShaderType::DebugLight].Use();

                    _shaders[ShaderType::DebugLight].SetMat4("model", modelMatDebugLight);
                    _shaders[ShaderType::DebugLight].SetMat4("view", view);
                    _shaders[ShaderType::DebugLight].SetMat4("projection", projection);

                    _shaders[ShaderType::DebugLight].SetVec3("lightColor", pl._diffuseColor);

                    
                    pDebugLightModel.Draw(_shaders[ShaderType::DebugLight]);
                }
            }
        }

        // Draw global BoundingBox for Level
        if (Settings::GetInstance()._runtime_bool[Settings::RuntimeBoolSetting::DrawBoundingBox])
        {
            // center of boundingbox
            _shaders[ShaderType::Simple].Use();
            _shaders[ShaderType::Simple].SetMat4("view", view);
            _shaders[ShaderType::Simple].SetMat4("projection", projection);
            _shaders[ShaderType::Simple].SetMat4("model", modelMat);
            _shaders[ShaderType::Simple].SetVec3("drawColor", glm::vec3(1.0f, 0.0f, 0.0f));

            glPointSize(10);
            glBindVertexArray(_geoBB._pointVAO);
            glDrawArrays(GL_POINTS, 0, 1);

            glLineWidth(2);
            glBindVertexArray(_geoBB._linesVAO);
            glDrawArrays(GL_LINES, 0, 8);
        }

        // --------------------------------------------------------------------------------------------------------------
        // Render Skybox to Framebuffer
        // --------------------------------------------------------------------------------------------------------------
        glDepthFunc(GL_LEQUAL);
        view = glm::mat4(glm::mat3(_camera->GetViewMatrix())); //remove one dimension to draw it only in places where nothing else is already drawn
        _shaders[ShaderType::Skybox].Use();
        _shaders[ShaderType::Skybox].SetMat4("view", view);
        _shaders[ShaderType::Skybox].SetMat4("projection", projection);

        glBindVertexArray(_vertexArrayObjects[VaoType::Skybox]);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, _skyboxCubemap._id);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
        glDepthFunc(GL_LESS);

        // --------------------------------------------------------------------------------------------------------------
        // Render Framebuffer to screen Quad
        // --------------------------------------------------------------------------------------------------------------
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        _shaders[ShaderType::Screen].Use();
        glBindVertexArray(_vertexArrayObjects[VaoType::Screen]);
        glBindTexture(GL_TEXTURE_2D, _framebufferTexture);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // --------------------------------------------------------------------------------------------------------------
        // Swap buffers
        // --------------------------------------------------------------------------------------------------------------
        glfwSwapBuffers(_window);

        // --------------------------------------------------------------------------------------------------------------
        // More Event Handling
        // --------------------------------------------------------------------------------------------------------------
        glfwPollEvents();

        _collisionManager->ResetContactPoints();
    }

    skr::tools::Logger::GetInstance().LogWithDatenAndTime("Ending Rendering");
    
    _collisionManager->Cleanup();
}

}
}
}
}

#pragma region GL Error Handling / Debugging
void APIENTRY GLDebugMessageCallback(GLenum src, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    std::string msg = "OpenGL Error detected:\n";

    switch (src)
    {
    case GL_DEBUG_SOURCE_API:
        msg.append("Source: API ");
        break;

    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
        msg.append("Source: Window System ");
        break;

    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        msg.append("Source: Shader Compiler ");
        break;

    case GL_DEBUG_SOURCE_THIRD_PARTY:
        msg.append("Source: Third Party ");
        break;

    case GL_DEBUG_SOURCE_APPLICATION:
        msg.append("Source: Application ");
        break;

    case GL_DEBUG_SOURCE_OTHER:
        msg.append("Source: Other ");
        break;
    default:
        msg.append("Source: unknown ");
    }

    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:               
        msg.append("Type: Error ");
        break;

    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: 
        msg.append("Type: Deprecated Behaviour ");
        break;

    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  
        msg.append("Type: Undefined Behaviour ");
        break;

    case GL_DEBUG_TYPE_PORTABILITY:         
        msg.append("Type: Portability ");
        break;

    case GL_DEBUG_TYPE_PERFORMANCE:         
        msg.append("Type: Performance "); 
        break;

    case GL_DEBUG_TYPE_MARKER:              
        msg.append("Type: Marker "); 
        break;

    case GL_DEBUG_TYPE_PUSH_GROUP:          
        msg.append("Type: Push Group "); 
        break;

    case GL_DEBUG_TYPE_POP_GROUP:           
        msg.append("Type: Pop Group "); 
        break;

    case GL_DEBUG_TYPE_OTHER:               
        msg.append("Type: Other "); 
        break;

    default:
        msg.append("Type: unknown ");
    }

    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:         
        msg.append("Severity: high "); 
        break;

    case GL_DEBUG_SEVERITY_MEDIUM:       
        msg.append("Severity: medium "); 
        break;

    case GL_DEBUG_SEVERITY_LOW:          
        msg.append("Severity: low "); 
        break;

    case GL_DEBUG_SEVERITY_NOTIFICATION: 
        msg.append("Severity: notification "); 
        break;

    default:
        msg.append("Severity: unknown");
    }

    if (length > 0)
    {
        msg.append("\nMessage: ");
        msg.append(message);
    }

    msg.append("\n\n");

    skr::tools::Logger::GetInstance().LogWithDatenAndTime(msg);
    std::cout << msg << std::endl;
}

void GetGLError()
{
    auto glerr = glGetError();
    if (glerr == GL_NO_ERROR)
        return;

    std::string msg = std::string("GL Error in File" + std::string(__FILE__) + " in Line " + std::to_string(__LINE__) + ". GL Erro Code: " + std::to_string(glerr));

    skr::tools::Logger::GetInstance().LogWithDatenAndTime("GL Error in Line: " + glerr);
    std::cout << "GL Error Code: " << glerr << std::endl;
}
#pragma endregion
