#include "..\..\pch.h"

#include "Mesh.h"

#include "..\..\AssetPath.h"
#include "TextureCache.h"

#ifdef _DEBUG
#include <assert.h>
#include <iostream>
#endif // DEBUG


namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<uint32_t> indices, std::vector<Texture> textures, glm::vec3 drawColor)
    : _vertices(vertices), _indices(indices), _textures(textures), 
    _useDrawColor(false), _drawColor(drawColor)
{
    SetupMeshIndexed();
}

Mesh::Mesh(skr::pcg::Face face)
    : _useDrawColor(false), _drawColor(glm::vec3(1.0f))
{
    Renderer::OpenGL::Vertex vertex;

    for (auto v : face.vertices)
    {
        vertex.Position.x = v.Position[0];
        vertex.Position.y = v.Position[1];
        vertex.Position.z = v.Position[2];

        vertex.Normal.x = v.Normal[0];
        vertex.Normal.y = v.Normal[1];
        vertex.Normal.z = v.Normal[2];

        vertex.TexCoords.x = v.TexCoords[0];
        vertex.TexCoords.y = v.TexCoords[1];

        vertex.Tangent = glm::vec3{ 0.0f, 0.0f,0.0f };

        vertex.Bitangent = glm::vec3{ 0.0f, 0.0f, 0.0f };

        _vertices.push_back(vertex);
    }

    _indices.insert(_indices.end(), face.indices.begin(), face.indices.end());

    _drawColor = glm::vec3(face.color[0], face.color[1], face.color[2]);

    if (face.diffuseTexturePath != "")
    {
        std::string texturePath = GetAssetPath(face.diffuseTexturePath);

        bool skip = false;
        for(auto& t : TextureCache::GetInstance().cachedTextures)
        {
            if(t._path.compare(texturePath) == 0)
            {
                _textures.push_back(t);
                skip = true;
                break;
            }
        }

        if (!skip)
        {
            Texture t(TextureType::DIFFUSE, texturePath);
            _textures.push_back(t);
            TextureCache::GetInstance().cachedTextures.push_back(t);
        }
    }

    if (face.normalTexturePath != "")
    {
        std::string normalPath = GetAssetPath(face.normalTexturePath);

        bool skip = false;
        for (auto& t : TextureCache::GetInstance().cachedTextures)
        {
            if (t._path.compare(normalPath) == 0)
            {
                _textures.push_back(t);
                skip = true;
                break;
            }
        }

        if (!skip)
        {
            Texture t(TextureType::NORMAL, normalPath);
            _textures.push_back(t);
            TextureCache::GetInstance().cachedTextures.push_back(t);
        }

#ifdef _DEBUG
        assert(_vertices.size() == 6); // face has to have exactly 6 vertices
#endif // DEBUG
        
        // Calculate tangent and bitangent
        auto tanbitan = CalculateTangentAndBitangentForTriangle(
            _vertices[0].Position,  _vertices[1].Position,  _vertices[2].Position,
            _vertices[0].TexCoords, _vertices[1].TexCoords, _vertices[2].TexCoords,
            _vertices[0].Normal
        );

        for (uint32_t i(0); i < 6; ++i)
        {
            _vertices[i].Tangent = tanbitan[0];
            _vertices[i].Bitangent = tanbitan[1];
        }

    }

    if (_textures.empty())
        _useDrawColor = true;

    if (_indices.empty())
        SetupMeshRawTriangles();
    else
        SetupMeshIndexed();
}

Mesh::~Mesh()
{
    // HACK crashes app later when trying to render a loaded model, NPE in nvogl32.dll
    // TODO gets called before and therefore already cleans up buffers -> check why this happens!
    //glDeleteVertexArrays(1, &_vao);
    //glDeleteBuffers(1, &_vbo);
    //glDeleteBuffers(1, &_ebo);
}

void Mesh::Draw(Shader shader, uint32_t depthMap, bool useMaterialTextures) const
{
    DrawPrepareTextures(shader, depthMap, useMaterialTextures);

    if(_useDrawColor)
        shader.SetVec3("drawColor", _drawColor);

    DrawGeometry();

    glActiveTexture(GL_TEXTURE0);
}

void Mesh::DrawGeometry() const
{
    glBindVertexArray(_vao);

    if (_indices.empty())
    {
        glDrawArrays(GL_TRIANGLES, 0, _vertices.size());
    }
    else
    {
        glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, 0);
    }
    glBindVertexArray(0);
}

void Mesh::SetDrawColor(glm::vec3 color)
{
    _drawColor = color;
}

void Mesh::GetExtremePositions(glm::vec3& max, glm::vec3& min)
{
    glm::vec3 maximum(std::numeric_limits<float>::min());
    glm::vec3 minimum(std::numeric_limits<float>::max());

    for (auto& v : _vertices)
    {
        if (v.Position.x > maximum.x)
            maximum.x = v.Position.x;

        if (v.Position.y > maximum.y)
            maximum.y = v.Position.y;

        if (v.Position.z > maximum.z)
            maximum.z = v.Position.z;


        if (v.Position.x < minimum.x)
            minimum.x = v.Position.x;

        if (v.Position.y < minimum.y)
            minimum.y = v.Position.y;

        if (v.Position.z < minimum.z)
            minimum.z = v.Position.z;
    }

    max = maximum;
    min = minimum;
}

void Mesh::DrawPrepareTextures(Shader shader, uint32_t depthMap, bool useMaterialTextures) const
{
    // get and bind textures
    uint32_t diffuseNr(1);
    uint32_t specularNr(1);
    uint32_t normalNr(1);
    uint32_t heightNr(1);

    uint32_t i(0);

    for (i = 0; i < _textures.size(); ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);

        std::string number, name;

        switch (_textures[i]._type)
        {
            // GL_TEXTURE0
        case TextureType::DIFFUSE:
            name = Texture::TextureTypeDiffuse;
            number = std::to_string(diffuseNr);
            diffuseNr++;
            break;

            //GL_TEXTURE1
        case TextureType::SPECULAR:
            name = Texture::TextureTypeSpecular;
            number = std::to_string(specularNr);
            specularNr++;
            break;

            //GL_TEXTURE2
        case TextureType::NORMAL:
            name = Texture::TextureTypeNormal;
            number = std::to_string(normalNr);
            normalNr++;
            break;

            //GL_TEXTURE3 - although should be unused
        case TextureType::HEIGHT:
            name = Texture::TextureTypeHeight;
            number = std::to_string(heightNr);
            heightNr++;
            break;
        default:
            name = "";
        }

        if (useMaterialTextures)
            name = "material." + name;

        shader.SetInt((name + number).c_str(), i);
        glBindTexture(GL_TEXTURE_2D, _textures[i]._id);
    }

    if (useMaterialTextures)
    {
        if (specularNr == 1)
        {
            shader.SetInt("material." + Texture::TextureTypeSpecular + "1", 0);
        }

        if (normalNr == 1)
        {
            shader.SetInt("material." + Texture::TextureTypeNormal + "1", 0);
        }
    }

    if (depthMap != 0)
    {
        ++i;
        glActiveTexture(GL_TEXTURE0 + i);
        shader.SetInt("depthMap", i);
        glBindTexture(GL_TEXTURE_CUBE_MAP, depthMap); 
    }
}

void Mesh::SetupMeshIndexed()
{
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);
    glGenBuffers(1, &_ebo);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), &_vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(uint32_t), &_indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));

    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));
}

void Mesh::SetupMeshRawTriangles()
{
    glGenVertexArrays(1, &_vao);
    glGenBuffers(1, &_vbo);

    glBindVertexArray(_vao);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo);
    glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(Vertex), &_vertices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));

    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));
}

std::array<glm::vec3, 2> Mesh::CalculateTangentAndBitangentForTriangle(glm::vec3 pos1, glm::vec3 pos2, glm::vec3 pos3, glm::vec2 tex1, glm::vec2 tex2, glm::vec2 tex3, glm::vec3 normal)
{
    std::array<glm::vec3, 2> result;
    glm::vec3 tangent;
    glm::vec3 bitangent;

    glm::vec3 edge1 = pos2 - pos1;
    glm::vec3 edge2 = pos3 - pos1;
    glm::vec2 deltaUV1 = tex2 - tex1;
    glm::vec2 deltaUV2 = tex3 - tex1;

    float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

    tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
    tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
    tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
    tangent = glm::normalize(tangent);

    bitangent.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
    bitangent.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
    bitangent.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
    bitangent = glm::normalize(bitangent);

    result[0] = tangent;
    result[1] = bitangent;

    return result;
}

std::array<glm::vec3, 2> Mesh::CalculateTangentAndBitangentForTriangle(std::array<glm::vec3, 3> positions, std::array<glm::vec2, 3> texcoords, glm::vec3 normal)
{
    return CalculateTangentAndBitangentForTriangle(positions[0], positions[1], positions[2], texcoords[0], texcoords[1], texcoords[2], normal);
}

}
}
}
}
