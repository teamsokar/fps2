#ifndef SKR_FPS2_RENDERER_OPENGL_MODEL_H
#define SKR_FPS2_RENDERER_OPENGL_MODEL_H

#include "..\..\pch.h"

#include <vector>

#include "Texture.h"
#include "Mesh.h"
#include "Shader.h"
#include "..\..\Collision\BoundingBox.h"

namespace skr
{
namespace fps2
{
namespace Renderer
{
namespace OpenGL
{
    //! \class Model
    //! \brief holds information of a model
    class Model
    {
        public:
            std::vector<Texture> _texturesLoaded;   //!< list of texture loaded for this model
            std::vector<Mesh> _meshes;              //!< list of meshes
            glm::vec3 _scale;                       //!< scale to draw model in
            std::string _path;                      //!< path to directory where model file is loaded from
            ShaderType _shadertype;                 //!< type of shader the model is drawn with
            bool _useMaterialTextures;              //!< flag indicating if material texture (normal mal, specular map) are used or material parameters
            Collision::BoundingBox _boundingBox;    //!< collision bounding box of the model
            float _yOffset;                         //!< offset from center of model if its defined at the bottom

        private:
            std::string _directory;                 //!< path to directory where model data (geometry, texturex) are located
            bool _gammaCorrection;                  //!< flag indicating if gamme corection is used
    
        public:
            //! \brief default constructor
            //! default constructor is needed since OpenGL Renderer Object is a global object, although it does nothing
            Model();

            // \brief rvalue copy constructor
            //! \param other rvalue to other model, which is copied
            Model(Model&& other) noexcept;

            //! \brief constructor
            //! \param path reference to path where model is locataded
            //! \param scale scale used to draw model
            //! \param yoffset vertical offset from bottom to center of model. Used if (0,0,0) of the model is defined at the bottom
            //! \param shadertype type of shader model is drawn with. Default: Main
            //! \param gamma flag inidicating if gamma correction is used. Default: false
            Model(std::string const &path, glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f), float yoffset = 0.0f, ShaderType shadertype = ShaderType::Main, bool gamma = false);

            //! \brief constructor
            //! \param faces list of faces from procedural generation the geometry is build from
            //! \param scale scale used to draw model
            //! \param yoffset vertical offset from bottom to center of model. Used if (0,0,0) of the model is defined at the bottom
            //! \param shadertype type of shader model is drawn with. Default: Main
            //! \param gamma flag inidicating if gamma correction is used. Default: false
            Model(std::vector<skr::pcg::Face> faces, glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f), float yoffset = 0.0f, ShaderType shadertype = ShaderType::Main, bool gamma = false);

            //! \brief draw model
            //! main drawing function
            //! \param shader shader to draw model with
            //! \param depthMap id to depthmap. Default: 0 (non used)
            void Draw(Shader shader, uint32_t depthMap = 0) const;

            // \brief draw only geomtry without textures
            //!  used to draw depthmaps for shadow mapping
            //! \param shader shader to draw geometry in. Assuming shadertype DepthMap //TODO not used??
            void DrawGeometry() const;

            //! \brief draw bounding box of the model
            //! \param shader shader to draw bounding box in. Assuming type simple
            //! \param view view matrix
            //! \param proj projection matrix
            //! \param model model matrix
            //! \param color color to draw bounding box in. Default: blue (0,0,1)
            void DrawBoundingBox(Shader shader, glm::mat4 view, glm::mat4 proj, glm::mat4 model, glm::vec3 color = glm::vec3(0.0f, 0.0f, 1.0f)) const;

            //! \brief set _scale to given value
            //! \param s new value for _scale
            void SetScale(glm::vec3 s)
            {
                _scale = s;
            }
            
            //! \brief set _yOffset to given value
            //! \param yoffset new value for _yOffset
            void SetYOffset(float yoffset)
            {
                _yOffset = yoffset;
            }
    
        private:
            //! \brief load model from given path
            //! \param path reference path to where model is located
            void LoadModel(std::string const &path);

            //! \brief process a node loaded by assimp
            //! \param node assimp node
            //! \param scene assimp scene
            void ProcessNode(aiNode* node, const aiScene* scene);

            //! \brief process mesh loaded by assimp
            //! \param mesh assimp mesh
            //! \param scene assimp scene
            Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);

            //! \brief calculates bounding box
            void CalculateBoundingBox();
    };

}
}
}
}

#endif //!SKR_FPS2_RENDERER_OPENGL_MODEL_H
