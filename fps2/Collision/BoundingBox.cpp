#include "..\pch.h"
#include "BoundingBox.h"



namespace skr
{
namespace fps2 
{
namespace Collision
{

BoundingBox::BoundingBox()
   : _position(glm::vec3(0.0f)), _size(glm::vec3(0.0f)),
     _centerVao(0), _centerVbo(0), _linesVao(0), _linesVbo(0)
{
}

BoundingBox::BoundingBox(glm::vec3 pos, glm::vec3 size)
    : _position(pos), _size(size),
      _centerVao(0), _centerVbo(0), _linesVao(0), _linesVbo(0)
{
    CreateGeometry();
}

void BoundingBox::UpdatePosition(glm::vec3 pos)
{
    _position = pos;
}

void BoundingBox::Draw(skr::fps2::Renderer::OpenGL::Shader shader, glm::mat4 view, glm::mat4 proj, glm::mat4 model, glm::vec3 color) const
{
    // expecting ShaderType::Simple
    shader.Use();
    shader.SetMat4("view", view);
    shader.SetMat4("projection", proj);
    shader.SetMat4("model", model);
    shader.SetVec3("drawColor", color);

    glPointSize(10);
    glBindVertexArray(_centerVao);
    glDrawArrays(GL_POINTS, 0, 1);

    glLineWidth(2);
    glBindVertexArray(_linesVao);
    glDrawArrays(GL_LINES, 0, 24);
}

void BoundingBox::CreateGeometry()
{
    // Draw center as point
    glGenVertexArrays(1, &_centerVao);
    glGenBuffers(1, &_centerVbo);

    float bbPos[3] = { _position.x, _position.y, _position.z };

    glBindVertexArray(_centerVao);
    glBindBuffer(GL_ARRAY_BUFFER, _centerVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bbPos), &bbPos, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    // Draw Box as lines
    glGenVertexArrays(1, &_linesVao);
    glGenBuffers(1, &_linesVbo);

    // good old cube...
    glm::vec3 llb(_position.x - _size.x * 0.5f, _position.y - _size.y * 0.5f, _position.z - _size.z * 0.5f);
    glm::vec3 urb(_position.x + _size.x * 0.5f, _position.y - _size.y * 0.5f, _position.z + _size.z * 0.5f);

    glm::vec3 lrb(llb.x, llb.y, urb.z);
    glm::vec3 ulb(urb.x, llb.y, llb.z);


    glm::vec3 llt(llb.x, llb.y + _size.y, llb.z);
    glm::vec3 urt(urb.x, urb.y + _size.y, urb.z);

    glm::vec3 lrt(lrb.x, lrb.y + _size.y, lrb.z);
    glm::vec3 ult(ulb.x, ulb.y + _size.y, ulb.z);

    // 12 edges * 2 (start+endpoint) * 3 coordinates
    float lineCoordinates[72] =
    {
        llb.x, llb.y, llb.z, ulb.x, ulb.y, ulb.z,
        ulb.x, ulb.y, ulb.z, urb.x, urb.y, urb.z,
        urb.x, urb.y, urb.z, lrb.x, lrb.y, lrb.z,
        lrb.x, lrb.y, lrb.z, llb.x, llb.y, llb.z,

        llt.x, llt.y, llt.z, ult.x, ult.y, ult.z,
        ult.x, ult.y, ult.z, urt.x, urt.y, urt.z,
        urt.x, urt.y, urt.z, lrt.x, lrt.y, lrt.z,
        lrt.x, lrt.y, lrt.z, llt.x, llt.y, llt.z,

        llb.x, llb.y, llb.z, llt.x, llt.y, llt.z,
        ulb.x, ulb.y, ulb.z, ult.x, ult.y, ult.z,
        urb.x, urb.y, urb.z, urt.x, urt.y, urt.z,
        lrb.x, lrb.y, lrb.z, lrt.x, lrt.y, lrt.z,
    };

    glBindVertexArray(_linesVao);
    glBindBuffer(GL_ARRAY_BUFFER, _linesVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineCoordinates), &lineCoordinates, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    // done
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

}
}
}
