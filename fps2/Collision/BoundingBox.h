#ifndef SKR_FPS2_COLLISION_BOUNDINGBOX_H
#define SKR_FPS2_COLLISION_BOUNDINGBOX_H

#include "..\pch.h"

#include "..\Renderer\OpenGL\Shader.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    //! \class BoundingBox
    //! \brief specifies bounding box of a geometry
    class BoundingBox
    {
    public:
        glm::vec3 _position; //!< position
        glm::vec3 _size; //!< size

        uint32_t _centerVao; //!< vertex array object of center point
        uint32_t _centerVbo; //!< vertex buffer object of center point
        uint32_t _linesVao; //!< vertex array object for lines visualizing size
        uint32_t _linesVbo; //!< vertex buffer object for lines visualizating size

    public:
        //! \brief default constructor
        //! sets all parameters to 0
        BoundingBox();

        //! \brief constructor
        //! vertex array and buffer objects are initialized to 0, calls CreateGeometry to properly initialize them
        //! \param pos Position
        //! \param size Size
        BoundingBox(glm::vec3 pos, glm::vec3 size);

        //! \brief update position
        //! \param pos new Position
        void UpdatePosition(glm::vec3 pos);
        
        //! \brief Draws bounding box
        //! \param shader Shader to draw geometry with
        //! \param view View matrix
        //! \param proj Projection matrix
        //! \param model Model Matrix
        //! \param color Color bounding box is drawn in. Default: Red (1,0,0)
        void Draw(skr::fps2::Renderer::OpenGL::Shader shader, glm::mat4 view, glm::mat4 proj, glm::mat4 model, glm::vec3 color = glm::vec3(1.0f, 0.0f, 0.0f)) const;

    private:
        //! \brief greates geometry buffer and initializes them for drawing
        void CreateGeometry();
    };

}
}
}

#endif //!SKR_FPS2_COLLISION_BOUNDINGBOX_H

