#include "..\pch.h"
#include "OverlapCallback.h"

#include <iostream>

#include "Logger.h"
#include "..\Renderer\OpenGL\LevelManager.h"
#include "..\Game\Game.h"

#include "collision/ContactManifold.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{

void OverlapCallback::notifyOverlap(rp3d::CollisionBody* collisionBody)
{
    //std::cout << "Collision detected" << std::endl;
    //skr::tools::Logger::GetInstance().LogWithDatenAndTime("Collision detected");

    for (auto& r : Renderer::OpenGL::LevelManager::GetInstance()._rooms)
    {
        for (auto& co : r._collisionObjects)
        {
            if (co->_body->getId() == collisionBody->getId())
            {
                //if (r._room._roomType == skr::pcg::RoomType::Start && !Game::Game::GetInstance().IsStarted())
                //{
                //    //std::cout << "14" << std::endl;
                //}

                if (r._room._roomType == skr::pcg::RoomType::Finish && !Game::Game::GetInstance().IsFinished())
                {
                    Game::Game::GetInstance().Finish();
                }
                
                if (co->GetCollisionCategory() == CollisionCategory::STARTLINE && !Game::Game::GetInstance().IsStarted())
                {
                    Game::Game::GetInstance().Start();
                }
            }
        }
    }
}

}
}
}
