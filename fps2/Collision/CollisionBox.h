#ifndef SKR_FPS2_COLLISION_COLLISION_BOX_H
#define SKR_FPS2_COLLISION_COLLISION_BOX_H

#include "..\pch.h"

#include "reactphysics3d.h"

#include "..\Renderer\OpenGL\Definitions.h"
#include "..\Renderer\OpenGL\Shader.h"
#include "BoundingBox.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    //! \class CollisionBox
    //! \brief defines a box used to collision
    class CollisionBox
    {
    public:
        rp3d::CollisionWorld* _world; //!< RP3D collision world

        rp3d::CollisionBody* _body; //!< collision body of the box
        rp3d::CollisionShape* _shape; //!< shape of the collision object
        rp3d::ProxyShape* _proxyShape; //!< proxy shape of the collision object

        glm::vec3 _position; //!< position

    public:
        //! \brief Default constructor
        //! initiliazes all pointers as null_ptr
        CollisionBox();

        //! \brief constructor
        //! \param world RP3D collision world 
        //! \param position Position of the CollisionBox
        //! \param size Size of the CollisionBox
        //! \param category Collision Categories the CollisionBox has. Default: Wall
        CollisionBox(rp3d::CollisionWorld* world, rp3d::Vector3 position, rp3d::Vector3 size, CollisionCategory category  = CollisionCategory::WALL);

        ////! \brief destructor
        //~CollisionBox();

        //! \brief returns the collision categories of this box
        //! \return CollisionCategory of this box
        CollisionCategory GetCollisionCategory();

        //! \brief sets CollisionCategory using the enum
        //! \param category category enum
        void SetCollisionCatergory(CollisionCategory category);

        //! \brief set CollisionCategory using indices for enum
        //! \param category CollisionCategorie inidices for enum
        void SetCollisionCatergory(unsigned short category);
        
        //! \brief Updates position of this CollisionBox as glm vec3
        //! \param pos new Position
        void UpdatePosition(glm::vec3 pos);

        //! \brief Updates position of this CollisionBox as rp3d vector3
        //! \param pos new Position
        void UpdatePosition(rp3d::Vector3 pos);

        //! \brief Draws the collision shape
        //! \param shader Shader used to draw
        //! \param view View Matrix
        //! \param proj Projection Matrix
        //! \param model Model Matrix
        //! \param color Color to draw the collision shape with. Default: Red (1,0,0)
        void DrawCollisionShape(skr::fps2::Renderer::OpenGL::Shader shader, glm::mat4 view, glm::mat4 proj, glm::mat4 model, glm::vec3 color = glm::vec3(1.0f, 0.0f, 0.0f)) const;

    private:
        BoundingBox _boundingBox; //!< bounding box of the collision shape
        
    };
}
}
}

#endif // SKR_FPS2_COLLISION_COLLISION_BOX_H
