#include "..\pch.h"

#include <iostream>

#include "CollisionCallback.h"

#include "collision/ContactManifold.h"
#include "constraint/ContactPoint.h"

#include "..\Renderer\OpenGL\LevelManager.h"
#include "..\Game\Game.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    void CollisionCallback::notifyContact(const CollisionCallbackInfo& collisionCallbackInfo)
    {
        rp3d::ContactManifoldListElement* manifoldElement = collisionCallbackInfo.contactManifoldElements;
        while (manifoldElement != nullptr)
        {
            rp3d::ContactManifold* contactManifold = manifoldElement->getContactManifold();

            rp3d::ContactPoint* contactPoint = contactManifold->getContactPoints();
            while (contactPoint != nullptr)
            {
                rp3d::Vector3 normal = contactPoint->getNormal();
                glm::vec3 norm(normal.x, normal.y, normal.z);

                rp3d::Vector3 point1 = contactPoint->getLocalPointOnShape1();
                point1 = collisionCallbackInfo.proxyShape1->getLocalToWorldTransform() * point1;
                glm::vec3 pos1(point1.x, point1.y, point1.z);
                _contactPoints.push_back(ContactPoint(pos1, norm));

                rp3d::Vector3 point2 = contactPoint->getLocalPointOnShape2();
                point2 = collisionCallbackInfo.proxyShape2->getLocalToWorldTransform() * point2;
                glm::vec3 pos2(point2.x, point2.y, point2.z);
                _contactPoints.push_back(ContactPoint(pos2, norm));

                contactPoint = contactPoint->getNext();
            }

            manifoldElement = manifoldElement->getNext();
        }
        
        //glm::vec3 meanNormal = GetMeanNormalOfContactPoints();
        //std::cout << "Collision detected. Mean Normal: " << meanNormal.x << '/' << meanNormal.y << '/' << meanNormal.z << std::endl;
    }

    void CollisionCallback::ResetContactPoints()
    {
        _contactPoints.clear();
    }

    std::vector<ContactPoint> CollisionCallback::GetContactPoints()
    {
        return _contactPoints;
    }

    glm::vec3 CollisionCallback::GetMeanNormalOfContactPoints()
    {
        if (_contactPoints.size() < 1)
            return glm::vec3(0.0f);

        if (_contactPoints.size() == 1)
            return _contactPoints[0].normal;

        glm::vec3 maximum(_contactPoints[0].normal);
        glm::vec3 minimum(_contactPoints[0].normal);

        for (size_t i(1); i < _contactPoints.size(); ++i)
        {
            if (_contactPoints[i].normal.x > maximum.x)
                maximum.x = _contactPoints[i].normal.x;

            if (_contactPoints[i].normal.y > maximum.y)
                maximum.y = _contactPoints[i].normal.y;

            if (_contactPoints[i].normal.z > maximum.z)
                maximum.z = _contactPoints[i].normal.z;


            if (_contactPoints[i].normal.x < minimum.x)
                minimum.x = _contactPoints[i].normal.x;

            if (_contactPoints[i].normal.y < minimum.y)
                minimum.y = _contactPoints[i].normal.y;

            if (_contactPoints[i].normal.z < minimum.z)
                minimum.z = _contactPoints[i].normal.z;
        }

        maximum = glm::normalize(maximum);
        minimum = glm::normalize(minimum);

        if (maximum == minimum)
            return minimum;

        glm::vec3 mean = maximum - minimum;

        return mean;
    }
}
}
}
