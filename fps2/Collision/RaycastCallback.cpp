#include "..\pch.h"
#include "RaycastCallback.h"

#include <iostream>
#include "Logger.h"
#include "..\Game\Game.h"
#include "..\Renderer\OpenGL\LevelManager.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{

float RaycastCallback::notifyRaycastHit(const rp3d::RaycastInfo& info)
{
    if (Game::Game::GetInstance().IsStarted())
    {
        std::string rayCastmsg = "Raycast Hit: Pos: " 
            + std::to_string(info.worldPoint.x) + "," + std::to_string(info.worldPoint.y) + "," + std::to_string(info.worldPoint.z)
            + " Normal: " + std::to_string(info.worldNormal.x) + "," + std::to_string(info.worldNormal.y) + "," + std::to_string(info.worldNormal.z);

        //std::cout << rayCastmsg << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime(rayCastmsg);

        _hitInfos.push_back(rp3d::Vector3(info.worldPoint.x, info.worldPoint.y, info.worldPoint.z));

        uint32_t hitId = 0;
        bool found = false;

        for (const auto& r : skr::fps2::Renderer::OpenGL::LevelManager::GetInstance()._rooms)
        {
            if (found)
                break;

            for (const auto& t : r._targets)
            {
                if (found)
                    break;

                for (const auto& co : t._collisionObjects)
                {
                    if (co->_body->getId() == info.body->getId())
                    {
                        hitId = t._id;
                        found = true;
                        break;
                    }
                }
            }
        }


        if (found)
            Game::Game::GetInstance().Hit(hitId);
    }

    return 0.0f;
}

void RaycastCallback::ClearHits()
{
    _hitInfos.clear();
}

}
}
}
