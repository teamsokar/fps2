#include "..\pch.h"
#include "CollisionBox.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    CollisionBox::CollisionBox()
        : _world(nullptr), _position(glm::vec3(0.0f)), _body(nullptr), _shape(nullptr), _proxyShape(nullptr)
    {
    }

    CollisionBox::CollisionBox(rp3d::CollisionWorld* world, rp3d::Vector3 position, rp3d::Vector3 size, CollisionCategory category)
    : _world(world), _position(glm::vec3(position.x, position.y, position.z))
{
    rp3d::Quaternion bodyOrientation = rp3d::Quaternion::identity();
    rp3d::Transform shapeTransform = rp3d::Transform::identity(); // position of shape in relation to collision body - position of body sets the position in the world

    rp3d::Transform bodyTransform(position, bodyOrientation);
    _body = _world->createCollisionBody(bodyTransform);
    _body->setType(rp3d::BodyType::STATIC);

    _shape = new rp3d::BoxShape(size * 0.5);

    _proxyShape = _body->addCollisionShape(_shape, shapeTransform);
    _proxyShape->setCollisionCategoryBits(static_cast<unsigned short>(category));

    _boundingBox = BoundingBox(glm::vec3(position.x, position.y, position.z), glm::vec3(size.x, size.y, size.z));
}

//CollisionBox::~CollisionBox()
//{
//    //_world->destroyCollisionBody(_body);
//}

CollisionCategory CollisionBox::GetCollisionCategory()
{
    return static_cast<CollisionCategory>(_proxyShape->getCollisionCategoryBits());
}

void CollisionBox::SetCollisionCatergory(CollisionCategory category)
{
    _proxyShape->setCollisionCategoryBits(static_cast<unsigned short>(category));
}

void CollisionBox::SetCollisionCatergory(unsigned short category)
{
    _proxyShape->setCollisionCategoryBits(category);
}

void CollisionBox::UpdatePosition(glm::vec3 pos)
{
    UpdatePosition(rp3d::Vector3(pos.x, pos.y, pos.z));
}

void CollisionBox::UpdatePosition(rp3d::Vector3 pos)
{
    rp3d::Transform bodyTransform(pos, rp3d::Quaternion::identity());
    _body->setTransform(bodyTransform);

    _boundingBox.UpdatePosition(glm::vec3(pos.x, pos.y, pos.z));
}

void CollisionBox::DrawCollisionShape(skr::fps2::Renderer::OpenGL::Shader shader, glm::mat4 view, glm::mat4 proj, glm::mat4 model, glm::vec3 color) const
{
    _boundingBox.Draw(shader, view, proj, model, color);
}

}
}
}
