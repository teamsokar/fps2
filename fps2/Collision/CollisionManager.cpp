#include "..\pch.h"
#include "CollisionManager.h"

#include "Logger.h"

#ifdef _DEBUG
#include <assert.h>
#endif // _DEBUG


#include "..\AssetPath.h"
#include "..\Renderer\OpenGL\Shader.h"
#include "..\Renderer\OpenGL\Model.h"
#include "..\Renderer\OpenGL\ModelCache.h"
#include "..\Renderer\OpenGL\LevelManager.h"
#include "..\Game\Game.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    CollisionManager::CollisionManager()
        : _hitModelId(0), _collisionWorld(nullptr)
    {
        Init();
    }

    void CollisionManager::Init()
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("Init ReactPhysics 3D Engine");

#ifdef _DEBUG
        assert(_collisionWorld == nullptr); // prevent memory leaks if not cleaned up before init it (again)
#endif // !_DEBUG

        _collisionWorld = new rp3d::CollisionWorld();

        rp3d::Vector3 initPlayerPosition(0.0f, 0.0f, 0.0f);
        rp3d::Transform playerTransform = rp3d::Transform::identity();
        _playerBox = Collision::CollisionBox(_collisionWorld, initPlayerPosition, rp3d::Vector3(0.5, 0.5, 0.5));
        _playerBox.SetCollisionCatergory(CollisionCategory::PLAYER);
        _playerBox._proxyShape->setCollideWithMaskBits(static_cast<unsigned int>(CollisionCategory::WALL) | static_cast<unsigned int>(CollisionCategory::TARGET) | static_cast<unsigned int>(CollisionCategory::START) | static_cast<unsigned int>(CollisionCategory::STARTLINE) | static_cast<unsigned int>(CollisionCategory::FINISH));

        _playerBox._body->setUserData((void*)"player");

        AddCollisionToRooms();

        auto hitModel = Renderer::OpenGL::Model(GetAssetPath("models/rp3d/sphere.obj"), glm::vec3(0.05f, 0.05f, 0.05f), 0.0f, Renderer::OpenGL::ShaderType::Simple);
        for (auto& m : hitModel._meshes)
        {
            m._useDrawColor = true;
            m.SetDrawColor(RENDER_HIT_COLOR);
        }

        _hitModelId = Renderer::OpenGL::ModelCache::GetInstance().AddModel(std::move(hitModel));
    }

    void CollisionManager::Cleanup()
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("CleanUp of ReactPhysics3D related stuff");

        for (auto& r : Renderer::OpenGL::LevelManager::GetInstance()._rooms)
        {
            for (auto& c : r._collisionObjects)
            {
                delete c;
                c = nullptr;
            }

            for (auto& t : r._targets)
            {
                for (auto c : t._collisionObjects)
                {
                    delete c;
                    c = nullptr;
                }
            }
        }

        if (_collisionWorld != nullptr)
        {
            delete _collisionWorld;
            _collisionWorld = nullptr;
        }
    }

    void CollisionManager::CheckCollision(glm::vec3 currentPosition)
    {
        rp3d::Vector3 newPos(currentPosition.x, currentPosition.y, currentPosition.z);
        rp3d::Quaternion idQuad = rp3d::Quaternion::identity();
        rp3d::Transform newTrans(newPos, idQuad);
        _playerBox._body->setTransform(newTrans);

        _collisionWorld->testCollision(_playerBox._body, &_collisionCallback, static_cast<unsigned short>(CollisionCategory::TARGET) | static_cast<unsigned short>(CollisionCategory::WALL));
    }

    void CollisionManager::CheckOverlap(glm::vec3 currentPosition)
    {
        rp3d::Vector3 newPos(currentPosition.x, currentPosition.y, currentPosition.z);
        rp3d::Quaternion idQuad = rp3d::Quaternion::identity();
        rp3d::Transform newTrans(newPos, idQuad);
        _playerBox._body->setTransform(newTrans);

        _collisionWorld->testOverlap(_playerBox._body, &_overlapCallback);
    }

    glm::vec3 CollisionManager::GetCurrentMeanNormalOfContactPoints()
    {
        return _collisionCallback.GetMeanNormalOfContactPoints();
    }

    void CollisionManager::RayCast(glm::vec3 start, glm::vec3 dir)
    {
        RayCast(rp3d::Vector3(start.x, start.y, start.z), rp3d::Vector3(dir.x, dir.y, dir.z));
    }

    void CollisionManager::RayCast(rp3d::Vector3 start, rp3d::Vector3 dir)
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("Ray Cast");

        rp3d::Vector3 endPoint = start - dir * RP3D_DEFAULT_RAYCAST_LENGHT;
        rp3d::Ray ray(start, endPoint);

        _collisionWorld->raycast(ray, &_raycastCallback, static_cast<unsigned int>(CollisionCategory::TARGET));
    }

    rp3d::CollisionWorld* CollisionManager::CollisionWorld()
    {
        return _collisionWorld;
    }

    bool CollisionManager::DrawHits()
    {
        return _drawHits;
    }

    RaycastCallback CollisionManager::GetRaycastCallback()
    {
        return _raycastCallback;
    }

    size_t CollisionManager::HitModelId()
    {
        return _hitModelId;
    }

    void CollisionManager::ResetContactPoints()
    {
        _collisionCallback.ResetContactPoints();
    }

    void CollisionManager::ClearHits()
    {
        _raycastCallback.ClearHits();
    }

    void CollisionManager::AddCollisionToRooms()
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Add Collision to Rooms");

        size_t id = 0;
        for (auto& r : Renderer::OpenGL::LevelManager::GetInstance()._rooms)
        {
            AddCollissionWallToRoom(r);

            for (auto& t : r._targets)
            {
                AddCollisionToTarget(t);
                t._id = id;
                Game::Game::GetInstance().AddTarget(id);
                ++id;
            }

            if (r._room._roomType == skr::pcg::RoomType::Start)
            {
                rp3d::Vector3 mid(r._room._center[0], r._room._center[1], r._room._center[2]);
                rp3d::Vector3 size(r._room._width, r._room._height, r._room._length);

                r._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size, CollisionCategory::START));

                switch (r._room._doors[0])
                {
                case skr::pcg::DoorDirection::TOP: // back
                {
                    mid.x = r._room._center[0];
                    mid.y = r._room._center[1];
                    mid.z = r._room._center[2] + r._room._length * 0.25f;

                    size.x = r._room._width * 2.0f;
                    size.y = r._room._height;
                    size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    break;
                }

                case skr::pcg::DoorDirection::RIGHT: // right
                {
                    mid.x = r._room._center[0] + r._room._length * 0.25f;
                    mid.y = r._room._center[1];
                    mid.z = r._room._center[2];

                    size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    size.y = r._room._height;
                    size.z = r._room._length * 2.0f;

                    break;
                }

                case skr::pcg::DoorDirection::BOTTOM: // front
                {
                    mid.x = r._room._center[0];
                    mid.y = r._room._center[1];
                    mid.z = r._room._center[2] - r._room._length * 0.25f;

                    size.x = r._room._width * 2.0f;
                    size.y = r._room._height;
                    size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                    break;
                }

                case skr::pcg::DoorDirection::LEFT: // left
                {
                    mid.x = r._room._center[0] - r._room._length * 0.25f;
                    mid.y = r._room._center[1];
                    mid.z = r._room._center[2];

                    size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    size.y = r._room._height;
                    size.z = r._room._length * 2.0f;

                    break;
                }

                default:
                    break;
                }

                r._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size, CollisionCategory::STARTLINE));
            }
            else if (r._room._roomType == skr::pcg::RoomType::Finish)
            {
                rp3d::Vector3 mid(r._room._center[0], r._room._center[1], r._room._center[2]);
                rp3d::Vector3 size(r._room._width, r._room._height, r._room._width);

                r._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size, CollisionCategory::START));
            }
        }
    }

    void CollisionManager::AddCollissionWallToRoom(Renderer::OpenGL::RoomGeometry& rg)
    {
        rp3d::Vector3 mid;
        rp3d::Vector3 size;

        // bottom
        mid.x = rg._room._center[0];
        mid.y = rg._room._center[1] - rg._room._height * 0.5f;
        mid.z = rg._room._center[2];

        size.x = rg._room._width;
        size.y = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
        size.z = rg._room._length;

        rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

        // top
        mid.x = rg._room._center[0];
        mid.y = rg._room._center[1] + rg._room._height * 0.5f;
        mid.z = rg._room._center[2];

        size.x = rg._room._width;
        size.y = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
        size.z = rg._room._length;

        rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

        // other, closed walls
        auto closedWalls = rg._room.DetermineClosedWalls(rg._room._doors);
        for (auto cw : closedWalls)
        {
            switch (cw)
            {
            case skr::pcg::DoorDirection::TOP: // back
            {
                mid.x = rg._room._center[0];
                mid.y = rg._room._center[1];
                mid.z = rg._room._center[2] + rg._room._length * 0.5f;

                size.x = rg._room._width;
                size.y = rg._room._height;
                size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));
                break;
            }
            case skr::pcg::DoorDirection::RIGHT: // right
            {
                mid.x = rg._room._center[0] + rg._room._width * 0.5f;
                mid.y = rg._room._center[1];
                mid.z = rg._room._center[2];

                size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                size.y = rg._room._height;
                size.z = rg._room._length;

                rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));
                break;
            }
            case skr::pcg::DoorDirection::BOTTOM: // front
            {
                mid.x = rg._room._center[0];
                mid.y = rg._room._center[1];
                mid.z = rg._room._center[2] - rg._room._length * 0.5f;

                size.x = rg._room._width;
                size.y = rg._room._height;
                size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));
                break;
            }
            case skr::pcg::DoorDirection::LEFT: // left
            {
                mid.x = rg._room._center[0] - rg._room._width * 0.5f;
                mid.y = rg._room._center[1];
                mid.z = rg._room._center[2];

                size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                size.y = rg._room._height;
                size.z = rg._room._length;

                rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));
                break;
            }
            default:
                break;
            }
        }

        // Create CollisionBoxes for geometry to the left and right of a door of rooms
        // skip for everything else
        if (rg._room._roomType == skr::pcg::RoomType::Room)
        {
            for (auto d : rg._room._doors)
            {
                switch (d)
                {
                case skr::pcg::DoorDirection::TOP: // back
                {
                    float wallWidth = (rg._room._width - skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;
                    float wallPositionOffset = (wallWidth + skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    // left wall
                    mid.x = rg._room._center[0] - wallPositionOffset;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] + rg._room._length * 0.5f;

                    size.x = wallWidth;
                    size.y = rg._room._height;
                    size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

                    // right wall
                    mid.x = rg._room._center[0] + wallPositionOffset;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] + rg._room._length * 0.5f;

                    size.x = wallWidth;
                    size.y = rg._room._height;
                    size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));
                    break;
                }

                case skr::pcg::DoorDirection::RIGHT: // right
                {
                    float wallWidth = (rg._room._length - skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;
                    float wallPositionOffset = (wallWidth + skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    // left wall
                    mid.x = rg._room._center[0] + rg._room._width * 0.5f;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] - wallPositionOffset;

                    size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    size.y = rg._room._height;
                    size.z = wallWidth;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

                    //right wall
                    mid.x = rg._room._center[0] + rg._room._width * 0.5f;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] + wallPositionOffset;

                    size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    size.y = rg._room._height;
                    size.z = wallWidth;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

                    break;
                }

                case skr::pcg::DoorDirection::BOTTOM: // front
                {
                    float wallWidth = (rg._room._width - skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;
                    float wallPositionOffset = (wallWidth + skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    mid.x = rg._room._center[0] - wallPositionOffset;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] - rg._room._length * 0.5f;

                    size.x = wallWidth;
                    size.y = rg._room._height;
                    size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

                    mid.x = rg._room._center[0] + wallPositionOffset;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] - rg._room._length * 0.5f;

                    size.x = wallWidth;
                    size.y = rg._room._height;
                    size.z = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));
                    break;
                }

                case skr::pcg::DoorDirection::LEFT: // left
                {
                    float wallWidth = (rg._room._length - skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;
                    float wallPositionOffset = (wallWidth + skr::pcg::PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    // left wall
                    mid.x = rg._room._center[0] - rg._room._width * 0.5f;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] - wallPositionOffset;

                    size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    size.y = rg._room._height;
                    size.z = wallWidth;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

                    //right wall
                    mid.x = rg._room._center[0] - rg._room._width * 0.5f;
                    mid.y = rg._room._center[1];
                    mid.z = rg._room._center[2] + wallPositionOffset;

                    size.x = RP3D_DEFAULT_COLLISION_WALL_THICKNESS;
                    size.y = rg._room._height;
                    size.z = wallWidth;

                    rg._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size));

                    break;
                }

                default:
                    break;
                }
            }
        }
    }

    void CollisionManager::AddCollisionToTargets()
    {
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("OpenGL Renderer Add Collision to Targets");

        for (auto& r : Renderer::OpenGL::LevelManager::GetInstance()._rooms)
        {
            size_t id = 0;
            for (auto& t : r._targets)
            {
                AddCollisionToTarget(t);
                t._id = id;
                Game::Game::GetInstance().AddTarget(id);
                ++id;
            }
        }
    }

    void CollisionManager::AddCollisionToTarget(Renderer::OpenGL::TargetGeometry& t)
    {
        rp3d::Vector3 mid;
        rp3d::Vector3 size;

        auto& model = Renderer::OpenGL::ModelCache::GetInstance().GetModel(t._modelId);
        auto bbox = model._boundingBox;

        mid.x = t._position.x;
        mid.y = t._position.y + model._yOffset;
        mid.z = t._position.z;

        size.x = bbox._size.x * model._scale.x;
        size.y = bbox._size.y * model._scale.y;
        size.z = bbox._size.z * model._scale.z;

        t._collisionObjects.push_back(new Collision::CollisionBox(_collisionWorld, mid, size, CollisionCategory::TARGET));
    }
}
}
}