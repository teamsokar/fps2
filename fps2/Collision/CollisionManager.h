#ifndef SKR_FPS2_COLLISION_COLLISIONMANAGER_H
#define SKR_FPS2_COLLISION_COLLISIONMANAGER_H

#include "reactphysics3d.h"
#include "CollisionBox.h"
#include "OverlapCallback.h"
#include "CollisionCallback.h"
#include "RaycastCallback.h"

#include "..\Renderer\OpenGL\RoomGeometry.h"
#include "..\Renderer\OpenGL\TargetGeometry.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    //! \class CollisionManager
    //! \brief initialization and handling of collisions and raycast
    //! basically everything that is released to or uses the ReactPhysics3D library
    class CollisionManager
    {
    public:
        //! \brief constructor
        //! calls Init()
        CollisionManager();

        //! \brief initialization
        //! creates collision world, creates player collision box, adds collision to rooms and targets and initializes hit visualization
        void Init();

        //! destroy everything related to ReactPhysics3D (RP3D), as doing it in the destructor is too late. As part of a global object, it gets called to late
        void Cleanup();

        //! \brief checks collision between objects and uses callback for notification
        //! \param currentPosition current Position of the player
        void CheckCollision(glm::vec3 currentPosition);

        //! \brief checks for overlaps between collision bodies
        //! as collision checks are quite expensive, for regular collision detection, overlap checks are enough
        //! \param currentPosition current Position of the player
        void CheckOverlap(glm::vec3 currentPosition);

        //! \brief calculates mean normal of all contact points
        //! depending on the size of the collision shapes, RP3D return multiple contact points and in case of multiple shapes, with different normals. In an attempt to stop the player from glitching through corners, a mean normal is calcualted and used for sliding along walls
        //! \return mean normal of all contact points
        glm::vec3 GetCurrentMeanNormalOfContactPoints();

        //! \brief triggers raycast
        //! default length is used
        //! \param start current point of the player, where the raycasts starte
        //! \param dir of the raycast
        void RayCast(glm::vec3 start, glm::vec3 dir);

        //! \brief triggers raycast
        //! default length is used
        //! \param start current point of the player, where the raycasts starte
        //! \param dir of the raycast
        void RayCast(rp3d::Vector3 start, rp3d::Vector3 dir);

        //! \brief Gets pointer to the current collision world
        //! \return pointer to collision world
        rp3d::CollisionWorld* CollisionWorld();

        //! \brief get value whethere hits are visualized or not
        //! \return true if hits are visualized, otherwise false
        bool DrawHits();

        //! \brief Gets the current raycast callback object
        //! \return raycast callback object
        RaycastCallback GetRaycastCallback();

        //! \brief Gets the model id of the model used for hit visualization
        //! \return model id of the model used for hit visualization
        size_t HitModelId();

        //! \brief clears the list of contact points
        void ResetContactPoints();

        //! \brief clears list of registered hits
        void ClearHits();

    private:
        rp3d::CollisionWorld* _collisionWorld; //!< collision world
        CollisionBox _playerBox; //!< collisionbox of the player
        OverlapCallback _overlapCallback; //!< collision callback object
        CollisionCallback _collisionCallback; //!< contact callback object

        RaycastCallback _raycastCallback; //!< raycast callback object
        bool _drawHits = true; //!< value specifying whether hits are visualized. Default true
        size_t _hitModelId; //!< model id of model for hit visualization

        //! \brief Adds collision to all walls and targets in all rooms
        void AddCollisionToRooms();
        
        //! \brief adds collision walls to specified room
        //! \param rg RoomGeomtry of room specified
        void AddCollissionWallToRoom(Renderer::OpenGL::RoomGeometry& rg);

        //! \brief adds collision to all targets in all rooms
        void AddCollisionToTargets();

        //! \brief adds collision boxes to all targest in a specified room
        void AddCollisionToTarget(Renderer::OpenGL::TargetGeometry& t);
    };

}
}
}

#endif // !SKR_FPS2_COLLISION_COLLISIONMANAGER_H

