#ifndef SKR_FPS2_COLLISION_CONTACTCALLBACK_H
#define SKR_FPS2_COLLISION_CONTACTCALLBACK_H

#include "..\pch.h"

#include <vector>

#include "reactphysics3d.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    //! \class ContactPoint
    //! \brief specifies a single contact point
    struct ContactPoint
    {
        glm::vec3 position; //!< position
        glm::vec3 normal; //!< normal

        //! \brief constructor
        //! \param pos position
        //! \param norm normal
        ContactPoint(glm::vec3 pos, glm::vec3 norm)
            : position(pos), normal(norm)
        {};
    };

    //! \class CollisionCallback
    //! \brief implements rp3d::CollisionCallback from ReactPhysics3D Library
    //! Used in detection of collisions between collision shapes
    class CollisionCallback : public rp3d::CollisionCallback
    {
    public:
        //! \brief callback function called if a collision is detected
        //! \param collisionCallbackInfo object holding information about the collision
        virtual void notifyContact(const CollisionCallbackInfo& collisionCallbackInfo) override;

        //! \brief clears list of registered contact points
        void ResetContactPoints();

        //! \brief returns list of registered contact points
        //! \return list of registered ContactPoints
        std::vector<ContactPoint> GetContactPoints();

        //! \brief calculates mean normal of registered contact points
        //! \return mean normal of registered contact points
        glm::vec3 GetMeanNormalOfContactPoints();

    private:
        std::vector<ContactPoint> _contactPoints; //!< list of registered contact points
    };
}
}
}

#endif // !SKR_FPS2_COLLISION_CONTACTCALLBACK_H
