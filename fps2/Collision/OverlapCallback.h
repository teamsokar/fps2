#ifndef SKR_FPS2_COLLISION_COLLISION_CALLBACK_H
#define SKR_FPS2_COLLISION_COLLISION_CALLBACK_H

#include "..\pch.h"

#include "reactphysics3d.h"

namespace skr
{
namespace fps2 
{
namespace Collision
{
    //! \class OverlapCallback
    //! \brief implements rp3d::OverlapCallback from ReactPhysics3D Library
    //! Used in detection of overlaps between collision shapes
    class OverlapCallback : public rp3d::OverlapCallback
    {
        public:
            //! \brief call if an overlap between collision bodies is detected
            //! \param collisionBody collision body that caused the collision
            virtual void notifyOverlap(rp3d::CollisionBody* collisionBody) override;
    };

}
}
}

#endif // !SKR_FPS2_COLLISION_COLLISION_CALLBACK_H
