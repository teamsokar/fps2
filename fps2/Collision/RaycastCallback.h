#ifndef SKR_FPS2_RENDERER_OPENGL_RAYCAST_CALLBACK_H
#define SKR_FPS2_RENDERER_OPENGL_RAYCAST_CALLBACK_H

#include "..\pch.h"

#include <vector>
#include "reactphysics3d.h"

namespace skr
{
namespace fps2
{
namespace Collision
{
    //! \class RaycastCallback
    //! \brief implements rp3d::RaycastCallback from ReactPhysics3D Library
    //! Used in hit detection
    class RaycastCallback : public rp3d::RaycastCallback
    {
    public:
        std::vector<rp3d::Vector3> _hitInfos; //!< list of hit info objects, containing information of each registered hit

    public:
        //! \brief callback function called if a hit is registered
        //! \param info info bject, containing information of the registered hit
        //! \return 0.0f as no additional information about the hit is needed
        float notifyRaycastHit(const rp3d::RaycastInfo& info);

        //! \brief clears list of registered hit infos
        void ClearHits();
        
    };
}
}
}

#endif //!SKR_FPS2_RENDERER_OPENGL_RAYCAST_CALLBACK_H
