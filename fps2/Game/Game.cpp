#include "..\pch.h"

#include <iostream>

#include "Game.h"
#include "..\Renderer\OpenGL\LevelManager.h"
#include "Logger.h"

namespace skr
{
namespace fps2
{
namespace Game
{
    Completed::Completed(uint32_t id)
        : _targetId(id), _completed(false)
    {

    }

    uint32_t Game::NumberOfCompeltedTargets()
    {
        uint32_t noTargetsCompleted = 0;

        for (auto t : _targets)
        {
            if (t._completed)
                noTargetsCompleted++;
        }

        return noTargetsCompleted;
    }

    Game& Game::GetInstance()
    {
        static Game instance;
        return instance;        
    }

    void Game::AddTarget(uint32_t target)
    {
        _targets.push_back(target);
    }

    void Game::Game::Shot()
    {
        _numberOfShots++;
    }

    void Game::Hit(uint32_t id)
    {
        _numberOfHits++;

        bool newHit = false;
        for (auto& c : _targets)
        {
            if (!c._completed && c._targetId == id)
            {
                c._completed = true;
                newHit = true;

                bool found = false;
                for (auto& r : Renderer::OpenGL::LevelManager::GetInstance()._rooms)
                {
                    for (auto& t : r._targets)
                    {
                        if (t._id != id)
                        {
                            continue;
                        }
                        else
                        {
                            t.SetCompleted(true);
                            found = true;
                            break;
                        }
                    }

                    if (found)
                        break;
                }
            }
        }

        if (newHit)
        {
            bool isComplete = false;
            for (auto t : _targets)
            {
                if (t._completed)
                {
                    isComplete = true;
                    continue;
                }
                else
                {
                    isComplete = false;
                    break;
                }
            }

            if (isComplete)
                _complete = true;
        }
    }

    void Game::Reset()
    {
        _targets.clear();
        _started = false;
        _finished = false;
        _numberOfHits = 0;
        _numberOfShots = 0;
        _complete = false;
    }

    void Game::ResetStats()
    {
        _numberOfHits = 0;
        _numberOfShots = 0;
        _complete = false;
    }

    bool Game::IsComplete()
    {
        return _complete;
    }

    std::string Game::GetStats()
    {
        std::string msg;

        msg += "Hit Ration: " + std::to_string(_numberOfHits) + "/" + std::to_string(_numberOfShots)
            +  " Target Completion Ratio: " + std::to_string(NumberOfCompeltedTargets()) + "/" + std::to_string(_targets.size())
            + " Completed: "
            +  (_complete ? "yes" : "no");

        if (!_finished)
        {
            std::chrono::duration<double> elapsedSeconds = std::chrono::system_clock::now() - _startTime;
            msg += " Time: " + std::to_string(elapsedSeconds.count());
        }
        else
        {
            std::chrono::duration<double> elapsedSeconds = _finishTime - _startTime;
            msg += " Time: " + std::to_string(elapsedSeconds.count());
        }

        return msg;
    }

    void Game::Start()
    {
        _startTime = std::chrono::system_clock::now();
        _started = true;
        std::cout << "Here you go!" << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("Game started");
    }

    void Game::Finish()
    {
        _finishTime = std::chrono::system_clock::now();
        _finished = true;
        std::cout << "Finished!\n" <<  GetStats() << std::endl;
        skr::tools::Logger::GetInstance().LogWithDatenAndTime("Game Finished\n" + GetStats());
    }

    bool Game::IsFinished()
    {
        return _finished;
    }

    bool Game::IsStarted()
    {
        return _started;
    }

    bool Game::IsRunning()
    {
        return IsStarted() && !IsFinished();
    }
}
}
}
