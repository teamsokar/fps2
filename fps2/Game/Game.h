#ifndef SKR_FPS2_GAME_H
#define SKR_FPS2_GAME_H

#include "..\pch.h"

#include <vector>
#include <string>
#include <chrono>

#include "pcg/Target.h"
#include "..\Renderer\OpenGL\TargetGeometry.h"

namespace skr
{
namespace fps2
{
namespace Game
{
    //! \class Completed
    //! \brief holds target completion
    struct Completed
    {
    public:
        uint32_t _targetId; //!< id of target
        bool _completed; //!< true if compelted, otherwise false

        //! \brief constructor
        //! \param id id of target
        Completed(uint32_t id);
    };

    //! \class Game
    //! \brief Main Game Manager class
    //! keeps track of status of game (started, finished, completed) and stats (number of shots, hits, etc).
    /*! Implements Scott Meyers' singleton pattern. */
    class Game
    {
    private:
        //! \brief default constructor
        //! resets stats
        Game()
        {
            ResetStats();
        };

        //! \brief copy constructor is deleted
        Game(const Game&) = delete;

        //! \brief copy assignment operator is deleted
        Game operator=(const Game&) = delete;

        bool _started; //!< flag inidacting if game ist startet
        bool _finished; //!< flat inidicating if finish is reached
        bool _complete; //!< flag inidication if game is completed
        std::chrono::system_clock::time_point _startTime; //!< recoreded time of player crossing the start line
        std::chrono::system_clock::time_point _finishTime; //!< recoreded time of player reaching the finish box

        //! \brief returns number of completed targets
        //! \return number of completed targets
        uint32_t NumberOfCompeltedTargets();

    public:
        //! \brief gets only instance of Game manager
        //! Implements Scott Meyer's singleton pattern
        //! \return reference to only instance of this class
        static Game& GetInstance();

        std::vector<Completed> _targets; //!< list of target ids and completion status
        uint32_t _numberOfShots; //!< number of shots fired by the plaer
        uint32_t _numberOfHits; //!< number of hits registered

        //! \brief adds target of given id to list of targets
        //! \param target id of target
        void AddTarget(uint32_t target);

        //! \brief called if a shot is fired (LMB press)
        void Shot();

        //! \brief called if a hit is registered
        //! \param id of target hit
        void Hit(uint32_t id);

        //! \brief resets game
        //! clears targets and resets all flags and stats to initial values
        void Reset();

        //! \brief resets stats
        void ResetStats();

        //! \brief returns value inidcating if all targets are not or not
        //! \return true if all targets are hit, otherwise false
        bool IsComplete();

        //! \brief gets current stats in human readable form
        //! \return string of current stats in human readable form
        std::string GetStats();

        //! \brief called if game is startet: player crosses the start line
        void Start();

        //! \brief called if game finished: player reaches the finish box
        void Finish();

        //! \brief returns value inidicating if game is finished
        //! \return true if finished, otherwise false
        bool IsFinished();

        //! \brief returns a value indcating if game has started
        //! \return true if game is started, otherwise false
        bool IsStarted();

        //! \brief returns a value indicating if game is running
        //! \return true if IsStarted() and not IsFinished(), otherwise false
        bool IsRunning();
    };

}
}
}

#endif // !SKR_FPS2_GAME_H

